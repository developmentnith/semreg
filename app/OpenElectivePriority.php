<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenElectivePriority extends Model
{
  protected $table = 'OpenElectivePriority';
    protected $fillable = ['id','RollNumber','ChoiceNum' ,'SubjectID' ,'SubjectName' ,'Department' , 'Degree','Semester' ,'year' , 'Allotted','Status' ,'Token' ,'created_at' ,'updated_at'];
}
