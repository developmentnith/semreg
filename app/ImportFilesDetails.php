<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportFilesDetails extends Model
{
    protected $table = 'ImportFilesDetails';
    protected $fillable = ['fileName', 'fileExtension', 'status']; 
   
}
