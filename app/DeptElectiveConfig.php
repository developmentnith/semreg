<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeptElectiveConfig extends Model
{
    protected $table = 'DeptElectiveConfig';
    protected $fillable = ['SubjectCode', 'SubjectName', 'FloatYear', 'Semester', 'SemNum','Branch','Degree','FloatSession','pool','FloatedBy','isActive','status','token'];


}
