<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenElectiveConfig extends Model
{
    protected $table = 'DeptOpenElectiveConfig';
    protected $fillable = ['id','FloatedByDept', 'SubjectCode', 'SubjectName', 'credit', 'Semester','Degree','FloatedToDept','FloatYear','FloatSession','MinimumStudent','MaximumStudent','FloatedByUser','isActive','Token','created_at','updated_at'];
}
