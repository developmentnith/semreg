<?php

namespace App\Exports;

use App\SemesterWiseSGPICGPI;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class SGPICGPIExport implements FromCollection, WithHeadings
        {
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return SemesterWiseSGPICGPI::all();
    }
    public function headings() : array{
        return [
            'RID',
            'RollNumber',
            'Semester',
            'SGPI',
            'CGPI',
            'Status',
            'UploadDate',
        ];
    }
}
