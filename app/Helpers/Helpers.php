<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Helpers {

    public static function getUserBranch($user_id) {

        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->AdmissionBranch) ? $user->AdmissionBranch : '');
    }
    public static function getStudentDegree($user_id) {

        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->AdmissionIN) ? $user->AdmissionIN : '');
    }
    
      public static function getUserDepartment($user_id) {

        $user = DB::table('users')->where('id', $user_id)->first();

        return (isset($user->department) ? $user->department : '');
    }
      public static function getUserName($user_id) {

        $user = DB::table('users')->where('id', $user_id)->first();

        return (isset($user->name) ? $user->name : '');
    }
    public static function getStudentID($user_id) {

        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->StudentID) ? $user->StudentID : '');
    }
    public static function getUserIDFromStudentID($stud_id) {

        $user = DB::table('StudentBasic')->where('StudentID', $stud_id)->first();

        return (isset($user->RegUserID) ? $user->RegUserID : '');
    }

    public static function getUserRollNumber($user_id) {

        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->InstituteRollNumber) ? $user->InstituteRollNumber : '');
    }
    public static function getUserRollNumberFromAlloted($user_id) {

        $user = DB::table('StudentRollNumber')->where('StudentID', $user_id)->first();

        return (isset($user->InstituteRollNumber) ? $user->InstituteRollNumber : '');
    }

    public static function getUserEmail($user_id) {

        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->EmailIDStudent) ? $user->EmailIDStudent : '');
    }

    public static function getUserStatus($user_id) {

        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->isActive) ? $user->isActive : '');
    }

    public static function getUserDOB($user_id) {

        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->DOB) ? $user->DOB : '');
    }

    public static function getUserFullName($user_id) {
        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->StudentName) ? $user->StudentName : '');
    }

    public static function getUserVerifiedStatus($user_id) {
        $user = DB::table('StudentBasic')->where('RegUserID', $user_id)->first();

        return (isset($user->Token) ? $user->Token : 'N');
    }
    
    
    public static function getUserCourse($user_id) {
        $user = DB::table('users')->where('id', $user_id)->first();

        return (isset($user->course) ? $user->course : '');
    }

    public static function getUserType($user_id) {
        $user = DB::table('users')->where('id', $user_id)->first();

        return (isset($user->studenttype) ? $user->studenttype : '');
    }

    public static function getType($user_id) {
        $user = DB::table('users')->where('id', $user_id)->first();

        return (isset($user->type) ? $user->type : '');
    }

    public static function populateCombo($tablename, $field) {
        $items = DB::table($tablename)->select($field)->distinct()->get();

        return $items;
    }

    public static function populateComboOne($tablename) {
//      $items = DB::table($tablename)->select($field)->distinct()->get();
        $items = DB::table($tablename)->get();
        return $items;
    }

    public static function getSemesterData($tablename) {
//      $items = DB::table($tablename)->select($field)->distinct()->get();
        $items = DB::table($tablename)->get();
        return $items;
    }

    public static function getSemesterRegData($id) {
        $result = DB::table('StudentSemReg')->where('userID', $id)->orderBy('Semester', 'desc')->get();
        return $result;
    }
    public static function getSubject($sbcode) {
        $result = DB::table('SubjectMaster')->where('SubjectCode', $sbcode)->first();
        return $result;
    }

    public static function getCurrentSemester($id) {
        $result = DB::table('StudentSemReg')->select('currentSem')->where('userID', $id)->get();
        return $result;
    }
    public static function isValidStudent($id) {
        $result = DB::table('StudentSemReg')->select('Verified')->where('userID', $id)->get();
     
        if($result->isEmpty()){
            return false;
        } else {                  
            if($result[0]->Verified==='N'){
                return false;
            } else {             
            return true;
            
            }
        }
        //return $result;
    }

    public static function getNextSem($id) {
        $result = DB::table('semesterMaster')->where('semName', '>', $id)->orderBy('semName', 'desc')->get();
        return $result;
    }

    public static function getSemNumToText($num) {

        switch ($num) {

            case '1' :
                return "S01";
                break;
            case '2' :
                return "S02";
                break;
            case '3' :
                return "S03";
                break;
            case '4' :
                return "S04";
                break;
            case '5' :
                return "S05";
                break;
            case '6' :
                return "S06";
                break;
            case '7':
                return "S07";
                break;
            case '8' :
                return "S08";
                break;

            case '9' :
                return "S09";
                break;
            case '10' :
                return "S10";
                break;

            default :
                return "S01";
        }
    }

    public static function getSemTextToNum($sem) {

        switch ($sem) {
            case "S01" :
                return 1;
                break;
            case "S02" :
                return 2;
                break;
            case "S03" :
                return 3;
                break;
            case "S04" :
                return 4;
                break;
            case "S05" :
                return 5;
                break;
            case "S06" :
                return 6;
                break;
            case "S07":
                return 7;
                break;
            case "S08" :
                return 8;
                break;

            case "S09" :
                return 9;
                break;
            case "S10" :
                return 10;
                break;

            default :
                return 1;
        }
    }

}
