<?php

namespace App\Imports;

use App\StudentBasic;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class AdminImport implements ToModel {
    public function model(array $row) {
        return new StudentBasic([ 
            'AdmissionIN' => $row[0],
            'AdmissionThrough' => $row[1],
            'AdmissionCategory' => $row[2],
            'StudentName' => $row[3],
            'FatherName' => $row[4],
            'MotherName' => $row[5],
            'DOB' => $row[6],
            'Gender' => $row[7],
            'Category' => $row[8],
            'Religion' => $row[9],
            'PermanentAddress' => $row[10],
            'EmailIDStudent' => $row[11],
            'AdharNo' => $row[12],
            'IsActive' => $row[13],
        ]);
    }

}
