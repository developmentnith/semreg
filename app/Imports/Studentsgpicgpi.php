<?php

namespace App\Imports;

use App\SemesterWiseSGPICGPI;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;

class Studentsgpicgpi implements ToModel {
    public function model(array $row) {
        return new StudentBasic([ 
            'RollNumber' => $row[0],
            'Semester' => $row[1],
            'SGPI' => $row[2],
            'CGPI' => $row[3],
            'Status' => $row[4],           
        ]);
    }

}
