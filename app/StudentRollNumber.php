<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentRollNumber extends Model
{  
    
     protected $table = 'StudentRollNumber';
    protected $fillable = ['StudentID', 'InstituteRollNumber', 'StudentName', 'Degree', 'Branch', 'Grop', 'Section', 'AdmissionYear'];
    //
}
