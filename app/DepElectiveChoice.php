<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepElectiveChoice extends Model
{
 
     protected $table = 'DepSubElectiveChoice';
    protected $fillable = ['id','studentID', 'Branch','Semester','RollNumber', 'Pool','SubjectID' ,'Status','Token','updated_at','created_at'];

    
    
}

    