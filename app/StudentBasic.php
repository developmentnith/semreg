<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentBasic extends Model
{
    //
    protected $table = 'StudentBasic';
    protected $fillable = ['AdmissionIN','AdmissionThrough',' AdmissionCategory','StudentName',' StudentNameHindi','FatherName','FatherNameHindi',' MotherName','MotherNameHindi','GuardianName','DOB','Gender','Category','Religion','PermanentAddress','CoAddress','ContactNumberStudent','ContactNumberFather','ContactNumberMother','ContactNumberGuardian','EmailIDStudent','EmailIDFather','BonafiedState','AdharNo','NADID','WeatherPWD','Rural_Urban','AnnualFamilyIncome','HostelRequired','RailwayStation','jeeMainRollNumber','JeeMainRankAl','JeeMainRankState','JeeMainRankCategoryAI','JeeMainRankCategoryState','GateRegID','GateScore','GateRegYear','GatePaperCode','LastInstitutionAttended','QualifyingExamPassed','QualifyingExamPassedState','MarksInPercentage_CGPI','MarksPercentageCGPI','AdmittedCategory','LastExamPassingYear','AdmissionBranch','Specilization','RegistrationNumber','FileNumber','AdmissionDate','DataChcekedBy','DataVerifiedBy','StudentAdmissionStatus','InstituteRollNumber','RegUserID','Status','Token','IsActive'];
}
