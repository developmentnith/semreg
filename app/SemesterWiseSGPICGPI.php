<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SemesterWiseSGPICGPI extends Model
{
    protected $table = 'SemesterWiseSGPICGPI';
    protected $fillable = ['RID', 'RollNumber', 'Semester', 'SGPI', 'CGPI','UGPGTag','Status','UploadDate'];
}
