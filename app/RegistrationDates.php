<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationDates extends Model
{
    protected $table = 'RegistrationDates';
    protected $fillable = ['RID', 'Type', 'Degree','Department','Session', 'Semester', 'StartDate', 'EndDate', 'Status', 'Token','created_at','updated_at'];
}
