<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRoll extends Model
{
    
     protected $table = 'userRoll';
    protected $fillable = ['userID', 'userName', 'rollName','status']; 
    //
}
