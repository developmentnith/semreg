<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSemReg extends Model
{  
    //
    protected $table = 'StudentSemReg';
    protected $fillable = ['userID','RollNumber','currentSem','Semester','RegYear','Session','SGPI','CGPI','PGSGPI','PGCGPI'];
}
