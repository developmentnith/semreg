<?php

namespace App\Http\Controllers;

use App\User;
use App\UserToken;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller {

    public function sendOtp(Request $request) {
//        $email = $request->get('email');
        $phone = $request->get('phone');
        $phoneotp = $this->generatePhoneOtp();
//        $emailotp = $this->generateEmailOtp();
//        $result = $this->phoneOtp($email, $phoneotp);
        $result = $this->checkSendSms($phone, $phoneotp);
//        $email_result = $this->emailOtp($email, $emailotp);
        if ($result) {
            return['massage' => $result, 'sucess' => 'true', 'sendotp' => $phoneotp];
        }
    }

    public function registerUser(Request $request) {
        $user = new User();
        $user->name = $request->name;
        $user->course = $request->course;
        $user->studenttype = $request->student_type;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->type = 'student';
        if ($request->password == $request->confirmPassword) {
            $user->password = Hash::make($request->password);
            $res = $user->save();
            return ['status' => 'true', 'msg' => 'Register Successfully'];
        } else {
            return ['status' => 'false', 'msg' => 'Password does not matched.'];
        }
    }

    public function loginUser(Request $request) {
        if ($request->email == "" || $request->password == "") {
            return response()->json(['msg' => "Username or Password field can not blank"], 200);
        }
        $password = trim($request->password);
        if (Auth::attempt(array('email' => $request->email, 'password' => $password, 'type' => $request->login_type))) {
            return ['status' => 'true', 'msg' => 'Login Successfully', 'login_type' => $request->login_type];
        } else {
            Auth::logout();
            return ['status' => 'false', 'msg' => 'Login Failed', 'login_type' => $request->login_type];
        }
    }

    public function passwordForget(Request $request) {
        $userinfo = DB::table('users')->where('email', $request->email_id)->where('phone', $request->phone_no)->first();
        if (isset($userinfo)) {
            $regEmail = $userinfo->email;
            $regPhone = $userinfo->phone;
            $email = $request->email_id;
            $phone = $request->phone_no;
            if ($email == $regEmail && $regPhone == $phone) {
                $password = $this->generatePhonePass();
                $data = [
                    'password' => Hash::make($password)
                ];
                DB::table('users')->where('email', $request->email_id)->where('phone', $request->phone_no)->update($data);
                $result = $this->phonePassword($phone, $password);
         
                if ($result) {
                    return['status' => 'true', 'msg' => 'Password is changed. A new password send in your phone number.'];
                }
            } else {
                return['status' => 'false', 'msg' => 'The mobile number you are giving is not registred phone number'];
            }
        } else {
            return['status' => 'false', 'msg' => 'email id and phone number is not registerd'];
        }
    }

    function generatePhoneOtp($length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function generatePhonePass($length = 8) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function generateEmailOtp($length = 6) {
        $characters = '0123456789abcdefghiklmnopqerstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function emailOtp($email, $emailotp) {
        $to = $email;
        $subject = 'OTP';
        $message = 'OTP : ' . $emailotp . '
<br><br>Thanks, <br>NITH';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: <acdrsunny@gmail.com>' . "\r\n";
        $mail_sent = mail($to, $subject, $message, $headers);
        if ($mail_sent) {
            echo '1';
        } else {
            echo '';
        }
    }

    function phoneOtp($email, $phoneotp) {
//        $mob = $phone;
//        $authKey = "276566A5lvFk43XWT5cdaad1b";
//        $mobileNumber = $mob;
//        $opt = $phoneotp;
//        $senderId = "NITHAD";
//        $message = urlencode("OTP: $opt");
//        $route = "4";
//        $postData = array(
//            'authkey' => $authKey,
//            'mobiles' => $mobileNumber,
//            'message' => $message,
//            'sender' => $senderId,
//            'route' => $route
//        );
//        $url = "http://api.msg91.com/api/sendhttp.php";
//        $ch = curl_init();
//        curl_setopt_array($ch, array(
//            CURLOPT_URL => $url,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_POST => true,
//            CURLOPT_POSTFIELDS => $postData
//        ));
//
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//        $output = curl_exec($ch);
//        if (curl_errno($ch)) {
//
//            echo 'error:' . curl_error($ch);
//        }
//        curl_close($ch);
//        return $output;


        $url = 'https://nith.ac.in/api/v1/sendemailotp/' . $email . '/' . $phoneotp;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response_json = curl_exec($ch);
        $result = json_decode($response_json);
        curl_close($ch);
        return $result;
    }

    function phonePassword($phone, $password) {
        $mob = $phone;
        $authKey = "276566A5lvFk43XWT5cdaad1b";
        $mobileNumber = $mob;
        $opt = $password;
        $senderId = "NITHAD";
        $message = urlencode("Password: $opt");
        $route = "4";
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url = "http://api.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'error:' . curl_error($ch);
        }

        curl_close($ch);
        return $output;

//        $url = 'https://nith.ac.in/api/v1/sendemailpassword/' . $email . '/' . $password;
//        $ch = curl_init($url);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $response_json = curl_exec($ch);
//        $result = json_decode($response_json);
//        curl_close($ch);
//        return $result;
    }

    public function forgetPassword() {
        return view('forget_password');
    }

    public function userCheckEmail(Request $req) {
        $email = $req->email;
        $email = DB::table('users')->where('email', $email)->value('email');
        if ($email) {
            return [array('msg' => 'true')];
        } else {
            return [array('msg' => 'false')];
        }
    }

    public function createPass() {
//        $pass = '123456';
//        $pass = 'semreg$#123';
        $pass = 'admin@123';
        echo Hash::make($pass);
        die();
    }

    public function checkSendSms($phone, $phoneotp) {
        $mob = $phone;
        $authKey = "276566A5lvFk43XWT5cdaad1b";
        $mobileNumber = $mob;
        $opt = $phoneotp;

        $senderId = "NITHAD";
        $message = urlencode("OTP: $opt");
        $route = "4";
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );
        $url = "http://api.msg91.com/api/sendhttp.php";
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
        ));

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        if (curl_errno($ch)) {

            echo 'error:' . curl_error($ch);
        }
        curl_close($ch);
        return $output;
    }

}
