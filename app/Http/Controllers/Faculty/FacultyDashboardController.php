<?php

namespace App\Http\Controllers\Faculty;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Helpers\Helpers;
use App\DeptElectiveConfig;
use App\OpenElectiveConfig;
use App\DepElectiveChoice;
use App\StudentNoDues;
use Illuminate\Support\Facades\DB;
use Session;

class FacultyDashboardController extends Controller {

    public function index() {
        return view('faculty.dashboardnew');
    }

    public function registerStudent() {
//        $data['populatebranshort'] = Helpers::populateComboOne('DepartmentMaster');
        $data['populatebranshort'] = DB::table('DepartmentMaster')->where('DepartmentShortName', Auth::user()->department)->get();
        $data['student'] = DB::table('StudentBasic')
                ->join('StudentSemReg', 'StudentSemReg.userID', '=', 'StudentBasic.RegUserID')
                ->where(array('StudentBasic.isActive' => 'Y', 'StudentBasic.AdmissionBranch' => Auth::user()->department, 'StudentBasic.Token' => 'Y'))
                ->get();
        return view('faculty.registerstudent', $data);
    }

    public function floatDepElective() {
        $id = Auth::user()->id;
        $data['deptelectiveconfig'] = DB::table('DeptElectiveConfig')->where('FloatedBy', User::find($id)->name)->get();
        $data['department'] = User::find($id)->department;
        $data['depelective'] = DB::table('electiveSemsterConfig')->where('Department', User::find($id)->department)->get();
        return view('faculty.floatdepelective', $data);
    }

    public function floatOpenElective() {
        $id = Auth::user()->id;
        $data['opentelectiveconfig'] = DB::table('DeptOpenElectiveConfig')->where('FloatedByUser', User::find($id)->name)->get();
        $data['department'] = User::find($id)->department;
        $data['depelective'] = DB::table('electiveSemsterConfig')->where('Department', User::find($id)->department)->get();
        return view('faculty.floatopenelective', $data);
    }

    public function ViewDepElective() {

        $dept = Auth::user()->department;

        $data['student'] = DB::table('DepSubElectiveChoice')->where('Token', 'N')
                ->where('Branch', $dept)
                ->get();
        return view('faculty.viewdeplective', $data);
    }

    public function ViewOpenElective() {
        $dept = Auth::user()->department;

        $data['openallot'] = DB::table('OpenElectiveAllotment')->where('Token', 'Y')
                ->where('Department', $dept)
                ->get();

        return view('faculty.viewopenelective', $data);
    }

    public function getSubjectName(Request $request) {
        $id = Auth::user()->id;
        $semester = $request->get('semester');
        $subject = DB::table('SubjectMaster')->where(array('Semester' => $semester, 'Department' => User::find($id)->department))->get();
        return ['result' => $subject];
    }

    public function getStudentName(Request $request) {
        $rollnum = $request->get('RollNumber');
        $studentname = DB::table('StudentBasic')->where(array('InstituteRollNumber' => $rollnum))->get();
        return ['result' => $studentname[0]->StudentName];
    }

    public function addDepartmentElective(Request $request) {
        $id = Auth::user()->id;
        $subjectnum = Helpers::getSemTextToNum($request->semester);
        $depelective = DB::table('DeptElectiveConfig')->where(array('SubjectCode' => $request->subject, 'Semester' => $request->semester))->first();
        if ($depelective === null) {
            $dep = new DeptElectiveConfig;
            $dep->Degree = $request->degree;
            $dep->FloatYear = $request->year;
            $dep->Semester = $request->semester;
            $dep->SemNum = $subjectnum;
            $dep->SubjectCode = $request->subject;
            $dep->SubjectName = '';
            $dep->Branch = '';
            $dep->FloatSession = '';
            $dep->FloatedBy = User::find($id)->name;
            $dep->status = 'N';
            $dep->isActive = 'N';
            $dep->token = 'N';
            $dep->Pool = $request->pool;
            $dep->save();
            $message = "Data Saved Successfully.";
            $type = 'success';
            Session::flash('message', $message);
            Session::flash('type', $type);
            return redirect('/faculty/float-dep-elective');
        } else {
            $message = "Data Allready exists";
            $type = 'error';
            Session::flash('message', $message);
            Session::flash('type', $type);
            return redirect('/faculty/float-dep-elective');
        }
    }

    public function editDepartmentElective(Request $request) {
        $id = Auth::user()->id;
        $subjectnum = Helpers::getSemTextToNum($request->semester);
        $depelective = DB::table('DeptElectiveConfig')->where(array('SubjectCode' => $request->subject, 'Semester' => $request->semester))->first();

        $dep = DeptElectiveConfig::find($request->id);
        $dep->Degree = $request->degree;
        $dep->FloatYear = $request->year;
        $dep->Semester = $request->semester;
        $dep->SemNum = $subjectnum;
        $dep->SubjectCode = $request->subject;
        $dep->SubjectName = '';
        $dep->Branch = '';
        $dep->FloatSession = '';
        $dep->FloatedBy = User::find($id)->name;
        $dep->status = 'N';
        $dep->isActive = 'N';
        $dep->token = 'N';
        $dep->Pool = $request->pool;
        $dep->save();
        $message = "Data Saved Successfully.";
        $type = 'success';
        Session::flash('message', $message);
        Session::flash('type', $type);
        return redirect('/faculty/float-dep-elective');
    }

    public function deleteDepelective($id) {
        $user = DeptElectiveConfig::find($id);
        $user->delete();
        Session::flash('message', 'Deleted Successfully.');
        Session::flash('type', 'success');
        return redirect('/faculty/float-dep-elective');
    }

    public function addOpentElective(Request $request) {
        $id = Auth::user()->id;
        $semnum = Helpers::getSemTextToNum($request->semester);
        $subName = DB::table('SubjectMaster')->where(array('SubjectCode' => $request->subject, 'Department' => User::find($id)->department))->first();
        $dep = new OpenElectiveConfig;
        $dep->Degree = $request->degree;
        $dep->FloatYear = $request->year;
        $dep->Semester = $request->semester;
        $dep->SubjectID = $request->subject;
        $dep->SubjectName = $subName->SubjectName;
        $dep->FloatSession = '';
        $dep->credit = $subName->credit;
        $dep->FloatedByDept = User::find($id)->department;
        $dep->FloatedToDept = 'all';
        $dep->MinimumStudent = '40';
        $dep->MaximumStudent = '100';
        $dep->FloatedByUser = User::find($id)->name;
        $dep->isActive = 'Y';
        $dep->token = 'N';
        $dep->save();
        $message = "Data Saved Successfully.";
        $type = 'success';
        Session::flash('message', $message);
        Session::flash('type', $type);
        return redirect('/faculty/float-open-elective');
    }

    public function deleteOpenelective($id) {
        $user = OpenElectiveConfig::find($id);
        $user->delete();
        Session::flash('message', 'Deleted Successfully.');
        Session::flash('type', 'success');
        return redirect('/faculty/float-open-elective');
    }

    public function multiApprove(Request $request) {
        $type = $request->type;
        $checkvalue = $request->get('checkvalue');
        $totalvalue = $request->get('total');
        if ($type) {
            DeptElectiveConfig::whereIn('id', explode(",", $checkvalue))
                    ->update([
                        'isActive' => 'Y',
                        'status' => 'Y'
            ]);
            $message = "Approved Successfully.";
            return ['status' => 'true', 'msg' => $message];
        } else {
            $message = "Unapprved Data";
            return ['status' => 'true', 'msg' => $message];
        }
    }

    public function depMultiApprove(Request $request) {
        $type = $request->type;
        $checkvalue = $request->get('checkvalue');
        $totalvalue = $request->get('total');
        if ($type) {

            DepElectiveChoice::whereIn('id', explode(",", $checkvalue))
                    ->update([
                        'Token' => 'Y'
            ]);
            $message = "Approved Successfully.";
            return ['status' => 'true', 'msg' => $message];
        } else {
            $message = "Unapprved Data";
            return ['status' => 'true', 'msg' => $message];
        }
    }

    public function semStudentApprove(Request $request) {
        $type = $request->type;
        $checkvalue = $request->get('checkvalue');
        $totalvalue = $request->get('total');
        $currentsem = $request->get('currentsem');
        if ($type) {
            DB::table('StudentSemReg')->whereIn('userID', explode(",", $checkvalue))
                    ->update([
                        'UpdateToken' => 'Y',
                        'ApproveToken' => 'Y',
                        'ApproveSemNum' => $currentsem,
            ]);
            $message = "Approved Successfully.";
            return ['status' => 'true', 'msg' => $message];
        } else {
            $message = "Unapprved Data";
            return ['status' => 'true', 'msg' => $message];
        }
    }

    public function approveSingleStudent($sem, $id) {
        if ($sem) {
            DB::table('StudentSemReg')->where('userID', $id)
                    ->update([
                        'UpdateToken' => 'Y',
                        'ApproveToken' => 'Y',
                        'ApproveSemNum' => $sem,
            ]);
            Session::flash('message', 'Approved Successfully.');
            Session::flash('type', 'success');
            return redirect('/faculty/view-student-for-semester');
        } else {
            Session::flash('message', 'Unapprved Data');
            Session::flash('type', 'error');
            return redirect('/faculty/view-student-for-semester');
        }
    }

    function popup($page_name = '', $param2 = '') {
        $id = Auth::user()->id;
        $page_data['depelective'] = DB::table('electiveSemsterConfig')->where('Department', User::find($id)->department)->get();
        $page_data['param2'] = $param2;
        return view('faculty.' . $page_name, $page_data);
    }

    function viewSGPI($page_name = '', $param2 = '') {
        $id = Auth::user()->id;
        $page_data['semdata'] = DB::table('StudentSemReg')->where('id', $param2)->get();
//        $sgpicgpiadmin = DB::table('SemesterWiseSGPICGPI')->where('RollNumber', '=', $userrolln)
//                        ->where('Status', 'Y')->orderBy('UGPGTag', 'DESC')->get();

        $page_data['param2'] = $param2;
        return view('faculty.' . $page_name, $page_data);
    }

    public function studentNodues() {

        $id = Auth::user()->id;
        $usname = Helpers::getUserName($id);

        $userdept = Helpers::getUserDepartment($id);


        $noduebydept = DB::table('StudentNoDues')->where(array('NoDueByDept' => $userdept, 'isCleared' => 'N'))->get();
        $data['nodues'] = $noduebydept;
        $data['usname'] = $usname;

        return view('faculty.student-nodues', $data);
    }

    public function addStudentNodues(Request $req) {

        $id = Auth::user()->id;


        $userdept = Helpers::getUserDepartment($id);



        $nodu = new StudentNoDues;
        $nodu->RollNumber = $req->rollnumber;
        $nodu->Semester = $req->semester;
        $nodu->NoDueDetail = $req->noduedetail;
        $nodu->NoDueRemark = $req->remarks;
        $nodu->NoDueByDept = $userdept;
        $nodu->isCleared = 'N';
        $nodu->NoDueDate = date("d-m-Y");
        $nodu->save();
        $message = "Data Saved Successfully.";
        $type = 'success';
        Session::flash('message', $message);
        Session::flash('type', $type);
        return redirect('/faculty/no-dues');
    }

    public function deleteNoDue($id) {

        $user = StudentNoDues::find($id);
        $user->delete();
        Session::flash('message', 'Deleted Successfully.');
        Session::flash('type', 'success');
        return redirect('/faculty/no-dues');
    }

    public function clearNoDue($id, $nm) {

        DB::table('StudentNoDues')->where('id', $id)->update(['NoDueClearedBy' => $nm, 'isCleared' => 'Y']);



        Session::flash('message', 'No dues Cleared Successfully.');
        Session::flash('type', 'success');
        return redirect('/faculty/no-dues');
    }

    public function verifyOldStudent() {

        $dept = Auth::user()->department;
        $shares['student'] = DB::table('StudentBasic')
                ->join('users', 'users.id', '=', 'StudentBasic.RegUserID')
                ->where(array('StudentBasic.isActive' => 'N', 'StudentBasic.AdmissionBranch' => $dept, 'users.studenttype' => 'oldstudent'))
//                ->where('StudentBasic.AdmissionBranch', '=', $dept)
                ->get();
//,'users.studenttype' => 'oldstudent'



        return view('faculty.verify_old_student', $shares);
    }

    public function viewStudentForSemester() {

        $dept = Auth::user()->department;
        $shares['student'] = DB::table('StudentBasic')
                ->join('users', 'users.id', '=', 'StudentBasic.RegUserID')
                ->join('StudentSemReg', 'StudentSemReg.userID', '=', 'users.id')
                ->where(array('StudentBasic.AdmissionBranch' => $dept))
                ->get();
        return view('faculty.view_student_for_semester', $shares);
    }

    public function approveStudentForDownload($stid) {
        DB::table('StudentSemReg')->where('userID', $stid)->update(['UpdateToken' => 'Y']);
        Session::flash('message', 'Student Activated For Downlaod PDF');
        Session::flash('type', 'success');
        return redirect('/faculty/verify-old-student');
    }

    public function OldStudentVerifySave($stid, $uid) {

        DB::table('StudentBasic')->where('studentID', $stid)->update(['isActive' => 'Y', 'Token' => 'Y']);
        DB::table('StudentSemReg')->where('userID', $uid)->update(['Verified' => 'Y']);
        Session::flash('message', 'Student Activated On Registration Portal');
        Session::flash('type', 'success');
        return redirect('/faculty/verify-old-student');
    }

    public function deleteOldStudent($stid, $uid) {
        return redirect('/faculty/verify-old-student');
    }

}
