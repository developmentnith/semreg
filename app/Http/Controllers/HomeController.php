<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('student.dashboard'); 
//        return view('home'); 
    }

    public function showRegister() {
        // show the form
        //return View::make('register');
    }

    public function doRegister() {
// process the form
    }

}
