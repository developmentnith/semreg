<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubjectMaster;
use Session;
use Illuminate\Support\Facades\Response;

class SubjectMasterController extends Controller {

    public function __construct() {
        view()->share('type', 'admin');
    }

    //Subject Master Controller
    public function index() {

        $data['subject'] = SubjectMaster::all();
        return view('admin.subjectmaster', $data);
    }

    public function addSubject(Request $request) {
        $subject = new SubjectMaster;
        $subject->SubjectName = $request->subjectname;
        $subject->SubjectCode = $request->subjectcode;
        $subject->credit = $request->credit;
        $subject->TheoryPractical = $request->theorypractical;
        $subject->Degree = $request->degree;
        $subject->Semester = $request->semester;
        $subject->Group = $request->group;
        $subject->SubjectType = $request->SubjectType;
        $subject->TotalLectureTheory = $request->totallecturetheory;
        $subject->TotalLabPractical = $request->totallecturePractical;
        $subject->isActive = $request->isactive;
        $subject->save();
        Session::flash('message', 'Saved Successfully.');
        Session::flash('type', 'success');
        return redirect('/admin/subject');
    }

    public function deleteSubject($id) {
        $user = SubjectMaster::find($id);
        $user->delete();
        Session::flash('message', 'Deleted Successfully.');
        Session::flash('type', 'success');
        return redirect('/admin/subject');
    }

}
