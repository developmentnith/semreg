<?php

namespace App\Http\Controllers\Admin;

use App\User;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\StudentBasic;
use App\OpenElectivePriority;
use App\RegistrationDates;
use App\ImportFilesDetails;
use App\StudentSemReg;
use App\StudentRollNumber;
use App\CancelRollNumber;
use App\OpenElectiveChoices;
use App\SemesterWiseSGPICGPI;
use App\OpenElectiveAllotment;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helpers;
use App\Exports\SGPICGPIExport;
use App\Imports\AdminImport;
use App\Imports\Studentsgpicgpi;
use Session;
use Excel;
use Illuminate\Support\Facades\Validator;

class AdminDashboardController extends Controller {

    public function __construct() {
        
    }

    public function index() {

        $typeauth = $this->checkAdmin();
        if ($typeauth == 'admin') {
            return view('admin.dashboard');
        } else {
            Auth::logout();
            return redirect('login');
        }
    }

    public function registerStudent() {

        $data['students'] = StudentBasic::all();
        $data['populatebranshort'] = Helpers::populateComboOne('DepartmentMaster');
        return view('admin.registerstudent', $data);
    }

    public function AllRegisterStudent() {

        $data['students'] = StudentBasic::all();
        $data['populatebranshort'] = Helpers::populateComboOne('DepartmentMaster');
        return view('admin.allregisterstudent', $data);
    }

    public function canceledStudents() {
        $data['students'] = DB::table('StudentBasic')->where('cancelstatus', 'canceled')->get();
        $data['populatebranshort'] = Helpers::populateComboOne('DepartmentMaster');
        return view('admin.canceledstudents', $data);
    }

    public function updateStudent(Request $request) {
        if ($request->autorollnubmer == 'auto') {

            $data = ['IsActive' => 'Y',
                'Status' => 'Y',
                'Token' => 'Y',
                'FileNumber' => $request->filenumber,
                'RegistrationNumber' => $request->regNumber,
                'DataChcekedBy' => Auth::user()->name
            ];
            DB::table('StudentBasic')->where('studentID', $request->studentId)->update($data);
            $studentRollData = DB::table('StudentRollNumber')->where('StudentID', $request->studentId)->get();
            if ($studentRollData->isEmpty()) {
                $message = "Student Data Missing";
                $type = 'error';
                Session::flash('message', $message);
                Session::flash('type', $type);
                return redirect('/admin/registred-students');
            } else {
                $userrolln = Helpers::getUserRollNumberFromAlloted($request->studentId);
                DB::table('StudentBasic')->where('studentID', $request->studentId)->update(['InstituteRollNumber' => $userrolln]);
                $usid = Helpers::getUserIDFromStudentID($request->studentId);
                $rollNumberAllot = new StudentSemReg;
                //$rollNumberAllot->userID = $studentRollData[0]->StudentID;
                $rollNumberAllot->userID = $usid;
                $rollNumberAllot->RollNumber = $studentRollData[0]->InstituteRollNumber;
                $rollNumberAllot->currentSem = '1';
                $rollNumberAllot->Semester = '';
                $rollNumberAllot->SGPI = '';
                $rollNumberAllot->CGPI = '';
                $rollNumberAllot->PGSGPI = '';
                $rollNumberAllot->PGCGPI = '';
                $rollNumberAllot->ApproveSemNum = '';
                $rollNumberAllot->UpdateToken = 'N';
                $rollNumberAllot->Verified = 'Y';
                $rollNumberAllot->RegYear = date('Y');
                $rollNumberAllot->Session = env('CURRENT_SESSION', 'default_value');
                $rollNumberAllot->save();
                $message = "Data Updated Successfully.";
                $type = 'success';
                Session::flash('message', $message);
                Session::flash('type', $type);
                return redirect('/admin/registred-students');
            }
        } else {
            if ($request->rollnumber) {
//                $rollinfo = DB::table('StudentBasic')->where('RegistrationNumber', $request->rollnumber)->get();
                $rollinfo = DB::table('StudentRollNumber')->where('InstituteRollNumber', $request->rollnumber)->get();
                if ($rollinfo->isEmpty()) {
                    $data = ['IsActive' => 'Y',
                        'Status' => 'Y',
//                        'Token' => 'Y',
                        'FileNumber' => $request->filenumber,
                        'RegistrationNumber' => $request->regNumber,
                        'InstituteRollNumber' => $request->rollnumber,
                        'DataChcekedBy' => Auth::user()->name
                    ];
                    DB::table('StudentBasic')->where('studentID', $request->studentId)->update($data);
                    $studentData = DB::table('StudentBasic')->where('studentID', $request->studentId)->get();
                    $rollNumber = new StudentRollNumber;
                    $rollNumber->StudentID = $request->studentId;
                    $rollNumber->InstituteRollNumber = $request->rollnumber;
                    $rollNumber->StudentName = $studentData[0]->StudentName;
                    $rollNumber->Degree = $studentData[0]->AdmissionIN;
                    $rollNumber->Branch = $studentData[0]->AdmissionBranch;
                    $rollNumber->Grop = $request->group;
                    $rollNumber->Section = $request->section;
                    $rollNumber->AdmissionYear = env('ADMISSION_YEAR', 'default_value');
                    $rollNumber->save();
                    $message = "Data Updated Successfully.";
                    $type = 'success';
                    Session::flash('message', $message);
                    Session::flash('type', $type);
                    return redirect('/admin/registred-students');
                } else {
                    $message = "Data Not Updated Roll Number Already Exists";
                    $type = 'error';
                    Session::flash('message', $message);
                    Session::flash('type', $type);
                    return redirect('/admin/registred-students');
                }
            }
        }
    }

    public function allUpdateStudent(Request $request) {
        if ($request->autorollnubmer == 'auto') {

            $data = ['IsActive' => 'Y',
                'Status' => 'Y',
                'Token' => 'Y',
                'FileNumber' => $request->filenumber,
                'RegistrationNumber' => $request->regNumber,
                'DataChcekedBy' => Auth::user()->name
            ];
            DB::table('StudentBasic')->where('studentID', $request->studentId)->update($data);
            $studentRollData = DB::table('StudentRollNumber')->where('StudentID', $request->studentId)->get();
            if ($studentRollData->isEmpty()) {
                $message = "Student Data Missing";
                $type = 'error';
                Session::flash('message', $message);
                Session::flash('type', $type);
                return redirect('/admin/all-registred-students');
            } else {
                $userrolln = Helpers::getUserRollNumberFromAlloted($request->studentId);
                DB::table('StudentBasic')->where('studentID', $request->studentId)->update(['InstituteRollNumber' => $userrolln]);
                $usid = Helpers::getUserIDFromStudentID($request->studentId);
                $rollNumberAllot = new StudentSemReg;
                //$rollNumberAllot->userID = $studentRollData[0]->StudentID;
                $rollNumberAllot->userID = $usid;
                $rollNumberAllot->RollNumber = $studentRollData[0]->InstituteRollNumber;
                $rollNumberAllot->currentSem = '1';
                $rollNumberAllot->Semester = '';
                $rollNumberAllot->SGPI = '';
                $rollNumberAllot->CGPI = '';
                $rollNumberAllot->PGSGPI = '';
                $rollNumberAllot->PGCGPI = '';
                $rollNumberAllot->ApproveSemNum = '';
                $rollNumberAllot->UpdateToken = 'N';
                $rollNumberAllot->Verified = 'Y';
                $rollNumberAllot->RegYear = date('Y');
                $rollNumberAllot->Session = env('CURRENT_SESSION', 'default_value');
                $rollNumberAllot->save();
                $message = "Data Updated Successfully.";
                $type = 'success';
                Session::flash('message', $message);
                Session::flash('type', $type);
                return redirect('/admin/all-registred-students');
            }
        } else {
            if ($request->rollnumber) {
//                $rollinfo = DB::table('StudentBasic')->where('RegistrationNumber', $request->rollnumber)->get();
                $rollinfo = DB::table('StudentRollNumber')->where('InstituteRollNumber', $request->rollnumber)->get();
                if ($rollinfo->isEmpty()) {
                    $data = ['IsActive' => 'Y',
                        'Status' => 'Y',
//                         'Token' => 'Y',
                        'FileNumber' => $request->filenumber,
                        'RegistrationNumber' => $request->regNumber,
                        'InstituteRollNumber' => $request->rollnumber,
                        'DataChcekedBy' => Auth::user()->name
                    ];
                    DB::table('StudentBasic')->where('studentID', $request->studentId)->update($data);
                    $studentData = DB::table('StudentBasic')->where('studentID', $request->studentId)->get();
                    $rollNumber = new StudentRollNumber;
                    $rollNumber->StudentID = $request->studentId;
                    $rollNumber->InstituteRollNumber = $request->rollnumber;
                    $rollNumber->StudentName = $studentData[0]->StudentName;
                    $rollNumber->Degree = $studentData[0]->AdmissionIN;
                    $rollNumber->Branch = $studentData[0]->AdmissionBranch;
                    $rollNumber->Grop = $request->group;
                    $rollNumber->Section = $request->section;
                    $rollNumber->AdmissionYear = env('ADMISSION_YEAR', 'default_value');
                    $rollNumber->save();
                    $message = "Data Updated Successfully.";
                    $type = 'success';
                    Session::flash('message', $message);
                    Session::flash('type', $type);
                    return redirect('/admin/all-registred-students');
                } else {
                    $message = "Data Not Updated Roll Number Already Exists";
                    $type = 'error';
                    Session::flash('message', $message);
                    Session::flash('type', $type);
                    return redirect('/admin/all-registred-students');
                }
            }
        }
    }

    public function cancelUpdateStudent(Request $request) {
        if ($request->rollnumber) {
            $rollinfo = DB::table('StudentRollNumber')->where('InstituteRollNumber', $request->rollnumber)->get();
            if ($rollinfo->isEmpty()) {
                $data = ['IsActive' => 'Y',
                    'Status' => 'Y',
//                         'Token' => 'Y',
                    'FileNumber' => $request->filenumber,
                    'RegistrationNumber' => $request->regNumber,
                    'InstituteRollNumber' => $request->rollnumber,
                    'DataChcekedBy' => Auth::user()->name,
                    'cancelstatus' => 'Active'
                ];
                DB::table('StudentBasic')->where('studentID', $request->studentId)->update($data);
                $studentData = DB::table('StudentBasic')->where('studentID', $request->studentId)->get();
                $rollNumber = new StudentRollNumber;
                $rollNumber->StudentID = $request->studentId;
                $rollNumber->InstituteRollNumber = $request->rollnumber;
                $rollNumber->StudentName = $studentData[0]->StudentName;
                $rollNumber->Degree = $studentData[0]->AdmissionIN;
                $rollNumber->Branch = $studentData[0]->AdmissionBranch;
                $rollNumber->Grop = $request->group;
                $rollNumber->Section = $request->section;
                $rollNumber->AdmissionYear = env('ADMISSION_YEAR', 'default_value');
                $rollNumber->save();
                DB::table('CancelStudentRollNumber')->where(array('StudentID' => $request->studentID))->delete();
                $message = "Data Updated Successfully.";
                $type = 'success';
                Session::flash('message', $message);
                Session::flash('type', $type);
                return redirect('/admin/all-registred-students');
            } else {
                $message = "Data Not Updated Roll Number Already Exists";
                $type = 'error';
                Session::flash('message', $message);
                Session::flash('type', $type);
                return redirect('/admin/all-registred-students');
            }
        }
    }

    public function viewStudents($id) {
        $data['studentdata'] = DB::table('StudentBasic')->where('StudentID', $id)->get();
        return view('admin.viewstudent', $data);
    }

    public function deleteStudents($id) {
        $student = StudentBasic::find($id);
        $student->delete();
        Session::flash('message', 'Deleted Successfully.');
        Session::flash('type', 'success');
        return redirect('/admin/registred-students');
    }

    public function importExcel() {
        $data['importfile'] = ImportFilesDetails::all();
        return view('admin.importexcel', $data);
    }

    public function generatePDF($id) {
        $data = ['title' => 'Welcome to NITH'];
//        return view('admin.myPDF', $data);
        $data['studentbasic'] = DB::table('StudentBasic')->where('studentID', $id)->orderBy('studentID', 'asc')->get();
        $data['StudentNumber'] = DB::table('StudentRollNumber')->where('StudentID', $id)->orderBy('studentID', 'asc')->get();
        $data['branchName'] = DB::table('DepartmentMaster')->select('DepartmentFullName')->where('DepartmentShortName', $data['studentbasic'][0]->AdmissionBranch)->get();
//        return view('admin.myPDF', $data);   
        $pdf = PDF::loadView('admin.myPDF', $data);
        return $pdf->download('NITHdown.pdf');
    }

    public function openElective() {
        $data['degree'] = DB::table('electiveSemsterConfig')->select('Degree')->distinct()->where('electiveType', 'OPEN ELECTIVE')->get();
        $data['openelective'] = DB::table('electiveSemsterConfig')->select('Semester')->distinct()->where('electiveType', 'OPEN ELECTIVE')->get();
        $data['yer'] = DB::table('DeptOpenElectiveConfig')->select('FloatYear')->distinct()->where('isActive', 'Y')->get();
        $data['AllotmentDetail'] = DB::table('OpenElectiveAllotment')->select('id', 'RollNumber', 'SubjectID', 'SubjectName', 'Department', 'ChoiceNum', 'Semester', 'Year', 'Status', 'Token')->where('Status', 'Y')->get();
        return view('admin.openelective', $data);
    }

    public function openElectiveAllotment(Request $request) {
        $id = Auth::user()->id;
        Validator::make($request->all(), [
            'degree' => 'required',
            'semester' => 'required',
            'year' => 'required',
        ])->validate();
        $dg = $request->degree;
        $sm = $request->semester;
        $nos = $request->nos;
        
        $yr = $request->year;
        
        if ($sm == "S03") {
            $cgpisem = "S02";
        } elseif ($sm == "S04") {
            $cgpisem = "S03";
        } elseif ($sm == "S05") {
            $cgpisem = "S04";
        } elseif ($sm == "S06") {
            $cgpisem = "S05";
        } elseif ($sm == "S07") {
            $cgpisem = "S06";
        } elseif ($sm == "S08") {
            $cgpisem = "S07";
        } else {
            $cgpisem = "S01";
        }
               
        //$execute = DB::select( 'call OpenElectiveAllotment('S05',2019)')
               
        DB::select('call OpenElectiveAllotment(?,?,?,?)', array($sm,$cgpisem,$yr,$nos));
        $data['degree'] = DB::table('electiveSemsterConfig')->select('Degree')->distinct()->where('electiveType', 'OPEN ELECTIVE')->get();
        $data['openelective'] = DB::table('electiveSemsterConfig')->select('Semester')->distinct()->where('electiveType', 'OPEN ELECTIVE')->get();
        $data['yer'] = DB::table('DeptOpenElectiveConfig')->select('FloatYear')->distinct()->where('isActive', 'Y')->get();
        
        $data['AllotmentDetail'] = DB::table('OpenElectiveAllotment')->select('id', 'RollNumber', 'SubjectID', 'SubjectName', 'Department', 'ChoiceNum', 'Semester', 'Year', 'Status', 'Token')->where('Status', 'Y')->get();
        flashy()->success('Open Elective Subject Allotment has been made Sucessfully...');
        return view('admin.openelective', $data);
    }

    public function deleteOpenpelectiveAllotted($param) {
        echo $param;
        die();
    }

    public function checkAdmin() {
        $id = Auth::user()->id;
        $typeauth = Helpers::getType($id);
        return $typeauth;
    }

    function popup($page_name = '', $param2 = '') {
        $rollNumber = OpenElectiveAllotment::find($param2)->RollNumber;
        $data['choices'] = DB::table('OpenElectivePriority')->select('ChoiceNum', 'SubjectID')->where('RollNumber', $rollNumber)->get();
        $data['degree'] = DB::table('electiveSemsterConfig')->select('Degree')->distinct()->where('electiveType', 'OPEN ELECTIVE')->get();
        $data['openelective'] = DB::table('electiveSemsterConfig')->select('Semester')->distinct()->where('electiveType', 'OPEN ELECTIVE')->get();
        $data['yer'] = DB::table('DeptOpenElectiveConfig')->select('FloatYear')->distinct()->where('isActive', 'Y')->get();
        $data['param2'] = $param2;
        return view('admin.' . $page_name, $data);
    }

    function updatePopup($page_name = '', $param2 = '') {
        $data['studentid'] = $param2;
        return view('admin.' . $page_name, $data);
    }

    function cancelAlotPopup($page_name = '', $param2 = '') {
        $data['studentid'] = $param2;
        $data['result'] = DB::table('StudentBasic')->where('studentID', $param2)->get();
        $data['resultdata'] = DB::table('CancelStudentRollNumber')->where('StudentID', $param2)->get();
        return view('admin.' . $page_name, $data);
    }

    public function editOpenElectiveAllotment(Request $request) {
        $id = Auth::user()->id;
        $rollnumber = $request->input('rollnumber');
        $subjectid = $request->input('subjectid');
        $chnum = $request->input('choice_number');
        $subjectname = $request->input('subjectname');
        $department = $request->input('department');
        $semester = $request->input('semester');
        $year = $request->input('year');
        $rid = $request->input('rid');
        //  DB::update('update OpenElectiveAllotment set SubjectID = ?,SubjectName = ?,ChoiceNum = ?,Semester = ?,Year = ?, Department = ? ,isUpdated = ? where id = ?' , [$subjectid,$subjectname,$chnum,$semester,$year,$department,$rid,'Y']);
        $Allot = OpenElectiveAllotment::find($rid);
        if (empty($Allot)) {// you can do this condition to check if is empty
            $Allot = new OpenElectiveAllotment; //then create new object
        }
        $Allot->SubjectID = $subjectid;
        $Allot->SubjectName = $subjectname;
        $Allot->Semester = $semester;
        $Allot->Year = $year;
        $Allot->Department = $department;
        $Allot->ChoiceNum = $chnum;
        $Allot->isUpdated = 'Y';
        $Allot->save();
        DB::update('update OpenElectivePriority set Allotted = ?,Semester=?,year=? where RollNumber = ?', ['N', $semester, $year, $rollnumber]);
        DB::update('update OpenElectivePriority set Allotted = ? where RollNumber = ? and ChoiceNum=? and Semester=? and year=?', ['Y', $rollnumber, $chnum, $semester, $year]);
        Session::flash('message', 'Data Successfully. Updated');
        Session::flash('type', 'success');
        return redirect('/admin/open-elective');
    }

    public function GetOpenElectiveChoice(Request $request) {
        $rollnum = $request->get('RollNumber');
        $choicenum = $request->get('ChoiceNum');
        $semester = $request->get('Semester');
        $detail = DB::table('OpenElectivePriority')->where(array('RollNumber' => $rollnum, 'ChoiceNum' => $choicenum, 'Semester' => $semester))->get();
        return ['status' => true, 'result' => $detail];
    }

    public function createUser() {
        $data['users'] = User::all();
        $data['populatebranshort'] = Helpers::populateComboOne('DepartmentMaster');
        return view('admin.createuser', $data);
    }

    public function addUser(Request $request) {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->department = $request->department;
        $user->phone = '123456789';
        $user->type = $request->type;
        $user->role = implode(',', $request->role);
        $user->password = Hash::make($request->password);
        $res = $user->save();
        Session::flash('message', 'Data Add Successfully');
        Session::flash('type', 'success');
        return redirect('/admin/create-user');
    }

    function deleteUser($id) {
        $user = User::find($id);
        $user->delete();
        Session::flash('message', 'Deleted Successfully.');
        Session::flash('type', 'success');
        return redirect('/admin/create-user');
    }

    function editPopUp($page_name = '', $param2 = '') {
        if ($page_name == 'edit_subject') {
            $data['param2'] = $param2;
            return view('admin.' . $page_name, $data);
        } elseif ($page_name == 'edit_student') {
            $populatebranshort = Helpers::populateComboOne('DepartmentMaster');
            $data['populatebranshort'] = $populatebranshort;
            $data['result'] = DB::table('StudentBasic')->where('studentID', $param2)->get();
            $data['resultdata'] = DB::table('StudentRollNumber')->where('StudentID', $param2)->get();
            return view('admin.' . $page_name, $data);
        } elseif ($page_name == 'all_edit_student') {
            $populatebranshort = Helpers::populateComboOne('DepartmentMaster');
            $data['populatebranshort'] = $populatebranshort;
            $data['result'] = DB::table('StudentBasic')->where('studentID', $param2)->get();
            $data['resultdata'] = DB::table('StudentRollNumber')->where('StudentID', $param2)->get();
            return view('admin.' . $page_name, $data);
        } else {
            $data['populatebranshort'] = Helpers::populateComboOne('DepartmentMaster');
            $data['param2'] = $param2;
            return view('admin.' . $page_name, $data);
        }
    }

    function cancelPopup($page_name = '', $param2 = '', $param3 = '') {
        $data['studentID'] = $param2;
        $data['studentRollNo'] = $param3;
        return view('admin.' . $page_name, $data);
    }

    public function studentSgpiCgpi() {
        $results['duplicate'] = DB::select('SELECT a.* FROM SemesterWiseSGPICGPI  a JOIN (SELECT RollNumber, Semester, COUNT(*) FROM SemesterWiseSGPICGPI Where Status=? GROUP BY Semester,RollNumber HAVING count(*) > 1 ) b ON a.RollNumber = b.RollNumber AND a.Semester = b.Semester and a.Status=? ORDER BY a.Semester', ['Y', 'Y']);
        $results['sgpicgpi'] = DB::table('SemesterWiseSGPICGPI')->where('Status', 'Y')->get();
        return view('admin.student_sgpi_cgpi', $results);
    }

    public function updateDuplicateSGPI() {
        $res = DB::select('SELECT MIN(RID) MinID  FROM SemesterWiseSGPICGPI where Status=?  GROUP BY RollNumber,Semester HAVING COUNT(Semester) > 1', ['Y']);
        foreach ($res as $key => $value) {
            DB::update('UPDATE SemesterWiseSGPICGPI Set Status=? where RID=?', ['N', $value->MinID]);
        }
        $res1 = DB::select('SELECT MIN(RID) MinID  FROM SemesterWiseSGPICGPI where Status=?  GROUP BY RollNumber,Semester HAVING COUNT(Semester) > 1', ['Y']);
        foreach ($res1 as $key => $value) {
            DB::update('UPDATE SemesterWiseSGPICGPI Set Status=? where RID=?', ['N', $value->MinID]);
        }
        Session::flash('message', 'All Duplicate SGPI/CGPI Deactivated Suceesfully');
        Session::flash('type', 'success');
        return redirect('/admin/student-sgpi-cgpi');
    }

    public function downloadExcelSGPICGPI($type) {
        return Excel::download(new SGPICGPIExport, 'sgpicgpiExport.' . $type);
    }

    public function addExcel(Request $request) {
        if ($file = $request->file('exceldata')) {
            $filename = $file->getClientOriginalName();
            $file->move('upload/studentphoto', $filename);
            $filePath = 'upload/studentphoto/' . $filename;
            if (!file_exists($filePath) || !is_readable($filePath))
                return FALSE;
            if ($request->table_name == 1)
                $data = Excel::import(new AdminImport, $filePath);
            else
                $data = Excel::import(new Studentsgpicgpi, $filePath);
            if ($data) {
                $import = new ImportFilesDetails();
                $import->fileName = $request->filenames;
                $import->fileExtension = $filename;
                $import->status = 'Active';
                $res = $import->save();
                Session::flash('message', 'Import Data Successfully');
                Session::flash('type', 'success');
                return redirect('/admin/import-excel');
            }
        }
        return back();
    }

    public function dateConfiguration() {

        $id = Auth::user()->id;

        $detail = DB::table('RegistrationDates')->where('Status', 'Y')->get();
        $detail1 = DB::table('RegistrationDates')->where('Status', 'N')->get();
        $username = Helpers::getUserFullName($id);
        $course = Helpers::getUserCourse($id);
        $studenttyp = Helpers::getUserType($id);
        $populateyear = Helpers::populateCombo('gateyear', 'gateyr');
        $populatestate = Helpers::populateCombo('state_list', 'state');
        $populatecountry = Helpers::populateCombo('countries', 'name');
        $populatebranch = Helpers::populateCombo('Branch_Spec', 'department');
        $populatebranshort = Helpers::populateComboOne('DepartmentMaster');
        $populatespec = Helpers::populateCombo('Branch_Spec', 'specilization');
        $data['populatestate'] = $populatestate;
        $data['populatecountry'] = $populatecountry;
        $data['populatebranch'] = $populatebranch;
        $data['populatespec'] = $populatespec;
        $data['userid'] = $id;
        $data['populateyear'] = $populateyear;
        $data['course'] = $course;
        $data['studenttyp'] = $studenttyp;
        $data['populatebranshort'] = $populatebranshort;
        $data['detail'] = $detail;
        $data['detail1'] = $detail1;
        return view('admin.date_configuration', $data);
    }

    public function dateStartEndSemester(Request $request) {
        $datesave = new RegistrationDates();
        $type = $request->input('type');
        $degree = $request->input('degree');
        $department = $request->input('department');
        $session = $request->input('session');
        $semester = $request->input('semester');
        // $startdate = date("d-m-Y", strtotime($request->input('startdate')));
        //$enddate = date("d-m-Y", strtotime($request->input('enddate')));

        $startdate = $request->input('startdate');
        $enddate = $request->input('enddate');



        if ($type == 'DEPARTMENT ELECTIVE') {
            $depelective = DB::table('electiveSemsterConfig')->where(array('Semester' => $semester, 'department' => $department, 'Degree' => $degree, 'electiveType' => 'DEPARTMENT ELECTIVE'))->count();
            if ($depelective <= 0) {
                Session::flash('message', 'DEPARTMENT ELECTIVE for this Semester Not Exist in Config Table');
                Session::flash('type', 'success');
                return redirect('/admin/date-configuration');
            }
        }

        if ($type == 'OPEN ELECTIVE') {
            $depelective = DB::table('electiveSemsterConfig')->where(array('Semester' => $semester, 'department' => $department, 'Degree' => $degree, 'electiveType' => 'OPEN ELECTIVE'))->count();
            if ($depelective <= 0) {
                Session::flash('message', 'OPEN ELECTIVE for this Semester Not Exist in Config Table');
                Session::flash('type', 'success');
                return redirect('/admin/date-configuration');
            }
        }
        $date_now = date("Y-m-d");
        if ($date_now < $startdate) {
            Session::flash('message', 'Start Date Cand not be Less than Current Date');
            Session::flash('type', 'success');
            // return redirect('/admin/date-configuration');
        }

        $datesave->Type = $type;
        $datesave->Degree = $degree;
        $datesave->Department = $department;
        $datesave->Session = $session;
        $datesave->Semester = $semester;
        $datesave->StartDate = $startdate;
        $datesave->EndDate = $enddate;

        $datesave->save();

        //When Dates are Declared for semester Registration ApproveToken in Semester Registration

        if ($type == 'SEMESTER') {
            //$dept = Auth::user()->department;
            $rs = DB::table('StudentBasic')
                            ->join('StudentSemReg', 'StudentSemReg.userID', '=', 'StudentBasic.RegUserID')
                            ->where(array('StudentBasic.isActive' => 'Y', 'StudentBasic.AdmissionBranch' => $department))->update(['StudentSemReg.ApproveToken' => 'Y']);
//                ->where('StudentBasic.AdmissionBranch', '=', $dept)
        }









        $detail = DB::table('RegistrationDates')->where('Status', 'Y')->get();
        return redirect('/admin/date-configuration');
    }

    public function deleteRegistrationDate($rid) {
        $res = RegistrationDates::where('RID', $rid)->delete();
        Session::flash('message', 'Data Deleted Successfully');
        Session::flash('type', 'success');
        return redirect('/admin/date-configuration');
    }

    public function deactivateRegistrationDate($rid) {
        $res = DB::update('UPDATE RegistrationDates Set Status=? where RID=?', ['N', $rid]);
        Session::flash('message', 'Data Deactivated Successfully');
        Session::flash('type', 'success');
        return redirect('/admin/date-configuration');
    }

    public function activateRegistrationDate($rid) {
        $res = DB::update('UPDATE RegistrationDates Set Status=? where RID=?', ['Y', $rid]);
        Session::flash('message', 'Data Activated Successfully');
        Session::flash('type', 'success');
        return redirect('/admin/date-configuration');
    }

    public function CancelOpenElectiveAllotment() {


        $res = OpenElectiveAllotment::where('Token', 'N')->delete();
        //  DB::table('OpenElectiveAllotment')->where('Token', 'N')->update(['Status' => 'Y', 'Allotted' => 'N']); 
        $res1 = OpenElectivePriority::where('Token', 'N')->update(['Status' => 'Y', 'Allotted' => 'N']);
//        DB::update('UPDATE OpenElectiveAllotment Set Status=? ,Allotted=? where Token=?', ['Y','N', 'Y']);

        Session::flash('message', 'All Un-Finalized Allotment Deleted Successfully');
        Session::flash('type', 'success');
        return redirect('/admin/open-elective');
    }

    public function FinalizeOpenElectiveAllotment() {

        // $res = OpenElectiveAllotment::where('Token', 'N')->update('Token','Y');
        // die('helol');
        $res1 = OpenElectiveAllotment::where('Token', 'N')->update(['Token' => 'Y']);

        Session::flash('message', ' Allotment Finalized Sucessfully Successfully');
        Session::flash('type', 'success');
        return redirect('/admin/open-elective');
    }

    public function editStudent(Request $request) {
        if ($request->StudentName == 'dep') {
            $data = [
                'StudentName' => $request->StudentName,
                'FatherName' => $request->fathername,
                'FatherNameHindi' => $request->fathernamehinid,
                'MotherName' => $request->mothername,
                'MotherNameHindi' => $request->mothernamehindi,
                'InstituteRollNumber' => $request->InstituteRollNumber,
                'EmailIDStudent' => $request->EmailIDStudent,
                'Gender' => $request->Gender,
                'AdmissionBranch' => $request->AdmissionBranch,
            ];
            $data1 = [
                'InstituteRollNumber' => $request->InstituteRollNumber,
                'Branch' => $request->AdmissionBranch,
                'Grop' => $request->group,
                'Section' => $request->section,
            ];
            DB::table('StudentBasic')->where('studentID', $request->studentID)->update($data);
            DB::table('StudentRollNumber')->where('StudentID', $request->studentID)->update($data1);
            $message = "Data Updated Successfully.";
            $type = 'success';
            Session::flash('message', $message);
            Session::flash('type', $type);
            return redirect('/admin/registred-students');
        } else {
            $data = [
                'StudentName' => $request->StudentName,
                'FatherName' => $request->fathername,
                'FatherNameHindi' => $request->fathernamehinid,
                'MotherName' => $request->mothername,
                'MotherNameHindi' => $request->mothernamehindi,
                'InstituteRollNumber' => $request->InstituteRollNumber,
                'EmailIDStudent' => $request->EmailIDStudent,
                'Gender' => $request->Gender,
                'AdmissionBranch' => $request->AdmissionBranch,
            ];
            $data1 = [
                'InstituteRollNumber' => $request->InstituteRollNumber,
                'Branch' => $request->AdmissionBranch,
                'Grop' => $request->group,
                'Section' => $request->section,
            ];
            DB::table('StudentBasic')->where('studentID', $request->studentID)->update($data);
            DB::table('StudentRollNumber')->where('StudentID', $request->studentID)->update($data1);
            $message = "Data Updated Successfully.";
            $type = 'success';
            Session::flash('message', $message);
            Session::flash('type', $type);
            return redirect('/admin/all-registred-students');
        }
    }

    public function editSubject(Request $request) {
        $data = [
            'SubjectName' => $request->subjectname,
            'SubjectCode' => $request->subjectcode,
            'credit' => $request->credit,
            'TheoryPractical' => $request->theorypractical,
            'Degree' => $request->degree,
            'Semester' => $request->semester,
            'Group' => $request->group,
            'SubjectType' => $request->SubjectType,
            'TotalLectureTheory' => $request->totallecturetheory,
            'TotalLabPractical' => $request->totallecturePractical,
            'isActive' => $request->isactive,
        ];
        DB::table('SubjectMaster')->where('id', $request->subjectid)->update($data);
        $message = "Data Updated Successfully.";
        $type = 'success';
        Session::flash('message', $message);
        Session::flash('type', $type);
        return redirect('/admin/subject');
    }

//    public function cancelRollNumber($studentID, $rollNubmer) {
//        $studentRollData = DB::table('StudentRollNumber')->where(array('StudentID' => $studentID, 'InstituteRollNumber' => $rollNubmer))->get();
//        if (!$studentRollData->isEmpty()) {
//            $rollNumber = new CancelRollNumber;
//            $rollNumber->StudentID = $studentRollData[0]->StudentID;
//            $rollNumber->InstituteRollNumber = $studentRollData[0]->InstituteRollNumber;
//            $rollNumber->StudentName = $studentRollData[0]->StudentName;
//            $rollNumber->Degree = $studentRollData[0]->Degree;
//            $rollNumber->Branch = $studentRollData[0]->Branch;
//            $rollNumber->Grop = $studentRollData[0]->Grop;
//            $rollNumber->Section = $studentRollData[0]->Section;
//            $rollNumber->AdmissionYear = env('ADMISSION_YEAR', 'default_value');
//            $rollNumber->save();
//            $data = [
//                'cancelstatus' => 'Canceled',
//                'InstituteRollNumber' => 'xxxx',
//            ];
//            DB::table('StudentBasic')->where('studentID', $studentID)->update($data);
//            DB::table('StudentRollNumber')->where(array('StudentID' => $studentID, 'InstituteRollNumber' => $rollNubmer))->delete();
//            $message = "Data Canceled Successfully.";
//            $type = 'success';
//            Session::flash('message', $message);
//            Session::flash('type', $type);
//            return redirect('/admin/all-registred-students');
//        }
//    }

    public function newCancelRollNumber(Request $request) {
        if ($request->password == 'Nith$#0007') {
            $studentRollData = DB::table('StudentRollNumber')->where(array('StudentID' => $request->studentID, 'InstituteRollNumber' => $request->studentRollNo))->get();
            if (!$studentRollData->isEmpty()) {
                $rollNumber = new CancelRollNumber;
                $rollNumber->StudentID = $studentRollData[0]->StudentID;
                $rollNumber->InstituteRollNumber = $studentRollData[0]->InstituteRollNumber;
                $rollNumber->StudentName = $studentRollData[0]->StudentName;
                $rollNumber->Degree = $studentRollData[0]->Degree;
                $rollNumber->Branch = $studentRollData[0]->Branch;
                $rollNumber->Grop = $studentRollData[0]->Grop;
                $rollNumber->Section = $studentRollData[0]->Section;
                $rollNumber->AdmissionYear = env('ADMISSION_YEAR', 'default_value');
                $rollNumber->save();
                $data = [
                    'cancelstatus' => 'Canceled',
                    'InstituteRollNumber' => 'xxxx',
                ];
                DB::table('StudentBasic')->where('studentID', $request->studentID)->update($data);
                DB::table('StudentRollNumber')->where(array('StudentID' => $request->studentID, 'InstituteRollNumber' => $request->studentRollNo))->delete();
                $message = "Data Canceled Successfully.";
                $type = 'success';
                Session::flash('message', $message);
                Session::flash('type', $type);
                return redirect('/admin/all-registred-students');
            }
        } else {
            $message = "Password mismatch.";
            $type = 'error';
            Session::flash('message', $message);
            Session::flash('type', $type);
            return redirect('/admin/all-registred-students');
        }
    }

    public function testTable() {
        return view('admin.testtable'); 
    }

}
