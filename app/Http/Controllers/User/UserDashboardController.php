<?php

namespace App\Http\Controllers\User;

use App\User;
use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\StudentBasic;
use App\OpenElectivePriority;
use App\RegistrationDates;
use App\ImportFilesDetails;
use App\StudentSemReg;
use App\StudentRollNumber;
use App\OpenElectiveChoices;
use App\SemesterWiseSGPICGPI;
use App\OpenElectiveAllotment;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helpers;
use App\Exports\SGPICGPIExport;
use App\Imports\AdminImport;
use App\Imports\Studentsgpicgpi;
use Session;
use Excel;
use Illuminate\Support\Facades\Validator;

class UserDashboardController extends Controller {

    public function __construct() {
        
    }

    public function index() {

        $typeauth = $this->checkUser();
        if ($typeauth == 'user') {
            return view('user.dashboard');
        } else {
            Auth::logout();
            return redirect('login');
        }
    }

    public function checkUser() {
        $id = Auth::user()->id;
        $typeauth = Helpers::getType($id);
        return $typeauth;
    }

    public function AllRegisterStudent() {
        $data['students'] = StudentBasic::all();
        $data['populatebranshort'] = Helpers::populateComboOne('DepartmentMaster');
        return view('user.allregisterstudent', $data);
    }

}
