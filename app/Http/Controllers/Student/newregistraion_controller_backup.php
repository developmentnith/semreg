<?php

namespace App\Http\Controllers\Student;

use App\User;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\StudentBasic;
use App\StudentSemReg;
use App\DeptElectiveConfig;
use App\DepElectiveChoice;
use App\OpenElectiveChoices;
use App\OpenElectiveConfig;
use App\Helpers\Helpers;
use Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\PreventBackHistory;

class StudentDashboardController extends Controller {

    public function index() {
        $id = Auth::user()->id;



        $username = Helpers::getUserFullName($id);
        $userdegree = Helpers::getStudentDegree($id);
        $userbranch = Helpers::getUserBranch($id);
        $verifiedstatus = Helpers::getUserVerifiedStatus($id);
        if ($verifiedstatus == 'Y') {
            $verifiedstatus = 'YES';
        } else {
//            $verifiedstatus = 'NO';
            $verifiedstatus = 'YES';
        }

        $userstatus = Helpers::getUserStatus($id);
        $userrolln = Helpers::getUserRollNumber($id);
        $useremail = Helpers::getUserEmail($id);
//        if ($userrolln->isEmpty()) {
//            $data['userrolln'] = 'RollNumber Not Allotted';
//        }

        $ndue = DB::table('StudentNoDues')->where('RollNumber', '=', $userrolln)
                        ->where('isCleared', 'N')->get();
        $sgpicgpistudent = DB::table('StudentSemReg')->where('RollNumber', '=', $userrolln)
                        ->where('userID', $id)->get();
        $sgpicgpiadmin = DB::table('SemesterWiseSGPICGPI')->where('RollNumber', '=', $userrolln)
                        ->where('Status', 'Y')->orderBy('UGPGTag', 'DESC')->get();
        $results = DB::table('StudentBasic')->where('RegUserID', $id)->first();





        $data['nodues'] = $ndue;
        $data['username'] = $username;
        $data['userdeg'] = $userdegree;
        $data['userbranch'] = $userbranch;
        $data['userstatus'] = $userstatus;
        $data['userrolln'] = $userrolln;
        $data['useremail'] = $useremail;
        $data['verifiedstatus'] = $verifiedstatus;
        $data['sgpicgpistu'] = $sgpicgpistudent;
        $data['sgpicgpiadm'] = $sgpicgpiadmin;
        $data['studentbasicId'] = !empty($results->studentID) ? $results->studentID : '';


        $nextrow = '';
        $semdata = array();
        $reg = Helpers::getSemesterRegData($id);

        if ($reg->isEmpty()) {
            $totalSem = 0;
        } else {
            $nextrow = $reg[0]->currentSem;
            $semdata[] = explode(',', $reg[0]->SGPI);
            foreach ($semdata as $r) {
                $totalSem = count($r);
            }
        }

//        if (!empty($reg[0]->currentSem)) {
//            $nextrow = $reg[0]->currentSem;
//            $semdata[] = explode(',', $reg[0]->SGPI);
//            foreach ($semdata as $r) {
//                $totalSem = count($r);
//            }
        $data['course'] = Helpers::getUserCourse($id);
        if (!$reg->isEmpty()) {
            $data['now'] = $totalSem;
            $data['last'] = $reg[0]->currentSem;
            $data['active_status'] = $reg[0]->UpdateToken;
            $data['approved_sem'] = $reg[0]->ApproveSemNum;
            $data['semesterdata'] = DB::table('semesterMaster')->orderBy('semName', 'asc')->get();
        } else {
            $data['now'] = 0;
            $data['last'] = 1;
            $data['active_status'] = '';
            $data['approved_sem'] = '';
            $data['semesterdata'] = DB::table('semesterMaster')->orderBy('semName', 'asc')->get();
        }

        $studentData = DB::table('StudentBasic')->where('RegUserID', $id)->get();
        if ($studentData->isEmpty())
            return redirect('/student/reg-form');
        else
            return view('student.dashboard', $data);
    }

    public function showOpenElective() {
        $id = Auth::user()->id;
        if (!Helpers::isValidStudent($id)) {
            Session::flash('message', 'Your Registration Detail Has Not Been Activated Yet!');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        $userrolln = Helpers::getUserRollNumber($id);
        $userbranch = Helpers::getUserBranch($id);
        $sem = Helpers::getCurrentSemester($id);
        $deg = Helpers::getUserCourse($id);
        if ($sem->isEmpty()) {
            Session::flash('message', 'RollNumber/Semester not found semester registraion table!');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }

        if (empty($userbranch)) {
            Session::flash('message', 'Contact System Admin.(Admission Branch is Missing)');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }

        if (empty($deg)) {
            Session::flash('message', 'Branch/Degree is Not Configured for Student');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        ///check dates for open electives
        // $userrolln = $RollNum;
        $se = Helpers::getSemNumToText($sem[0]->currentSem);

        if (count($sem) <= 0) {
            Session::flash('message', 'RollNumber/Semester not found semester registraion table!');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        $nsem = Helpers::getSemNumToText($sem[0]->currentSem);

        $cdt = date("Y-m-d");
        $validDate = DB::table('RegistrationDates')
                ->where('Department', '=', $userbranch)
                ->Where('Semester', '=', $nsem)
//                ->Where(DB::raw("(DATE_FORMAT(StartDate,'%d-%m-%y'))"), '>=', $cdt)               
                ->Where('StartDate', '<=', $cdt)
                ->Where('EndDate', '>=', $cdt)
                ->Where('TYPE', '=', "OPEN ELECTIVE")
                ->Where('Degree', '=', $deg)
                ->Where('Status', '=', 'Y')
                ->get();
        ///end of date






        $openexist = DB::table('electiveSemsterConfig')
                ->where('Department', '=', $userbranch)
                ->Where('Degree', '=', $deg)
                ->Where('Semester', '=', $nsem)
                ->Where('electiveType', '=', "OPEN ELECTIVE")
                ->get();

        
//        echo "<pre>";
//         print_r($openexist);
//         die();

        if ($openexist->isEmpty()) {
            Session::flash('message', 'Open Elective For This Semester is Not Available');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        } else {
            $ChoiceExercised = DB::table('OpenElectivePriority')
                    ->where('RollNumber', '=', $userrolln)
                    ->Where('Semester', '=', $nsem)
                    ->Where('Status', '=', "Y")
                    ->get();
            if ($ChoiceExercised->isEmpty()) {
                if ($validDate->isEmpty()) {
                    Session::flash('message', 'Either the Open Elective Filling up Dates Dates are Over. OR the Registration is not started yet to fill the choices of Open Elective. ');
                    Session::flash('type', 'error');
                    return redirect('/student/dashboard');
                }
                $subjct = DB::table('DeptOpenElectiveConfig')
                        ->where('FloatedByDept', '!=', $userbranch)
                        ->where('Semester', '=', $nsem)
                        ->where('isActive', '=', "Y")
                        ->get();
                $totalRow = DB::table('DeptOpenElectiveConfig')
                        ->where('FloatedByDept', '!=', $userbranch)
                        ->where('Semester', '=', $nsem)
                        ->where('isActive', '=', "Y")
                        ->count();
                $data['Choices'] = '';
                $data['ChoiceExist'] = 'False';
                $data['subject'] = $subjct;
                $data['totalRow'] = $totalRow;
                $data['rollno'] = $userrolln;
                return view('student.regopenelective', $data);
            } else {
                $ChoiceFilled = DB::table('OpenElectivePriority')
                        ->select("RollNumber", "ChoiceNum", "SubjectName", "SubjectID", "Semester", "Department", "Status", "Degree", "year")
                        ->where('RollNumber', '=', $userrolln)
                        ->Where('Semester', '=', $nsem)
                        ->Where('Status', '=', "Y")
                        ->get();

                //->Where('SemNum', '=', $sem[0]->currentSem)
                $data['subject'] = '';
                $data['rollno'] = $userrolln;
                $data['Choices'] = $ChoiceFilled;
                $data['ChoiceExist'] = 'True';
                return view('student.regopenelective', $data);
            }
        }
    }

    public function regForm() {

        $id = Auth::user()->id;
        $course = Helpers::getUserCourse($id);
        $studenttyp = Helpers::getUserType($id);
        $populateyear = Helpers::populateCombo('gateyear', 'gateyr');
        $populateboard = Helpers::populateCombo('Board', 'BoardName');
        $populatestate = Helpers::populateCombo('state_list', 'state');
        $populatecountry = Helpers::populateCombo('countries', 'name');
        $populatebranch = Helpers::populateCombo('Branch_Spec', 'department');
        $populatebranshort = Helpers::populateComboOne('DepartmentMaster');
        $populatespec = Helpers::populateCombo('Branch_Spec', 'specilization');
        $username = Helpers::getUserFullName($id);
        if ($username) {
            return redirect('/student/dashboard');
        }
        $data['usrname'] = Auth::user()->name;
        $data['eml'] = Auth::user()->email;
        $data['phone'] = Auth::user()->phone;
        $data['populateboard'] = $populateboard;
        $data['populatestate'] = $populatestate;
        $data['populatecountry'] = $populatecountry;
        $data['populatebranch'] = $populatebranch;
        $data['populatespec'] = $populatespec;
        $data['userid'] = $id;
        $data['populateyear'] = $populateyear;
        $data['course'] = $course;
        $data['studenttyp'] = $studenttyp;
        $data['populatebranshort'] = $populatebranshort;
        return view('student.regform', $data);
    }

    public function regSemester() {

        $id = Auth::user()->id;

        if (!Helpers::isValidStudent($id)) {
            Session::flash('message', 'Your Registration Detail Has Not Been Activated Yet!');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }

        //lines of codes added here to check the dates of semester registration


        $userrolln = Helpers::getUserRollNumber($id);
        $userbranch = Helpers::getUserBranch($id);
        $sem = Helpers::getCurrentSemester($id);
        $deg = Helpers::getUserCourse($id);

        if ($sem->isEmpty()) {
            Session::flash('message', 'Roll number not found semester registraion table!');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        $nsem = Helpers::getSemNumToText($sem[0]->currentSem);

        $cdt = date("Y-m-d");




        $validDate = DB::table('RegistrationDates')
                ->where('Department', '=', $userbranch)
                ->Where('Semester', '=', $nsem)
//                ->Where(DB::raw("(DATE_FORMAT(StartDate,'%d-%m-%y'))"), '>=', $cdt)               
                ->Where('StartDate', '<=', $cdt)
                ->Where('EndDate', '>=', $cdt)
                ->Where('TYPE', '=', 'SEMESTER')
                ->Where('Degree', '=', $deg)
                ->Where('Status', '=', 'Y')
                ->get();

        if ($validDate->isEmpty()) {
            Session::flash('message', 'Either the Registration Dates are over. OR the Registration is not started yet. ');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }

///end of lines


        $nextrow = '';
        $semdata = array();
        $reg = Helpers::getSemesterRegData($id);

        if (!empty($reg[0]->currentSem)) {
            $nextrow = $reg[0]->currentSem;
            $semdata[] = explode(',', $reg[0]->SGPI);
            foreach ($semdata as $r) {
                $totalSem = count($r);
            }
        }
        if (User::find($id)->studenttype == 'newstudent') {
            if (empty($reg[0]->Semester) && empty($reg[0]->SGPI) && empty($reg[0]->CGPI)) {
                $totalSem = 0;
                $currentSem = 1;
            } else {
                $totalSem;
                $currentSem = $reg[0]->currentSem;
            }
        } else {
            $totalSem;
            $currentSem = $reg[0]->currentSem;
        }

        $data['course'] = Helpers::getUserCourse($id);
        if (count($reg) > 0) {
            $data['now'] = $totalSem;
            $data['last'] = $reg[0]->currentSem;
            $data['semesterdata'] = DB::table('semesterMaster')->where('semNum', $nextrow + 1)->orderBy('semName', 'asc')->get();
        } else {
            $data['now'] = 0;
            $data['last'] = 1;
            $data['semesterdata'] = DB::table('semesterMaster')->where('semNum', 2)->orderBy('semName', 'asc')->get();
        }
        if (Helpers::getUserCourse($id) == 'DUAL') {
            $data['msgug'] = '(UG Level)';
            $data['msgpg'] = '(PG Level)';
        } else {
            $data['msgug'] = '';
            $data['msgpg'] = '';
        }


        return view('student.regsemester', $data);
    }

    public function semRegAdd(Request $request) {
        $id = Auth::user()->id;
        $reg = Helpers::getSemesterRegData($id);
        $RollNum = Helpers::getUserRollNumber($id);
        $SemNum = Helpers::getSemTextToNum($request->semester_num);
        $currentId = DB::table('StudentSemReg')->where('userID', $id)->value('id');
        if (count($reg) > 0) {
            if (empty($reg[0]->Semester) && empty($reg[0]->SGPI) && empty($reg[0]->CGPI)) {
                $sem = $request->semester_num;
                $sgpi = $request->sgpi;
                $cpi = $request->cgpi;
                $pgsgpi = $request->pgsgpi;
                $pgcpi = $request->pgcgpi;
            } else {
                $sem = $reg[0]->Semester . ',' . $request->semester_num;
                $sgpi = $reg[0]->SGPI . ',' . $request->sgpi;
                $cpi = $reg[0]->CGPI . ',' . $request->cgpi;
                if (empty($reg[0]->PGSGPI) && empty($reg[0]->PGSGPI)) {
                    $pgsgpi = $request->pgsgpi;
                    $pgcpi = $request->pgcgpi;
                } else {
                    $pgsgpi = $reg[0]->PGSGPI . ',' . $request->pgsgpi;
                    $pgcpi = $reg[0]->PGCGPI . ',' . $request->pgcgpi;
                }
            }
            $data = ['userID' => $id,
                'RollNumber' => $RollNum,
                'currentSem' => $request->semester_name,
                'Semester' => $sem,
                'RegYear' => '2020',
                'Session' => 'july',
                'ApproveToken' => 'N',
                'SGPI' => $sgpi,
                'CGPI' => $cpi,
                'PGSGPI' => $pgsgpi,
                'PGCGPI' => $pgcpi,
            ];
            StudentSemReg::find($currentId)->update($data);
        } else {
            $sem = new StudentSemReg;
            $sem->userID = $id;
            $sem->RollNumber = $RollNum;
            $sem->currentSem = $request->semester_name;
            $sem->Semester = $request->semester_num;
            $sem->RegYear = '2020';
            $sem->Session = 'jan';
            $sem->SGPI = $request->sgpi;
            $sem->CGPI = $request->cgpi;
            $sem->save();
        }
        Session::flash('message', 'Saved Successfully.');
        Session::flash('type', 'success');
        return redirect('/student/dashboard');
    }

    public function regDepartmentElective() {
        $id = Auth::user()->id;
        $yr = date("Y");
        if (!Helpers::isValidStudent($id)) {
            Session::flash('message', 'Your Registration Detail Has Not Been Activated Yet!');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        $RollNum = Helpers::getUserRollNumber($id);
        if (empty($RollNum)) {
            Session::flash('message', 'RollNumber is Not Allotted Yet.');
            Session::flash('type', 'success');
            return redirect('/student/dashboard');
        }
        $course = Helpers::getUserCourse($id);
        $branch = Helpers::getUserBranch($id);
        $sem = Helpers::getCurrentSemester($id);
        if (count($sem) <= 0) {
            Session::flash('message', 'No Semester is Allotted Yet');
            Session::flash('type', 'success');
            return redirect('/student/dashboard');
        }
        $semText = Helpers::getSemNumToText($sem[0]->currentSem);
        $semNum = Helpers::getSemTextToNum($sem[0]->currentSem);
        ////checking for Department Elective Date
        $userrolln = $RollNum;
        $userbranch = Helpers::getUserBranch($id);
        $sem = Helpers::getCurrentSemester($id);
        $deg = Helpers::getUserCourse($id);
        if (count($sem) <= 0) {
            Session::flash('message', 'RollNumber/Semester not found semester registraion table!');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        $nsem = Helpers::getSemNumToText($sem[0]->currentSem + 1);
        $cdt = date("d-m-Y");
        $validDate = DB::table('RegistrationDates')
                ->where('Department', '=', $userbranch)
                ->Where('Semester', '=', $nsem)
//                ->Where(DB::raw("(DATE_FORMAT(StartDate,'%d-%m-%y'))"), '>=', $cdt)               
                ->Where('StartDate', '<=', $cdt)
                ->Where('EndDate', '>=', $cdt)
                ->Where('TYPE', '=', "DEPARTMENT ELECTIVE")
                ->Where('Degree', '=', $deg)
                ->Where('Status', '=', 'Y')
                ->count();
        /// End for Department date
        $retval = DB::table('electiveSemsterConfig')->where('SemNum', $semNum)
                ->where('Department', '=', $branch)
                ->where('Degree', '=', $course)
                ->where('electiveType', '=', 'DEPARTMENT ELECTIVE')
                ->exists();
        if (!empty($retval)) {
            $reg = DB::table('DeptElectiveConfig')
                    ->select("Pool", DB::raw('count(*) as Num'))
                    ->where('Branch', '=', $branch)
                    ->Where('FloatYear', '=', $yr)
                    ->Where('SemNum', '=', $sem[0]->currentSem)
                    //->Where('SemNum', '=', 5)
                    ->Where('degree', '=', $course)
                    ->groupBy('pool')
                    ->get();
            $alreadyExist = DB::table('DepSubElectiveChoice')
                    ->select("SubjectID", "RollNumber", "Semester", "Pool")
                    ->where('RollNumber', '=', $RollNum)
                    ->Where('Semester', '=', $semText)
                    ->exists();

            if (!empty($alreadyExist)) {
                Session::flash('message', 'You Have Already Exercised Your Department Elective Choices');
                Session::flash('type', 'error');
                return redirect('/student/dashboard');
            }
            if ($validDate <= 0) {
                Session::flash('message', 'Either the Registration Dates are Over. OR the Registration is not started yet to fill the choices of Department Elective. ');
                Session::flash('type', 'error');
                return redirect('/student/dashboard');
            }
            $data['branch'] = $branch;
            $data['yr'] = $yr;
            $data['course'] = $course;
            $data['result'] = $reg;
            return view('student.regdepartmentelective', $data);
        } else {
            Session::flash('message', 'Either you Have Already Filled the Department Elective Choices or There is No Department Elective Subject in this Semester');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
    }

    public function addRegDepElective(Request $request) {
        $id = Auth::user()->id;
        $yr = date("Y");
        $course = Helpers::getUserCourse($id);
        $branch = Helpers::getUserBranch($id);
        $sem = Helpers::getCurrentSemester($id);
        $userrolln = Helpers::getUserRollNumber($id);
        $deg = Helpers::getUserCourse($id);
        $dpt = getUserDepartment($id);
        if (empty($userrolln)) {
            Session::flash('message', 'Not a Registered Student Yet');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        if (empty($sem)) {
            Session::flash('message', 'Semester Configuration Problem for Department Elective');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        $reg = DB::table('DeptElectiveConfig')
                ->select("Pool", DB::raw('count(*) as Num'))
                ->where('Branch', '=', $branch)
                ->Where('FloatYear', '=', $yr)
                ->Where('SemNum', '=', $userrolln)
                ->Where('degree', '=', $course)
                ->groupBy('pool')
                ->get();
        foreach ($reg as $data) {
            $se = 'sub_' . $data->Pool;
            $dep = new DepElectiveChoice;
            $dep->Semester = $request->Semester;
            $dep->studentID = $id;
            $dep->RollNumber = $userrolln;
            $dep->Branch = $dpt;
            $dep->Pool = $request->pool_ . $data->Pool;
            $dep->SubjectID = $request->$se;
            $dep->Status = 'Y';
            $dep->Token = 'N';
            $dep->save();
        }
        Session::flash('message', 'Saved Successfully.');
        Session::flash('type', 'success');
        return redirect('/student/dashboard');
    }

    //        Reg-Form Submit to Database
    public function regformAdd(Request $request) {

        $id = Auth::user()->id;


        $dupl = DB::table('StudentBasic')
                ->select("RegUserID")
                ->where('RegUserID', '=', $id)
                ->exists();
        if (!empty($dupl)) {
            Session::flash('message', 'Data already Exist for This Student in Basic Registration Detail');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }

        $validatedData = $request->validate([
            'admissionin' => 'bail|required|max:40',
            'studentname' => 'bail|required|max:255',
            'studentnamehindi' => 'bail|required|max:255',
            'fathername' => 'bail|required|max:255',
            'fathernamehindi' => 'bail|required|max:255',
            'mothername' => 'bail|required|max:255',
            'mothernamehindi' => 'bail|required|max:255',
            'dob' => 'bail|required|max:50',
            'gender' => 'bail|required|max:15',
            'category' => 'bail|required|max:50',
            'religion' => 'bail|required|max:50',
            'PWD' => 'bail|required|max:10',
            'contactnumberstudent' => 'bail|required|max:10',
            'contactnumberfather' => 'max:10',
            'contactnumbermother' => 'max:10',
            'studentemail' => 'bail|unique:StudentBasic,EmailIDStudent|required|max:255',
            'fatheremail' => 'max:255',
            'ruralurban' => 'bail|required|max:20',
            'guardianname' => 'max:255',
            'contactnumberguardian' => 'max:255',
            'coaddress' => 'bail|required|max:300',
            'peraddress' => 'required|max:300',
            'bonafiedstate' => 'required|max:255',
            'adharno' => 'max:12',
            'annualincome' => 'required|max:255',
            'hostelrequired' => 'required|max:255',
            'railwaystation' => 'required|max:255',
            'admissionbranch' => 'required|max:150',
        ]);




        $course = Helpers::getUserCourse($id);
        $username = Helpers::getUserFullName($id);
        $studenttyp = Helpers::getUserType($id);
        $student = new StudentBasic;
        if ($file = $request->file('photograph')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('upload/studentphoto', $name);
            $student->StudentPhoto = $name;
        }
        $student->RegUserID = $id;
        $student->AdmissionIN = $request->admissionin;
        $student->AdmissionThrough = $request->admissionthrough;
        if ($studenttyp == "oldstudent") {
            $student->InstituteRollNumber = $request->instroll;
        }
        $student->Plustwoboard = $request->plustwoboard;
        $student->AdmissionCategory = $request->seetallotment;
        $student->StudentName = $request->studentname;
        $student->StudentNameHindi = $request->studentnamehindi;
        $student->FatherName = $request->fathername;
        $student->FatherNameHindi = $request->fathernamehindi;
        $student->MotherName = $request->mothername;
        $student->MotherNameHindi = $request->mothernamehindi;
        $student->DOB = $request->dob;
        $student->Gender = $request->gender;
        $student->Category = $request->category;
        $student->Religion = $request->religion;
        $student->WeatherPWD = $request->PWD;
        $student->ContactNumberStudent = $request->contactnumberstudent;
        $student->ContactNumberFather = $request->contactnumberfather;
        $student->ContactNumberMother = $request->contactnumbermother;
        $student->EmailIDStudent = $request->studentemail;
        $student->EmailIDFather = $request->fatheremail;
        $student->Rural_Urban = $request->ruralurban;
        $student->GuardianName = $request->guardianname;
        $student->ContactNumberGuardian = $request->contactnumberguardian;
        $student->CoAddress = $request->coaddress;
        $student->PermanentAddress = $request->peraddress;
        $student->BonafiedState = $request->bonafiedstate;
        $student->AdharNo = $request->adharno;
        $student->NADID = $request->nadid;
        $student->feewaiver = $request->feewaiver;
        $student->AnnualFamilyIncome = $request->annualincome;
        $student->HostelRequired = $request->hostelrequired;
        $student->RailwayStation = $request->railwaystation;
        $student->AdmissionBranch = $request->admissionbranch;
        if ((($course == "BTech" || $course == "DUAL" || $course == "BArch") && $studenttyp == "newstudent")) {
            $student->jeeMainRollNumber = $request->jeemainrollnumber;
            $student->JeeMainRankAl = $request->jeemainrankAIR;
            $student->JeeMainRankCategoryAI = $request->jeemainrankcategory;
            $student->jeePercentile = $request->jeepercentile;
            $student->ExamPassedCountry = $request->exampassedcountry;
            $student->QualifyingExamPassedState = $request->exampassedstate;
            $student->MarksInPercentage_CGPI = $request->marksinpercentagecgpi;
            $student->MarksPercentageCGPI = $request->markscgpi;
            $student->LastExamPassingYear = $request->exampassedyear;
            $student->AdmittedCategory = $request->admissioncategory;
            $student->QualifyingExamPassed = $request->qualifyexampassed;
            $student->LastInstitutionAttended = $request->lastinstattend;
        } elseif ((($course == "MTech" || $course == "MArch") && $studenttyp == "newstudent")) {
            $student->GateRegID = $request->gateregid;
            $student->GateScore = $request->gatescore;
            $student->GateRegYear = $request->gateregyear;
            $student->GatePaperCode = $request->gatepapercode;
            $student->ExamPassedCountry = $request->exampassedcountry;
            $student->QualifyingExamPassedState = $request->exampassedstate;
            $student->MarksInPercentage_CGPI = $request->marksinpercentagecgpi;
            $student->MarksPercentageCGPI = $request->markscgpi;
            $student->LastExamPassingYear = $request->exampassedyear;
            //$student->AdmissionBranch = $request->admissionbranch;
            $student->AdmittedCategory = $request->admissioncategory;
            $student->QualifyingExamPassed = $request->qualifyexampassed;
            $student->LastInstitutionAttended = $request->lastinstattend;
            $student->Specilization = $request->specilization;
        }

        $student->save();

        $us = user::find($id);
        $us->department = $request->admissionbranch;
        $us->save();
        if ($studenttyp == "oldstudent") {
            if ($request->semester_number == 0) {
                $currentSem = '';
                $studentsgpi = '';
                $studentcgpi = '';
                $studentpgsgpi = '';
                $studentpgcgpi = '';
            } else {
                $current_Sem = $request->semester_number + 1;
                $j = 1;
                for ($i = 1; $i < $current_Sem; $i ++) {
                    $currentS[] = $j;
                    $j++;
                }
                $currentSem = implode(',', $currentS);
                $studentsgpi = implode(',', $request->studentsgpi);
                $studentcgpi = implode(',', $request->studentcgpi);
//                if ($request->studentCurrentDegree == 'DUAL') {
//                    $studentpgsgpi = implode(',', $request->studentpgsgpi);
//                    $studentpgcgpi = implode(',', $request->studentpgcgpi);
//                }
            };
            $SemReg = new StudentSemReg;
            $SemReg->userID = $id;
            $SemReg->RollNumber = $request->instroll;
            $SemReg->currentSem = $request->semester_number + 1;
            $SemReg->Semester = $currentSem;
            $SemReg->SGPI = $studentsgpi;
            $SemReg->CGPI = $studentcgpi;
//            if ($request->studentCurrentDegree == 'DUAL') {
//                $SemReg->PGSGPI = $studentpgsgpi;
//                $SemReg->PGCGPI = $studentpgcgpi;
//            }
            $SemReg->ApproveSemNum = '';
            $SemReg->RegYear = date('Y');
            $SemReg->Session = 'July';

            $SemReg->save();
        }
        Session::flash('message', 'Saved Successfully.');
        Session::flash('type', 'success');
        return redirect('/student/dashboard');
    }

    public function checkEmail(Request $req) {
        $email = $req->email;
//         $items = DB::table('StudentBasic')->pluck('EmailIDStudent',$email); 
        $email = DB::table('StudentBasic')->where('EmailIDStudent', $email)->value('EmailIDStudent');
        if ($email) {
            return [array('msg' => 'TRUE')];
        } else {
            return [array('msg' => 'FALSE')];
        }
    }

    public function openElectiveSave(Request $request) {
        $toatalRow = $request->totalcount;
        $dataResult = DB::table('OpenElectivePriority')->where(array('RollNumber' => $request->student_roll, 'Semester' => 'S05'))->get();
        if (!$dataResult->isEmpty()) {
            Session::flash('message', 'Your Choices For open elective are already filled.');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
        if ($toatalRow > 0) {
            for ($i = 1; $i <= $toatalRow; $i++) {
                $subid = 'subid_' . $i;
                $depid = 'depid_' . $i;
                $sbid = 'sbid_' . $i;
                $chnum = 'Choice_' . $i;
                $openelc = New OpenElectiveChoices;
                $openelc->RollNumber = $request->student_roll;
                $openelc->Degree = $request->degree;
                $openelc->year = $request->year;
                $openelc->SubjectID = $request->$subid;
                $openelc->SubjectName = $request->$sbid;
                $openelc->Department = $request->$depid;
                $openelc->Semester = $request->semester;
                $openelc->ChoiceNum = $request->$chnum;
                $openelc->save();
            }
            Session::flash('type', 'success');
            Session::flash('message', 'Saved Successfully.');
            return redirect('/student/reg-open-elective');
        } else {
            Session::flash('type', 'error');
            Session::flash('message', 'Please fill the choices.');
            return redirect('/student/reg-open-elective');
        }
    }

    public function generatePDF($sem) {
        $id = Auth::user()->id;
        $username = Helpers::getUserFullName($id);
        $deg = Helpers::getStudentDegree($id);
        $userbranch = Helpers::getUserBranch($id);
        $userstatus = Helpers::getUserStatus($id);
        $userrolln = Helpers::getUserRollNumber($id);
        $useremail = Helpers::getUserEmail($id);
        if ($sem != 0) {
            $semester = $sem;
            $sm = Helpers::getSemNumToText($semester);
            $CoreSubject = DB::table('SubjectMaster')->where('Department', $userbranch)
                    ->where('Degree', $deg)
                    ->where('Semester', $sm)
                    ->where('SubjectType', 'Core')
                    ->where('isActive', 'Y')
                    ->get();
            $data['studentbasic'] = DB::table('StudentBasic')->where('RegUserID', $id)->orderBy('studentID', 'asc')->get();
            $data['studentsem'] = DB::table('StudentSemReg')->where('userID', $id)->orderBy('id', 'asc')->get();
            $data['depelective'] = DB::table('DepSubElectiveChoice')->where(array('RollNumber' => $userrolln, 'Token' => 'Y', 'Semester' => $sm))->get();
            $data['openelective'] = DB::table('OpenElectiveAllotment')->where(array('RollNumber' => $userrolln, 'Token' => 'Y', 'Semester' => $sm))->get();
            $data['userdata'] = DB::table('users')->where('id', $id)->orderBy('id', 'asc')->get();
            $data['title'] = 'Welcome to NITH';
            $data['csubject'] = $CoreSubject;
            $pdf = PDF::loadView('student.myPDF', $data);
            return $pdf->download('reg.pdf');
        } else {
            Session::flash('message', 'Please Select PDF Semester!.');
            Session::flash('type', 'error');
            return redirect('/student/dashboard');
        }
    }

    public function viewProfile() {
        $id = Auth::user()->id;
        $data['user_info'] = DB::table('users')->where('id', $id)->get();
        return view('student.view_profile', $data);
    }

    public function saveProfile(Request $request) {
        $user = Auth::user();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->save();
        Session::flash('message', 'Profile Update Successfully.');
        Session::flash('type', 'success');
        return redirect('/student/dashboard');
    }

    function editPopUp($page_name = '', $param2 = '') {
        if ($page_name == 'edit_student') {
            $data['param2'] = $param2;
            $data['result'] = DB::table('StudentBasic')->where('studentID', $param2)->get();
            return view('student.' . $page_name, $data);
        }
    }

    public function editStudent(Request $request) {
        $data = [
            'StudentName' => $request->StudentName,
            'FatherName' => $request->fathername,
            'FatherNameHindi' => $request->fathernamehinid,
            'MotherName' => $request->mothername,
            'MotherNameHindi' => $request->mothernamehindi,
            'PermanentAddress' => $request->peraddress,
//            'CoAddress' => $request->coaddress,
        ];
        if ($file = $request->file('photograph')) {
            $name = time() . $file->getClientOriginalName();
            $file->move('upload/studentphoto', $name);
            $data['StudentPhoto'] = $name;
        }
        DB::table('StudentBasic')->where('studentID', $request->studentID)->update($data);
        $message = "Data Updated Successfully.";
        $type = 'success';
        Session::flash('message', $message);
        Session::flash('type', $type);
        return redirect('/student/dashboard');
    }

    public function getResult(Request $request) {
        $rollnum = $request->get('RollNumber');
        $url = 'http://14.139.56.15/scheme18/singleresult/ResultJSON.asp?RollNum=' . $rollnum;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response_json = curl_exec($ch);
        $result = json_decode($response_json);
     
        curl_close($ch);
        if ($result)
            return ['status' => true, 'result' => $result];
        else
            return ['status' => false, 'result' => $result];
    }

}
