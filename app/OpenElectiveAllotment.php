<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenElectiveAllotment extends Model {

    protected $table = 'OpenElectiveAllotment';
    protected $fillable = ['RollNumber', 'SubjectID', 'SubjectName', 'ChoiceNum', 'Semester', 'Year', 'Department', 'Status', 'Token'];

}
