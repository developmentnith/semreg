<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancelRollNumber extends Model
{  
    
     protected $table = 'CancelStudentRollNumber';
    protected $fillable = ['StudentID', 'InstituteRollNumber', 'StudentName', 'Degree', 'Branch', 'Grop', 'Section', 'AdmissionYear'];
    //
}
