<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentNoDues extends Model
{
    //StudentNoDues
    protected $table = 'StudentNoDues';
    protected $fillable = ['id','RollNumber','Semester','NoDueDetail','NoDueByDept','NoDueRemark','isCleared','NoDueClearedBy','NoDueDate','created_at','updated_at'];
}
