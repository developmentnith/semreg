<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectMaster extends Model
{
    
     protected $table = 'SubjectMaster';
    protected $fillable = ['SubjectName', 'SubjectCode', 'credit', 'TheoryPractical', 'Degree', 'Semester', 'Group', 'SubjectType', 'TotalLectureTheory', 'TotalLecturePractical', 'isActive'];
    //
}
