<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenElectiveChoices extends Model
{
   protected $table = 'OpenElectivePriority'; 
        protected $fillable = ['RollNumber', 'ChoiceNum', 'SubjectCode', 'SubjectName', 'Department','Degree','Semester','Status','year','Allotted','Status','Token'];

}
