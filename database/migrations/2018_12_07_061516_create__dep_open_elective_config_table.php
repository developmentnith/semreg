<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepOpenElectiveConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DeptOpenElectiveConfig', function (Blueprint $table) {
            $table->increments('id');
             $table->string('FloatedByDept', 100);
             $table->string('SubjectCode', 100);
             $table->string('SubjectName', 200);
             $table->integer('credit');
             $table->string('Semester', 20);
             $table->string('FloatedToDept', 150);
             $table->string('FloatYear', 10);
             $table->string('FloatSession', 20);
             $table->integer('MinimumStudent');
             $table->integer('MaximumStudent');
             $table->string('isActive', 10);
             $table->string('FloatedBy', 80);
            
            
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DeptOpenElectiveConfig');
    }
}
