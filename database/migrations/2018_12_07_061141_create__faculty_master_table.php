<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacultyMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('FacultyMaster', function (Blueprint $table) {
            $table->increments('id');
             
            $table->string('FacultyName',200);
            $table->string('FatherName',200);
            $table->string('FacultyNameHindi',200);
            $table->string('Designation',50);
            $table->string('Gender',20);
            $table->string('DOB',50);
            $table->string('EmailID',80);
            $table->string('Password',30);
            $table->string('Department',100);
            $table->string('isVerified',10)->default('N');
            $table->string('isActive',10)->default('N');
            
            /*$table->string('',10);*/
            
            
            
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('FacultyMaster');
    }
}
