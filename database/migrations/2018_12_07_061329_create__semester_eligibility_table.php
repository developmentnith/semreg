<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSemesterEligibilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SemesterEligibility', function (Blueprint $table) {
            $table->increments('id');
             $table->string('RollNumber', 20);
             $table->string('Semester', 20);
             $table->string('SGPI', 20);
             $table->string('CGPI', 20);
             $table->string('isEligibleNextSemReg', 10)->default('Y');
             $table->string('Remarks', 500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SemesterEligibility');
    }
}
