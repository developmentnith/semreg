<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSemNoDuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SemNoDues', function (Blueprint $table) {
            $table->increments('id');
             $table->string('RollNumber', 20);
             $table->string('Semester', 20);
             $table->string('NodueDetail', 200);
             $table->string('NodueDepartment', 200);
             $table->string('NodueRemarks', 200);
             $table->string('isCleared', 200)->default('N');
             $table->string('NodueClearedBy', 100);
             $table->string('NodueDate', 30);
//             $table->string('isleared', 20);
            
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SemNoDues');
    }
}
