<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentSemRegTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StudentSemReg', function (Blueprint $table) {
            $table->increments('id');
            $table->string('RollNumber', 50);
            $table->string('Semester', 20);
            $table->string('RegYear', 6);
            $table->string('Session', 20);
            $table->string('SGPI', 20);
            $table->string('CGPI', 20);
            $table->string('DepartmentElective', 100);
            $table->string('OpenElective', 20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('StudentSemReg');
    }
}
