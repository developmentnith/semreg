<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepElectiveConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DeptElectiveConfig', function (Blueprint $table) {
            $table->increments('id');
             $table->string('SubjectCode', 100);
             $table->string('SubjectName', 200);
             $table->string('Semester', 20);
             $table->string('Branch', 20);
             $table->string('FloatYear', 10);
             $table->string('FloatSession', 20);
             $table->string('FloatedBy', 20);
             $table->string('isActive', 10);
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DeptElectiveConfig');
    }
}
