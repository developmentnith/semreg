<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DepartmentMaster', function (Blueprint $table) {
            $table->increments('id');
            $table->string('DepartmentFullName', 150)->unique();
            $table->string('DepartmentShortName', 20)->unique();
            $table->string('DepartmentNumber', 150)->unique();
            $table->string('DepartNameHindi', 150);
               
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DepartmentMaster');
    }
}
