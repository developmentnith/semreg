<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentbasicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('StudentBasic', function (Blueprint $table) {
            $table->bigIncrements('studentID');
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //$table->primary('id');
            //$table->string('name', 100);
            //$table->text('description');
            //$table->char('name', 4);
            //$table->date('created_at');
            //$table->decimal('amount', 5, 2);
            //->default($value)
            
            $table->string('AdmissionIN', 40);
            $table->string('AdmissionThrough', 40);
            $table->string('AdmissionCategory', 40);
            
            $table->string('StudentName', 150);
            $table->string('StudentNameHindi', 150);
            $table->string('FatherName', 150);
            $table->string('FatherNameHindi', 150);
            $table->string('MotherName', 150);
            $table->string('MotherNameHindi', 150);
            $table->string('GuardianName', 150);
            
            $table->string('DOB', 50);
            $table->string('Gender', 15);
            $table->string('Category', 50);
            $table->string('Religion', 50);
            $table->text('PermanentAddress');
            $table->text('CoAddress');
            $table->string('ContactNumberStudent',20)->unique();
            $table->string('ContactNumberFather',20)->unique();
            $table->string('ContactNumberMother',20);
            $table->string('ContactNumberGuardian',20);
            $table->string('EmailIDStudent',120)->unique();
            $table->string('EmailIDFather',120);
            $table->string('password',25);
            $table->string('BonafiedState',150);
            $table->string('AdharNo',20);
            $table->string('WeatherPWD',10);
            $table->string('Rural_Urban',20);
            $table->string('AnnualFamilyIncome',20);
            $table->string('HostelRequired',20);
            $table->string('RailwayStation',100);

            
            
            
            $table->string('jeeMainRollNumber', 40);
            $table->string('JeeMainRankAl',20);
            $table->string('JeeMainRankState',20);
            $table->string('JeeMainRankCategoryAI',20);
            $table->string('JeeMainRankCategoryState',20);
            
            $table->string('GateRegID',20);
            $table->string('GateScore',20);
            $table->string('GateRegYear',20);
            $table->string('GatePaperCode',20);
            
            
            
            
            $table->string('LastInstitutionAttended',150);
            $table->string('QualifyingExamPassed',150);
            $table->string('QualifyingExamPassedState',150);
            $table->string('MarksInPercentage_CGPI',50);
            $table->decimal('MarksPercentageCGPI',4,2);
            $table->string('AdmittedCategory',50);
            $table->string('LastExamPassingYear',6);
            $table->string('AdmissionBranch',150);
            $table->string('Specilization',150);
            $table->string('ExamPassedCountry',150);
                        
            $table->string('RegistrationNumber',100);
            $table->string('FileNumber',100);
            $table->date('AdmissionDate');
            $table->String('DataChcekedBy',50);
            $table->String('DataVerifiedBy',50);
            $table->String('StudentAdmissionStatus',50);
            $table->String('InstituteRollNumber',50);
            $table->String('RegUserID',50);
            $table->String('Status',50)->default('N');
            $table->String('Token',50)->default('N');
            $table->String('OtpPhone',50);
            $table->String('OtpEmail',50);
            $table->String('IsActive',10)->default('N');
                       
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('StudentBasic');
    }
}
