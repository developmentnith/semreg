<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SubjectMaster', function (Blueprint $table) {
            $table->increments('id');
            $table->string('SubjectCode', 50)->unique();
             $table->string('SubjectName', 250);
             $table->integer('credit');
            $table->string('TheoryPractical', 50);
            $table->string('Degree', 50);
            $table->string('Department', 50);
            $table->string('Semester', 50);
            $table->string('Group', 50);
            $table->string('SubjectType', 50);
            $table->integer('TotalLectureTheory');
            $table->integer('TotalLabPractical');
            $table->string('isActive',10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SubjectMaster');
    }
}
