<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/sendopt', 'RegisterController@sendOtp');
Route::post('/registeruser', 'RegisterController@registerUser');
Route::get('/create-pass', 'RegisterController@createPass');
Route::post('/loginuser', 'RegisterController@loginUser');
Route::post('/password-forget', 'RegisterController@passwordForget');
Route::get('/subject', 'Admin\SubjectMasterController@index');
Route::get('/forget-password', 'RegisterController@forgetPassword');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/useremail', 'RegisterController@userCheckEmail');



 Route::get('check-toster', 'Auth\RegisterController@checkToster');

//Route::get('/login', array('uses' => 'HomeController@showLogin'));

Route::group(['middleware' => 'prevent-back-history'], function() {
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
});

   

Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
        Route::get('/', 'AdminDashboardController@index');
        Route::get('/dashboard', 'AdminDashboardController@index');
        Route::get('/subject', 'SubjectMasterController@index');
        Route::get('/registred-students', 'AdminDashboardController@registerStudent');
        Route::get('/all-registred-students', 'AdminDashboardController@AllRegisterStudent');
        Route::get('/canceled-students', 'AdminDashboardController@canceledStudents');
        Route::get('/import-excel', 'AdminDashboardController@importExcel');
        Route::post('/add-excel', 'AdminDashboardController@addExcel');
        Route::post('/add-subject', 'SubjectMasterController@addSubject');
        Route::get('/subjectdelete/{id}', 'SubjectMasterController@deleteSubject');
//        Route::get('/update-student/{id}', 'AdminDashboardController@updateStudent');
        Route::post('/update-student', 'AdminDashboardController@updateStudent');
        Route::post('/all-update-student', 'AdminDashboardController@allUpdateStudent');
        Route::post('/cancel-update-student', 'AdminDashboardController@cancelUpdateStudent');
        Route::get('/view-student/{id}', 'AdminDashboardController@viewStudents');
        Route::get('/delete-student/{id}', 'AdminDashboardController@deleteStudents');
        Route::get('/generate-pdf/{id}', 'AdminDashboardController@generatePDF');
        Route::get('/open-elective', 'AdminDashboardController@openElective');
        Route::post('/open-elective-allotment', 'AdminDashboardController@openElectiveAllotment');
        Route::post('/edit-open-elective-allotment', 'AdminDashboardController@editOpenElectiveAllotment');
        Route::post('/get-open-elective-choice', 'AdminDashboardController@GetOpenElectiveChoice');
        Route::get('/delete-openelective-allotted/{id}', 'AdminDashboardController@deleteOpenpelectiveAllotted');
        Route::get('/modal-pop/{page}/{id}', 'AdminDashboardController@popup');
        Route::get('/update-modal-pop/{page}/{id}', 'AdminDashboardController@updatePopup');
        Route::get('/cancel-modal-pop/{page}/{id}', 'AdminDashboardController@cancelAlotPopup'); 
        Route::get('/create-user', 'AdminDashboardController@createUser');
        Route::get('/student-sgpi-cgpi', 'AdminDashboardController@studentSgpiCgpi');
        Route::post('/add-user', 'AdminDashboardController@addUser');
        Route::post('/edit-student', 'AdminDashboardController@editStudent'); 
        Route::post('/edit-subject', 'AdminDashboardController@editSubject');
        Route::post('/upd-dupl-sgpi', 'AdminDashboardController@updateDuplicateSGPI');
        Route::get('/delete-user/{id}', 'AdminDashboardController@deleteuser');
        Route::get('/edit-modal-pop/{page}/{id}', 'AdminDashboardController@editPopUp');
        Route::get('download-SGPI-CGPI-Excel/{type}', 'AdminDashboardController@downloadExcelSGPICGPI');
        Route::get('date-configuration', 'AdminDashboardController@dateConfiguration');
        Route::post('add-semreg-start-end-date', 'AdminDashboardController@dateStartEndSemester');
        Route::get('delete-registration-date/{rid}', 'AdminDashboardController@deleteRegistrationDate');
        Route::get('deactivate-registration-date/{rid}', 'AdminDashboardController@deactivateRegistrationDate');
        Route::get('activate-registration-date/{rid}', 'AdminDashboardController@activateRegistrationDate');
        Route::get('/cancel-allotment', 'AdminDashboardController@CancelOpenElectiveAllotment');
        Route::get('/final-open-allotment', 'AdminDashboardController@FinalizeOpenElectiveAllotment');
        Route::get('/cancel-pop/{page}/{id}/{roll}', 'AdminDashboardController@cancelPopup');
        Route::get('/testtable', 'AdminDashboardController@testTable');

//        Route::get('/cancel-roll-nubmer/{id}/{roll}', 'AdminDashboardController@cancelRollNumber');
        Route::post('/cancel-roll-nubmer', 'AdminDashboardController@newCancelRollNumber');
    });
});

Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'prefix' => 'user'], function () {
        Route::get('/', 'UserDashboardController@index');
        Route::get('/dashboard', 'UserDashboardController@index');
        Route::get('/all-registred-students', 'UserDashboardController@AllRegisterStudent');
    });
});

Route::group(['middleware' => 'auth'], function () {

    Route::group(['namespace' => 'Faculty', 'prefix' => 'faculty'], function () {
        Route::get('/', 'FacultyDashboardController@index');
        Route::get('/dashboard', 'FacultyDashboardController@index');
        Route::get('/register-student', 'FacultyDashboardController@registerStudent');
        Route::get('/float-dep-elective', 'FacultyDashboardController@floatDepElective');
        Route::get('/float-open-elective', 'FacultyDashboardController@floatOpenElective');
        Route::get('/view-dep-elective', 'FacultyDashboardController@ViewDepElective');
        Route::get('/view-open-elective', 'FacultyDashboardController@ViewOpenElective');
        Route::post('/get-subjectname', 'FacultyDashboardController@getSubjectName');
        Route::post('/get-studentname', 'FacultyDashboardController@getStudentName');
        Route::post('/add-department-elective', 'FacultyDashboardController@addDepartmentElective');
        Route::post('/edit-department-elective', 'FacultyDashboardController@editDepartmentElective');
        Route::get('/delete-depelective/{id}', 'FacultyDashboardController@deleteDepelective');
        Route::post('/add-open-elective', 'FacultyDashboardController@addOpentElective');
        Route::post('/add-student-nodues', 'FacultyDashboardController@addStudentNodues');
        Route::get('/delete-openelective/{id}', 'FacultyDashboardController@deleteOpenelective');
        Route::post('/multiapprove', 'FacultyDashboardController@multiApprove');
        Route::post('/depmultiapprove', 'FacultyDashboardController@depMultiApprove');
        Route::post('/sem-student-approve', 'FacultyDashboardController@semStudentApprove');
        Route::get('/approve-single-student/{sem}/{id}', 'FacultyDashboardController@approveSingleStudent');
        Route::get('/modal-pop/{page}/{id}', 'FacultyDashboardController@popup');
        Route::get('/no-dues', 'FacultyDashboardController@studentNodues');
        Route::get('/delete-nodues/{id}', 'FacultyDashboardController@deleteNoDue');
        Route::get('/clear-nodues/{id}/{nm}', 'FacultyDashboardController@clearNoDue');
        Route::get('/verify-old-student', 'FacultyDashboardController@verifyOldStudent');
        Route::get('/old-student-verified/{sid}/{uid}', 'FacultyDashboardController@OldStudentVerifySave');
        Route::get('/delete-old-student/{sid}/{uid}', 'FacultyDashboardController@deleteOldStudent');
        Route::get('/view-student-for-semester', 'FacultyDashboardController@viewStudentForSemester');
        Route::get('/approve-student-for-download/{sid}', 'FacultyDashboardController@approveStudentForDownload');
        Route::get('/view-student-sem-data/{page}/{id}', 'FacultyDashboardController@viewSGPI');
    });
});
Route::group(['middleware' => 'auth'], function () {

    Route::group(['namespace' => 'Student', 'prefix' => 'student'], function () {
        Route::get('/', 'StudentDashboardController@index');
        Route::get('/dashboard', 'StudentDashboardController@index');
        Route::get('/reg-form', 'StudentDashboardController@regForm');
        Route::get('/reg-semester', 'StudentDashboardController@regSemester');
//        Route::get('/reg-semester', 'StudentDashboardController@regSemester');
         Route::post('/get-result', 'StudentDashboardController@getResult');
         Route::post('/old-get-result', 'StudentDashboardController@oldGetResult');
        Route::get('/reg-department-elective', 'StudentDashboardController@regDepartmentElective');
        Route::post('/semreg-add', 'StudentDashboardController@semRegAdd');
        Route::post('/depreg-add', 'StudentDashboardController@addRegDepElective');
        Route::get('/reg-open-elective', 'StudentDashboardController@showOpenElective');
        Route::get('/current-open-elective', 'StudentDashboardController@currentOpenElective');
        Route::get('/get-pdf/{id}', 'StudentDashboardController@generatePDF');
        Route::get('/view-profile', 'StudentDashboardController@viewProfile');
        Route::post('/save-profile', 'StudentDashboardController@saveProfile');
        Route::get('/edit-modal-pop/{page}/{id}', 'StudentDashboardController@editPopUp');
//        Reg-Form Submit to Database

        Route::post('/regform-add', 'StudentDashboardController@regformAdd');
        Route::post('/update-student-sem', 'StudentDashboardController@updateStudentSem');
        Route::post('/open-elective-add', 'StudentDashboardController@openElectiveSave');
        Route::post('/checkmail', 'StudentDashboardController@checkEmail');
       
        Route::post('/edit-student', 'StudentDashboardController@editStudent');
    });
});
