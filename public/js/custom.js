$(document).on("click", ".OtpButton", function () {
//    document.getElementById('submitRegister').disabled = true;
    var phoneVal = $('#phoneValue').val();
    if (phoneVal) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: "POST",
            url: "/sendopt",
            data: $("#registerFrom").serialize(),
            dataType: "json",
            success: function (response) {
                if (response.sucess == 'true') {
                    Cookies.set("otp", response.sendotp);
                    $('#error').html('');
                    $('#sucess').html('OTP sent successfully');
                } else {
                    $('#sucess').html('')
                    $('#error').html('Please check the number');
                }
            }
        });
    } else {
        $('#error').html('all fields required.');
    }
});

$(document).on("click", ".submitRegister", function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var parent_fieldset = $(this).parents('.fieldset');
    var nextStep = true;
    parent_fieldset.find('.required').each(function () { 
        if ($(this).val() == "") {
            $(this).addClass('input-error');
            nextStep = false;
        } else {
            $(this).removeClass('input-error');
        }
    });
    if (nextStep) {
        $.ajax({
            type: "POST",
            url: "/registeruser",
            data: $("#registerFrom").serialize(),
            dataType: "json",
            success: function (response) {
                if (response.status == 'true') {
                    $('#meg').html('<div class="alert alert-success"><strong>Success!</strong> ' + response.msg + '</div>');
                    window.location = "login";
                } else {
                    $('#meg').html('<div class="alert alert-danger"><strong>Danger!!</strong> ' + response.msg + '</div>');
                }

            }
        });
    }

});
$(document).on("click", ".submitLogin", function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var parent_fieldset = $(this).parents('.LoginFieldset');
    var nextStep = true;
    parent_fieldset.find('.required').each(function () {
        if ($(this).val() == "") {
            $(this).addClass('input-error');
            nextStep = false;
        } else {
            $(this).removeClass('input-error');
        }
    });
    if (nextStep) {
        $.ajax({
            type: "POST",
            url: "/loginuser",
            data: $("#loginForm").serialize(),
            dataType: "json",
            success: function (response) {
                if (response.status == 'true') {
                    toastr.success(response.msg);
                    if (response.login_type == 'admin') {
                        setTimeout(function () {
                            window.location = "admin/dashboard";
                        }, 1000);

                    } else if (response.login_type == 'faculty') {
                        setTimeout(function () {
                            window.location = "faculty/dashboard";
                        }, 1000);

                    } else if (response.login_type == 'student') {
                        setTimeout(function () {
                            window.location = "student/dashboard";
                        }, 1000);

                    } else if (response.login_type == 'user') {
                        setTimeout(function () {
                            window.location = "user/dashboard";
                        }, 1000);

                    } else {
                        toastr.error(response.msg);
                    }
                } else {
                    toastr.error('Login Failed');
                }

            }
        });
    }

});


$(document).on("click", ".forgetPassword", function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#forgetPassword').html('wait for response..');
    document.getElementById('forgetPassword').disabled = true;
    $.ajax({
        type: "POST",
        url: "/password-forget",
        data: $("#forgetForm").serialize(),
        dataType: "json",
        success: function (response) {
            if (response.status == 'true') {
                toastr.success(response.msg);
            } else {
                $('#forgetPassword').html('Wait for response..');
                document.getElementById('forgetPassword').disabled = false;
                toastr.error(response.msg);
            }

        }
    });

});

$(document).on("keyup", ".confirmOtp", function () {
    var phoneVal = $('.confirmOtp').val();
    var otp = Cookies.get('otp');
    var buttonId = 'submitRegister';
    if (phoneVal == otp) {
        document.getElementById('submitRegister').disabled = false;
        $('.showmsg').removeClass("red");
        $('.showmsg').addClass("green");
        $('#checkPhone').html('<i class="fa fa-check" aria-hidden="true"></i>');
        enableBtn(buttonId);
    } else {
        document.getElementById('submitRegister').disabled = true;
        $('.showmsg').removeClass("green");
        $('.showmsg').addClass("red");
        $('#checkPhone').html('<i class="fa fa-times" aria-hidden="true"></i>');
        disableBtn(buttonId);
    }

});

function xyz(status) {
    alert(status);

}



$(document).on("blur", ".checkEmail", function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var _email = $('#studentemail').val();
    $.ajax({
        type: "POST",
        url: "/student/checkmail",
        data: {email: _email},
        dataType: "json",
        success: function (response) {
            if (response.msg == 'TRUE') {
                $("#msg").html("Person with this mail already Registered")
            } else {
                $("#msg").html("")
            }

        }
    });
});
$(document).on("blur", ".userCheckEmail", function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var _email = $('#emailValue').val();
    $.ajax({
        type: "POST",
        url: "/useremail",
        data: {email: _email},
        dataType: "json",
        success: function (response) {
            if (response[0].msg == 'true') {
                document.getElementById('submitRegister').disabled = true;
                $("#userError").html("Email Already Registered")
            } else {
                document.getElementById('submitRegister').disabled = false;
                $("#userError").html("")
            }

        }
    });
});
$(document).on("click", ".regEdit", function () {
    $('.firstFomr').show();
    $('.datashow').hide();
});

$(document).on("change", ".semRegister", function () {
    var semValue = $('option:selected', $('select#semRegister')).data('bind');
    $('.semName').html(semValue);
});

function semSgpi(val, sem) {
    $('.sgpi_' + sem).html(val);
}
function semCgpi(val, sem) {
    $('.cgpi_' + sem).html(val);
}
function pgsemSgpi(val, sem) {
    $('.pgsgpi_' + sem).html(val);
}
function pgsemCgpi(val, sem) {
    $('.pgcgpi_' + sem).html(val);
}

$('.datashow').hide();
$(document).on("click", ".regNext", function () {
    var msg = ''
    var parent_fieldset = $(this).parents('.validate');
    var next_step = true;
    parent_fieldset.find('.required').each(function () {
        if ($(this).val() == "") {
            $(this).addClass('input-error');
            msg = 'All Fields Requird';
            next_step = false;
        } else {
            $(this).removeClass('input-error');
        }

    });
    if (next_step) {
        $('.firstFomr').hide();
        $('#show_admissionin').val($('#admissionin').val());
        $('#show_instroll').val($('#instroll').val());
        $('#show_course').val($('option:selected', $('select#course')).val());
        $('#show_studentname').val($('#studentname').val());
        $('#show_studentnamehindi').val($('#studentnamehindi').val());
        $('#show_fathername').val($('#fathername').val());
        $('#show_fathernamehindi').val($('#fathernamehindi').val());
        $('#show_mothername').val($('#mother_name').val());
        $('#show_mothernamehindi').val($('#mothernamehindi').val());
        $('#show_datepicker').val($('#datepicker').val());
        $('#show_gender').val($('option:selected', $('select#gender')).val());
        $('#show_category').val($('option:selected', $('select#category')).val());
        $('#show_religion').val($('option:selected', $('select#religion')).val());
        $('#show_pwd').val($('option:selected', $('select#pwd')).val());
        $('#show_contactnumberstudent').val($('#contactnumberstudent').val());
        $('#show_contactnumberfather').val($('#contactnumberfather').val());
        $('#show_contactnumbermother').val($('#contactnumbermother').val());
        $('#show_studentemail').val($('#studentemail').val());
        $('#show_fatheremail').val($('#fatheremail').val());
        $('#show_bonafiedstate').val($('option:selected', $('select#bonafiedstate')).val());
        $('#show_ruralurban').val($('option:selected', $('select#ruralurban')).val());
        $('#show_guardianname').val($('#guardianname').val());
        $('#show_contactnumberguardian').val($('#contactnumberguardian').val());
        $('#show_coaddress').val($('#coaddress').val());
        $('#show_peraddress').val($('#peraddress').val());
        $('#show_adharno').val($('#adharno').val());
        $('#show_nadid').val($('#nadid').val());
        $('#show_annualincome').val($('option:selected', $('select#annualincome')).val());
        $('#show_feewaiver').val($('option:selected', $('select#feewaiver')).val());
        $('#show_hostelrequired').val($('option:selected', $('select#hostelrequired')).val());
        $('#show_railwaystation').val($('#railwaystation').val());
        $('#show_jeemainrollnumber').val($('#jeemainrollnumber').val());
        $('#show_jeemainrankAIR').val($('#jeemainrankAIR').val());
        $('#show_jeemainrankcategory').val($('#jeemainrankcategory').val());
        $('#show_jeepercentile').val($('#jeepercentile').val());
        $('#show_exampassedcountry').val($('option:selected', $('select#exampassedcountry')).val());
        $('#show_exampassedstate').val($('option:selected', $('select#exampassedstate')).val());
        $('#show_seetallotment').val($('option:selected', $('select#seetallotment')).val());
        $('#show_plustwoboard').val($('option:selected', $('select#plustwoboard')).val());
        $('#show_marksinpercentagecgpi').val($('option:selected', $('select#marksinpercentagecgpi')).val());
        $('#show_markscgpi').val($('#markscgpi').val());
        $('#show_exampassedyear').val($('option:selected', $('select#exampassedyear')).val());
        $('#show_admissionbranch').val($('option:selected', $('select#admissionbranch')).val());
        $('#show_specilization').val($('option:selected', $('select#specilization')).val());
        $('#show_admissioncategory').val($('option:selected', $('select#admissioncategory')).val());
        $('#show_qualifyexampassed').val($('option:selected', $('select#qualifyexampassed')).val());
        $('#show_lastinstattend').val($('#lastinstattend').val());
        $('#show_gateregid').val($('#gateregid').val());
        $('#show_gatescore').val($('#gatescore').val());
        $('#show_gateregyear').val($('#gateregyear').val());
        $('#show_gatepapercode').val($('#gatepapercode').val());
        var sem_Value = $('option:selected', $('select#semesterNumber')).val();
        var _currentDegree = $('#studentCurrentDegree').val();
        $('#show_semseternum').val(sem_Value);
        if (sem_Value != '0') {
            var j = 1;
            for (i = 1; i <= sem_Value; i++) {
                var _getsgpiid = '#sgpi_' + j;
                var _getcgpiid = '#cgpi_' + j;
                var _showsgpiid = '#show_sgpi_' + j;
                var _showcgpiid = '#show_cgpi_' + j;
                $(_showsgpiid).val($(_getsgpiid).val());
                $(_showcgpiid).val($(_getcgpiid).val());
                if (_currentDegree == 'DUAL') {
                    var _getpgsgpiid = '#pgsgpi_' + j;
                    var _getpgcgpiid = '#pgcgpi_' + j;
                    var _showpgsgpiid = '#show_pgsgpi_' + j;
                    var _showpgcgpiid = '#show_pgcgpi_' + j;
                    $(_showpgsgpiid).val($(_getpgsgpiid).val());
                    $(_showpgcgpiid).val($(_getpgcgpiid).val());
                }
                j++;
            }
        }
        $('.datashow').show();
    } else {
        toastr.error(msg);
    }

});

$(document).on("click", ".regSubmit", function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var data = new FormData($('#regForm')[0]);
    $.ajax({
        type: "POST",
        url: "/student/regform-add",
        data: data,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        success: function (response) {
            if (response.status == 'true') {
                toastr.success(response.msg);
                window.location = "dashboard";
            } else {
                toastr.error('Data not saved.. Pls try again.');
            }

        }
    });
});

function disableBtn(id) {

    document.getElementById(id).disabled = true;
}

function enableBtn(id) {

    document.getElementById(id).disabled = false;
}
function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
    {
        return false;
    }
    return true;
}

function validateEmail(emailField) {
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if (reg.test(emailField.value) == false)
    {
        return false;
    }
    return true;

}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function readURLPhoto(input) {
    if (input.files[0].size < 3097152) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.photoImage')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    } else {
        toastr.error('Maximum file size should be 1 MB.');
    }
}



$(document).on("change", ".depSemester", function () {
    var semValue = $('option:selected', $('select#depSemester')).val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "/faculty/get-subjectname",
        data: {semester: semValue},
        dataType: "json",
        success: function (response) {
            $('.openSubject').html('');
            var res = response.result;
            for (var i in res) {
                var item = res[i];
                $('.openSubject').append('<option  value="' + item.SubjectCode + '">' + item.SubjectCode + '(' + item.SubjectName + ')</option>')
            }

        }
    });
});

$(document).on("change", ".openSemester", function () {
    var semValue = $('option:selected', $('select#openSemester')).val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "/faculty/get-subjectname",
        data: {semester: semValue},
        dataType: "json",
        success: function (response) {
            $('.openelectiveSubject').html('');
            var res = response.result;
            for (var i in res) {
                var item = res[i];
                $('.openelectiveSubject').append('<option value="' + item.SubjectCode + '">' + item.SubjectCode + '(' + item.SubjectName + ')</option>')
            }

        }
    });
});

$(".checkAllAud").change(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var toggled = $(this).val();
    var checkValue = $('.checkValue').val();
    var checkBox = document.getElementById("checkValue");
    //alert('checkallaud function triggered');
    var checkboxValues = [];
    $('.checkValue').each(function (index, elem) {
        checkboxValues.push($(elem).val());
    });
    var _currentValue = checkboxValues.join(',');
    toggleBox(toggled, this.checked);
    if (checkBox.checked == true) {
        if (confirm('Are you sure you want to approved all data?')) {
            $.ajax({
                type: "POST",
                url: "/faculty/multiapprove",
                data: {checkvalue: _currentValue, type: 'checked', total: checkboxValues.length},
                dataType: "json",
                success: function (response) {
                    if (response.status == 'true') {
                        toastr.success(response.msg);
                        window.location = "float-dep-elective";
                    } else {
                        toastr.error(response.msg);
                    }
                }
            });
        }
    }
});
$(".checkAlldepelective").change(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var toggled = $(this).val();
    var checkValue = $('.checkDepValue').val();
    var checkBox = document.getElementById("checkDepValue");
    //alert('checkallaud function triggered');
    var checkboxValues = [];
    $('.checkDepValue').each(function (index, elem) {
        checkboxValues.push($(elem).val());
    });
    var _currentValue = checkboxValues.join(',');
    toggleBox(toggled, this.checked);
    if (checkBox.checked == true) {
        if (confirm('Are you sure you want to approved all data?')) {
            $.ajax({
                type: "POST",
                url: "/faculty/depmultiapprove",
                data: {checkvalue: _currentValue, type: 'checked', total: checkboxValues.length},
                dataType: "json",
                success: function (response) {
                    if (response.status == 'true') {
                        toastr.success(response.msg);
                        window.location = "view-dep-elective";
                    } else {
                        toastr.error(response.msg);
                    }
                }
            });
        }
    }
});



$(".regSemesterApprove").click(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var toggled = $(this).val();
    var checkValue = $('.checkSemesterValue').val();
    var currentSemNum = $('.currentSemNum').val();
    var checkBox = document.getElementById("checkSemesterValue");
    //alert('checkallaud function triggered');
    var checkboxValues = [];
    $('.checkSemesterValue').each(function (index, elem) {
        checkboxValues.push($(elem).val());
    });
    var _currentValue = checkboxValues.join(',');
    toggleBox(toggled, this.checked);
    if (checkBox.checked == true) {
        if (confirm('Are you sure you want to approved all data?')) {
            $.ajax({
                type: "POST",
                url: "/faculty/sem-student-approve",
                data: {checkvalue: _currentValue, type: 'checked', total: checkboxValues.length, currentsem: currentSemNum},
                dataType: "json",
                success: function (response) {
                    if (response.status == 'true') {
                        toastr.success(response.msg);
                        window.location = "view-student-for-semester";
                    } else {
                        toastr.error(response.msg);
                    }
                }
            });
        }
    }

});


$(document).on("click", ".editDepSubject", function () {
    var semValue = $('option:selected', $('select#depSemester')).val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "/faculty/get-subjectname",
        data: {semester: semValue},
        dataType: "json",
        success: function (response) {
            $('.openSubject').html('');
            var res = response.result;
            for (var i in res) {
                var item = res[i];
                $('.editopenSubject').append('<option  value="' + item.SubjectCode + '">' + item.SubjectCode + '(' + item.SubjectName + ')</option>')
            }

        }
    });
});




function toggleBox(toggled, checked) {
    var objList = document.getElementsByName(toggled)
    //alert('toggleBox function triggered');
    for (i = 0; i < objList.length; i++)
        objList[i].checked = checked;

}

$(document).on("change", ".semesterNumber", function () {
    var semValue = $('option:selected', $('select#semesterNumber')).val();
    var _currentDegree = $('#studentCurrentDegree').val();
    $('.showsgpi').html('');
    $('.show_sgpi').html('');
    $('.dualAndArch').html('');
    $('.dual_AndArch').html('');


    if (semValue != '0') {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var rollnum = $('#instroll').val()
        $.ajax({
            type: "POST",
            url: "/student/get-result",
            data: {RollNumber: rollnum},
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    $('.regNext').show();
                    var j = 1;
                    for (i = 1; i <= semValue; i++) {
                        if (_currentDegree == 'DUAL') {
                            if (i == 7 || i == 8) {
                                var _msgu = '(UG Level)';
                                var _msgp = '(PG Level)';
                            } else {
                                var _msgu = '';
                                var _msgp = '';
                            }
                        } else {
                            var _msgu = '';
                            var _msgp = '';
                        }
                        $('.showsgpi').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="sgpi_' + j + '"   name="studentsgpi[]">\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="cgpi_' + j + '"   name="studentcgpi[]">\n\
</div>\n\
</div>');
                        if (_currentDegree == 'DUAL') {
                            if (i == 7 || i == 8) {
                                $('.dualAndArch').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="pgsgpi_' + j + '"   name="studentpgsgpi[]">\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="pgcgpi_' + j + '"   name="studentpgcgpi[]">\n\
</div>\n\
</div>');
                            }
                        }
                        $('.show_sgpi').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control" id="show_sgpi_' + j + '"   readonly="" >\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control" id="show_cgpi_' + j + '"  readonly="" >\n\
</div>\n\
</div>');
                        if (_currentDegree == 'DUAL') {
                            if (i == 7 || i == 8) {
                                $('.dual_AndArch').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="show_pgsgpi_' + j + '"  readonly="">\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="show_pgcgpi_' + j + '"  readonly="">\n\
</div>\n\
</div>');
                            }
                        }
                        j++;
                    }
                    var res = response.result;
                    getStudentResult(res);

                } else {
                    if (response.result == 99) {
                        $('.regNext').hide();
                        toastr.error('This roll number allready registred in previous semster. you can use registred email id to fill choices.');
                    } else {
                        $('.regNext').hide();
                        toastr.error('Wrong Roll Number');
                    }

                }
            }
        });
    }
});

$(document).on("change", ".exitSemesterNumber", function () {
    var semValue = $('option:selected', $('select#exitSemesterNumber')).val();
    var _currentDegree = $('#studentCurrentDegree').val();
    $('.showsgpi').html('');
    $('.show_sgpi').html('');
    $('.dualAndArch').html('');
    $('.dual_AndArch').html('');


    if (semValue != '0') {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var rollnum = $('#instroll').val()
        $.ajax({
            type: "POST",
            url: "/student/old-get-result",
            data: {RollNumber: rollnum},
            dataType: "json",
            success: function (response) {
                if (response.status == true) {
                    $('.regNext').show();
                    var j = 1;
                    for (i = 1; i <= semValue; i++) {
                        if (_currentDegree == 'DUAL') {
                            if (i == 7 || i == 8) {
                                var _msgu = '(UG Level)';
                                var _msgp = '(PG Level)';
                            } else {
                                var _msgu = '';
                                var _msgp = '';
                            }
                        } else {
                            var _msgu = '';
                            var _msgp = '';
                        }
                        $('.showsgpi').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="sgpi_' + j + '"   name="studentsgpi[]">\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="cgpi_' + j + '"   name="studentcgpi[]">\n\
</div>\n\
</div>');
                        if (_currentDegree == 'DUAL') {
                            if (i == 7 || i == 8) {
                                $('.dualAndArch').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="pgsgpi_' + j + '"   name="studentpgsgpi[]">\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="pgcgpi_' + j + '"   name="studentpgcgpi[]">\n\
</div>\n\
</div>');
                            }
                        }
                        $('.show_sgpi').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control" id="show_sgpi_' + j + '"   readonly="" >\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgu + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control" id="show_cgpi_' + j + '"  readonly="" >\n\
</div>\n\
</div>');
                        if (_currentDegree == 'DUAL') {
                            if (i == 7 || i == 8) {
                                $('.dual_AndArch').append('<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">SGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="show_pgsgpi_' + j + '"  readonly="">\n\
</div>\n\
</div>\n\
<div class="form-group">\n\
<label class="control-label col-sm-12" for="railwaystation">CGPI Semester ' + j + ' ' + _msgp + '<span class="requiredfield">*</span></label>\n\
<div class="col-sm-12"> \n\
 <input type="text" class="form-control required" id="show_pgcgpi_' + j + '"  readonly="">\n\
</div>\n\
</div>');
                            }
                        }
                        j++;
                    }
                    var res = response.result;
                    getStudentResult(res);

                } else {
                    $('.regNext').hide();
                    toastr.error('Wrong Roll Number');

                }
            }
        });
    }
});

function getStudentResult(res) {
    for (var i in res) {
        var item = res[i];
        if (item.SemID == 'S01') {
            $('#sgpi_1').val(item.SGPI);
            $('#cgpi_1').val(item.CGPI);
        }
        if (item.SemID == 'S02') {
            $('#sgpi_2').val(item.SGPI);
            $('#cgpi_2').val(item.CGPI);
        }
        if (item.SemID == 'S03') {
            $('#sgpi_3').val(item.SGPI);
            $('#cgpi_3').val(item.CGPI);
        }
        if (item.SemID == 'S04') {
            $('#sgpi_4').val(item.SGPI);
            $('#cgpi_4').val(item.CGPI);
        }
        if (item.SemID == 'S05') {
            $('#sgpi_5').val(item.SGPI);
            $('#cgpi_5').val(item.CGPI);
        }
        $('#sgpi_1').prop('readonly', true);
        $('#cgpi_1').prop('readonly', true);
        $('#sgpi_2').prop('readonly', true);
        $('#cgpi_2').prop('readonly', true);
        $('#sgpi_3').prop('readonly', true);
        $('#cgpi_3').prop('readonly', true);
        $('#sgpi_4').prop('readonly', true);
        $('#cgpi_4').prop('readonly', true);
        $('#sgpi_5').prop('readonly', true);
        $('#cgpi_5').prop('readonly', true);
        $('#instroll').prop('readonly', true);
    }
}

$(document).on("change", ".downloadPDF", function () {
    var semValue = $('option:selected', $('select#downloadPDF')).val();
    window.location = "/student/get-pdf/" + semValue;
});



$(document).on("keyup", ".RollNumToName", function () {

    var rollnum = $('#RollNumToName').val();
    $('.stundentName').val('')
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "/faculty/get-studentname",
        data: {RollNumber: rollnum},
        dataType: "json",
        success: function (response) {
            $('.stundentName').val('');
            var res = response.result;
            $('.stundentName').val(res);
        }
    });
});



$(document).on("change", ".course", function () {
    var semValue = $('option:selected', $('select#course')).val();
    if (semValue == 'DASA') {
        $('#jeemainrankAIR').val('xxx');
        $('#jeemainrankcategory').val('xxx');
        $('#jeepercentile').val('xxx');
        document.getElementById("jeemainrankAIR").readOnly = true;
        document.getElementById("jeemainrankcategory").readOnly = true;
        document.getElementById("jeepercentile").readOnly = true;
    } else if (semValue == 'MEA') {
        $('#jeemainrankAIR').val('xxx');
        $('#jeemainrankcategory').val('xxx');
        $('#jeepercentile').val('xxx');
        document.getElementById("jeemainrankAIR").readOnly = true;
        document.getElementById("jeemainrankcategory").readOnly = true;
        document.getElementById("jeepercentile").readOnly = true;
    } else {
        $('#jeemainrankAIR').val('');
        $('#jeemainrankcategory').val('');
        $('#jeepercentile').val('');
        document.getElementById("jeemainrankAIR").readOnly = false;
        document.getElementById("jeemainrankcategory").readOnly = false;
        document.getElementById("jeepercentile").readOnly = false;
    }
});

$(document).ready(function () {
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });
});

