var DatatableColumnRenderingDemo = {
    init: function() {
        var a;
        a = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: "https://keenthemes.com/metronic/themes/themes/metronic/dist/preview/inc/api/datatables/demos/default.php"
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                theme: "default",
                "class": "",
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch"),
                delay: 400
            },
            rows: {
                callback: function(a, b, c) {}
            },
            columns: [ {
                field: "RecordID",
                title: "#",
                sortable: !1,
                width: 40,
                textAlign: "center",
                selector: {
                    "class": "m-checkbox--solid m-checkbox--brand"
                }
            }, {
                width: 200,
                field: "CompanyAgent",
                title: "Agent",
                template: function(a) {
                    var b = mUtil.getRandomInt(1, 14);
                    if (b > 8) output = '<div class="m-card-user m-card-user--sm">								<div class="m-card-user__pic">									<img src="https://keenthemes.com/metronic/themes/themes/metronic/dist/preview/assets/app/media/img/users/100_' + b + '.jpg" class="m--img-rounded m--marginless" alt="photo">								</div>								<div class="m-card-user__details">									<span class="m-card-user__name">' + a.CompanyAgent + '</span>									<a href="" class="m-card-user__email m-link">' + a.CompanyName + "</a>								</div>							</div>"; else {
                        var c = mUtil.getRandomInt(0, 7);
                        output = '<div class="m-card-user m-card-user--sm">								<div class="m-card-user__pic">									<div class="m-card-user__no-photo m--bg-fill-' + [ "success", "brand", "danger", "accent", "warning", "metal", "primary", "info" ][c] + '"><span>' + a.CompanyAgent.substring(0, 1) + '</span></div>								</div>								<div class="m-card-user__details">									<span class="m-card-user__name">' + a.CompanyAgent + '</span>									<a href="" class="m-card-user__email m-link">' + a.CompanyName + "</a>								</div>							</div>";
                    }
                    return output;
                }
            }, {
                field: "ShipCountry",
                title: "Ship Country",
                width: 150,
                template: function(a) {
                    return a.ShipCountry + " - " + a.ShipCity;
                }
            }, {
                field: "ShipAddress",
                title: "Ship Address",
                width: 200
            }, {
                field: "CompanyEmail",
                title: "Email",
                width: 150,
                template: function(a) {
                    return '<a class="m-link" href="mailto:' + a.CompanyEmail + '">' + a.CompanyEmail + "</a>";
                }
            }, {
                field: "Status",
                title: "Status",
                template: function(a) {
                    var b = {
                        1: {
                            title: "Pending",
                            "class": "m-badge--brand"
                        },
                        2: {
                            title: "Delivered",
                            "class": " m-badge--metal"
                        },
                        3: {
                            title: "Canceled",
                            "class": " m-badge--primary"
                        },
                        4: {
                            title: "Success",
                            "class": " m-badge--success"
                        },
                        5: {
                            title: "Info",
                            "class": " m-badge--info"
                        },
                        6: {
                            title: "Danger",
                            "class": " m-badge--danger"
                        },
                        7: {
                            title: "Warning",
                            "class": " m-badge--warning"
                        }
                    };
                    return '<span class="m-badge ' + b[a.Status].class + ' m-badge--wide">' + b[a.Status].title + "</span>";
                }
            }, {
                field: "Type",
                title: "Type",
                template: function(a) {
                    var b = {
                        1: {
                            title: "Online",
                            state: "danger"
                        },
                        2: {
                            title: "Retail",
                            state: "primary"
                        },
                        3: {
                            title: "Direct",
                            state: "accent"
                        }
                    };
                    return '<span class="m-badge m-badge--' + b[a.Type].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + b[a.Type].state + '">' + b[a.Type].title + "</span>";
                }
            }, {
                field: "Actions",
                width: 110,
                title: "Actions",
                sortable: !1,
                overflow: "visible",
                template: function(a, b, c) {
                    return '						<div class="dropdown ' + (c.getPageSize() - b <= 4 ? "dropup" : "") + '">							<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">                                <i class="la la-ellipsis-h"></i>                            </a>						  	<div class="dropdown-menu dropdown-menu-right">						    	<a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>						    	<a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>						    	<a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>						  	</div>						</div>						<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">							<i class="la la-edit"></i>						</a>						<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">							<i class="la la-trash"></i>						</a>					';
                }
            } ]
        }), $("#m_form_status").on("change", function() {
            a.search($(this).val(), "Status");
        }), $("#m_form_type").on("change", function() {
            a.search($(this).val(), "Type");
        }), $("#m_form_status, #m_form_type").selectpicker();
    }
};

jQuery(document).ready(function() {
    DatatableColumnRenderingDemo.init();
});