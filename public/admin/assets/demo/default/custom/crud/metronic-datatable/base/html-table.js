var DatatableHtmlTableDemo = {
    init: function() {
        var a;
        a = $(".m-datatable").mDatatable({
            data: {
                saveState: {
                    cookie: !1
                }
            },
            search: {
                input: $("#generalSearch")
            },
            columns: [ {
                field: "DepositPaid",
                type: "number"
            }, {
                field: "OrderDate",
                type: "date",
                format: "YYYY-MM-DD"
            }, {
                field: "Status",
                title: "Status",
                template: function(a) {
                    var b = {
                        1: {
                            title: "Pending",
                            "class": "m-badge--brand"
                        },
                        2: {
                            title: "Delivered",
                            "class": " m-badge--metal"
                        },
                        3: {
                            title: "Canceled",
                            "class": " m-badge--primary"
                        },
                        4: {
                            title: "Success",
                            "class": " m-badge--success"
                        },
                        5: {
                            title: "Info",
                            "class": " m-badge--info"
                        },
                        6: {
                            title: "Danger",
                            "class": " m-badge--danger"
                        },
                        7: {
                            title: "Warning",
                            "class": " m-badge--warning"
                        }
                    };
                    return '<span class="m-badge ' + b[a.Status].class + ' m-badge--wide">' + b[a.Status].title + "</span>";
                }
            }, {
                field: "Type",
                title: "Type",
                template: function(a) {
                    var b = {
                        1: {
                            title: "Online",
                            state: "danger"
                        },
                        2: {
                            title: "Retail",
                            state: "primary"
                        },
                        3: {
                            title: "Direct",
                            state: "accent"
                        }
                    };
                    return '<span class="m-badge m-badge--' + b[a.Type].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + b[a.Type].state + '">' + b[a.Type].title + "</span>";
                }
            } ]
        }), $("#m_form_status").on("change", function() {
            a.search($(this).val().toLowerCase(), "Status");
        }), $("#m_form_type").on("change", function() {
            a.search($(this).val().toLowerCase(), "Type");
        }), $("#m_form_status, #m_form_type").selectpicker();
    }
};

jQuery(document).ready(function() {
    DatatableHtmlTableDemo.init();
});