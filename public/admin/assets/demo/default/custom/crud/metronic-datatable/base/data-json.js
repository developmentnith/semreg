var DatatableJsonRemoteDemo = {
    init: function() {
        var a, b;
        a = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: "https://keenthemes.com/metronic/themes/themes/metronic/dist/preview/inc/api/datatables/datasource/default.json",
                pageSize: 10
            },
            layout: {
                theme: "default",
                "class": "",
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch")
            },
            columns: [ {
                field: "RecordID",
                title: "#",
                width: 50,
                sortable: !1,
                selector: !1,
                textAlign: "center"
            }, {
                field: "OrderID",
                title: "Order ID"
            }, {
                field: "ShipCountry",
                title: "Ship Country",
                template: function(a) {
                    return a.ShipCountry + " - " + a.ShipCity;
                }
            }, {
                field: "ShipCity",
                title: "Ship City",
                width: 110
            }, {
                field: "ShipName",
                title: "Ship Name",
                responsive: {
                    visible: "lg"
                }
            }, {
                field: "ShipAddress",
                title: "Ship Address",
                responsive: {
                    visible: "lg"
                }
            }, {
                field: "ShipDate",
                title: "Ship Date",
                type: "date",
                format: "MM/DD/YYYY"
            }, {
                field: "Status",
                title: "Status",
                template: function(a) {
                    var b = {
                        1: {
                            title: "Pending",
                            "class": "m-badge--brand"
                        },
                        2: {
                            title: "Delivered",
                            "class": " m-badge--metal"
                        },
                        3: {
                            title: "Canceled",
                            "class": " m-badge--primary"
                        },
                        4: {
                            title: "Success",
                            "class": " m-badge--success"
                        },
                        5: {
                            title: "Info",
                            "class": " m-badge--info"
                        },
                        6: {
                            title: "Danger",
                            "class": " m-badge--danger"
                        },
                        7: {
                            title: "Warning",
                            "class": " m-badge--warning"
                        }
                    };
                    return '<span class="m-badge ' + b[a.Status].class + ' m-badge--wide">' + b[a.Status].title + "</span>";
                }
            }, {
                field: "Type",
                title: "Type",
                template: function(a) {
                    var b = {
                        1: {
                            title: "Online",
                            state: "danger"
                        },
                        2: {
                            title: "Retail",
                            state: "primary"
                        },
                        3: {
                            title: "Direct",
                            state: "accent"
                        }
                    };
                    return '<span class="m-badge m-badge--' + b[a.Type].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + b[a.Type].state + '">' + b[a.Type].title + "</span>";
                }
            }, {
                field: "Actions",
                width: 110,
                title: "Actions",
                sortable: !1,
                overflow: "visible",
                template: function(a, b, c) {
                    return '						<div class="dropdown ' + (c.getPageSize() - b <= 4 ? "dropup" : "") + '">							<a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">                                <i class="la la-ellipsis-h"></i>                            </a>						  	<div class="dropdown-menu dropdown-menu-right">						    	<a class="dropdown-item" href="#"><i class="la la-edit"></i> Edit Details</a>						    	<a class="dropdown-item" href="#"><i class="la la-leaf"></i> Update Status</a>						    	<a class="dropdown-item" href="#"><i class="la la-print"></i> Generate Report</a>						  	</div>						</div>						<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Download">							<i class="la la-download"></i>						</a>						<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Edit settings">							<i class="la la-cog"></i>						</a>					';
                }
            } ]
        }), b = a.getDataSourceQuery(), $("#m_form_status").on("change", function() {
            a.search($(this).val(), "Status");
        }).val(void 0 !== b.Status ? b.Status : ""), $("#m_form_type").on("change", function() {
            a.search($(this).val(), "Type");
        }).val(void 0 !== b.Type ? b.Type : ""), $("#m_form_status, #m_form_type").selectpicker();
    }
};

jQuery(document).ready(function() {
    DatatableJsonRemoteDemo.init();
});