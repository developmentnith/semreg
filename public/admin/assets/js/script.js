var rowCache;

$(document).ready(function () {
    var dt = $('#example').dataTable();
    dt.fnDestroy();
});

$(document).ready(function () {
    var dt = $('#example2').dataTable();
    dt.fnDestroy();
});

$(document).ready(function () {
    rowCache = [];

    var url = 'http://www.json-generator.com/api/json/get/clmDuyndua?indent=2';
    var table = $('#example').DataTable({
        ajax: url,
        createdRow: function (row, data, dataIndex) {
            $(row).attr('id', 'row-' + dataIndex);
        },
        rowReorder: {
            dataSrc: 'order',
        },
        columns: [{
                data: 'order'
            }, {
                data: 'name'
            }, {
                data: 'place'
            }]
    });

    table.rowReordering();

    table.on('mousedown', 'tbody tr', function () {
        var $row = $(this);
        var r = table.rows(function (i, data) {
            return data.order == $row.children().first().text();
        });

        if (r[0].length > 1)
            r = r.rows(r[0][0]);

        rowCache.push({selector: 'example', row: r});
    });

    table.on('mouseup', mouseUp);
});

$(document).ready(function () {
    var url = 'http://www.json-generator.com/api/json/get/cnmZgfsBKa?indent=2';
    var table = $('#example2').DataTable({
        ajax: url,
        createdRow: function (row, data, dataIndex) {
            $(row).attr('id', 'row-' + dataIndex);
        },
        rowReorder: {
            dataSrc: 'order',
        },
        columns: [{
                data: 'order'
            }, {
                data: 'name'
            }, {
                data: 'checkbox'
            }]
    });
    table.rowReordering();

    table.on('mousedown', 'tbody tr', function () {
        var $row = $(this);
        var r = table.rows(function (i, data) {
            return data.order == $row.children().first().text();
        });

        if (r[0].length > 1)
            r = r.rows(r[0][0]);

        rowCache.push({selector: 'example2', row: r});
    });

    table.on('mouseup', mouseUp);
});

function mouseUp(event)
{
    var id = $(document.elementsFromPoint(event.clientX, event.clientY))
            .filter('table')
            .attr('id');

    if (id && event.currentTarget.id != id)
    {
        rowCache.every(function (el, i) {
            if (el.selector != id)
            {
                var data = el.row.data();

                if (data.length > 0) {

                    if (!data[0].checkbox)
                        data[0].checkbox = "<input type='checkbox' />"

                    el.row.remove().draw();
                    var $target = $("#" + id).DataTable();

                    if ($target.rows()[0].length > 0)
                    {
                        var rowsArray = $target.rows().data().toArray();
                        data[0].order = rowsArray[rowsArray.length - 1].order + 1;
                    } else
                        data[0].order = 1;

                    $target.rows.add(data.toArray()).draw();
                }
            }
        }
        );
    }

    rowCache = [];
}

$(document).ready(function () {
    $('body').on('mouseenter', 'input', function () {
        $('.btn').prop('disabled', true);
    });
    $('body').on('mouseout', 'input', function () {
        $('.btn').prop('disabled', false);
    });
});	


function FillSameAddress(f) {
  if(f.filladdress.checked == true) {
    f.coaddress.value = f.peraddress.value;
  }
}
$(document).ready(function () {
  //called when key is pressed in textbox
  $(".digit").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $(".errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});






    

    $(document).ready(function() {

      $(".only-numeric").bind("keypress", function (e) {

          var keyCode = e.which ? e.which : e.keyCode
          if (!(keyCode >= 48 && keyCode <= 57)) {
            $(".error").css("display", "inline");
            return false;
          }else{
            $(".error").css("display", "none");
          }
      });
    });
    
 $(document).ready(function() {   
$('.numbers').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});
 });





 $(document).ready(function () {
        $("#dialog").dialog({
            modal: false,
            autoOpen: false,
            title: "Help How to Input Name in Hindi",
            width: 500,
            height: 350
        });
        $("#btnShow").click(function () {
            $('#dialog').dialog('open');
        });
    });
   
//     $(document).ready("#studentnamehindi").on('input',function () {
//        var url = "https://translation.googleapis.com/language/translate/v2?key=API_Key";
//            url += "&source=" + "EN";
//            url += "&target=" + "HI";
//            url += "&q=" + escape($("#studentnamehindi").val());
//            $.get(url, function (data, status) {
//                $("#studentnamehindi").val(data.data.translations[0].translatedText);
//            });
//        });



$(document).ready(function() {
    $('#markscgpi').on('keyup keypress blur change', function(e) {
    if($(this).val()>100)
    {
        alert('Kindly Check The Percentage/CGPI Value');
        $("#markscgpi").val('');
        return false;
    }
  var key = e.which;
  if ((key < 48 || key > 57) && !(key == 8 || key == 9 || key == 13 || key == 37 || key == 39 || key == 46) ){
    return false;
  }
});
});