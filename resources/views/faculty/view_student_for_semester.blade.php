@extends('layouts.faculty_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">View Student For Semester</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-9">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                   View Student For Semester
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                          
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <table class="table table-hover" id="myTable">  
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">Roll number</th>
                                    <th scope="col">Student Name</th>
                                    <th scope="col">Father Name </th>
                                    <th scope="col">Contact Number</th>
                                    <th scope="col">Email-ID</th>
                                    <th scope="col" width="10%">Action</th>
                                    <th scope="col" width="15%">
                                        <label> Approve All<input type='checkbox' class='regSemesterApprove' value='current[]' /> </label>
                                        <!--<button type="button" class="btn btn-primary regSemesterApprove">Approve</button>--> 
                                    </th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($student as $oldstu)
                                @if($oldstu->UpdateToken == 'N')
                                <tr><td>{{$oldstu->InstituteRollNumber}}</td>
                                    <td>{{$oldstu->StudentName}}</td>
                                    <td>{{$oldstu->FatherName}}</td>
                                    <td>{{$oldstu->ContactNumberStudent}}</td>
                                    <td>{{$oldstu->EmailIDStudent}}</td>
                                    <td>                                       
                                        <a class="btn btn-info btn-xs" href="{{ url('faculty/approve-single-student/'.$oldstu->currentSem.'/'.$oldstu->RegUserID.'') }}" onclick="return confirm('Are you sure you want to Approve Student Record?')" style="cursor: pointer;"> <i class="fa fa-check" style="padding: 10px 5px;" aria-hidden="true"></i>
                                        <a class="btn btn-info btn-xs" onclick="showAjaxModal('{{ url('faculty/view-student-sem-data/view_sgpi/'.$oldstu->id.'') }}');" style="cursor: pointer; margin-left: 10px;"> <i class="fa fa-eye" style="padding: 10px 5px;" aria-hidden="true"></i>
                                    </td>
                                    <td>
                                        <input type="hidden" name="currentSem" class="currentSemNum" value="{{$oldstu->currentSem}}">
                                         <input type="checkbox" class="checkSemesterValue" id="checkSemesterValue" name="current[]"  value="{{$oldstu->RegUserID}}">
                                         <!--<a href="{{ url('faculty/approve-single-student/'.$oldstu->currentSem.'/'.$oldstu->RegUserID.'') }}"  class="btn btn-primary regSemesterApprove">Approve</a>-->
                                    </td>
                                  @endif
                                </tr>
                                @empty

                                @endforelse


                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->


@endsection