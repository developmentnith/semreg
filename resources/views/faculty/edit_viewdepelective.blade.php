<div class="row">
    <div class="col-md-12">
        <?php
        $result = App\DepElectiveChoice::find($param2);
        ?>
        <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('faculty/edit-view-department-elective') }}"> 
            @csrf
            <fieldset> 
                <div class="form-group">
                    <label for="email">Pool:</label>
                    <select class="form-control getSubjectByPool" name="pool" required="">
                        <option value="">Select Pool</option>
                        <option <?php if ($result->Pool == 'A') echo 'selected'; ?> value="A">A</option>
                        <option <?php if ($result->Pool == 'B') echo 'selected'; ?> value="B">B</option>
                        <option <?php if ($result->Pool == 'C') echo 'selected'; ?> value="C">C</option>
                        <option <?php if ($result->Pool == 'D') echo 'selected'; ?> value="D">D</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="email">Semester:</label>
                    <select class="form-control editDepSubject" name="semester" id='depSemester' required="">
                        <option value="">Select Semester</option>
                        @foreach($depelective as $se)
                        @if($se->electiveType == 'DEPARTMENT ELECTIVE')
                        <option <?php if ($se->Semester == $result->Semester) echo 'selected'; ?> value="{{$se->Semester}}">{{$se->Semester}}</option>
                        @endif
                        @endforeach

                    </select>
                </div>

                <div class="form-group">

                    <label for="email">Subject Code:</label> 
                    <select class="form-control editopenSubject" name="subject" required="">
                        <option value="<?= $result->SubjectID ?>"><?= $result->SubjectID ?></option>
                    </select>
                </div>


                <input type="hidden" name="id" value="<?= $param2 ?>">
                <input class="btn btn-success pull-right" type="submit" value="Submit">
            </fieldset>
        </form>
    </div>
</div>
