@extends('layouts.faculty_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Float Department Elective</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-9">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Student No Dues Details
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="javascript:void()" data-toggle="modal" data-target="#addOpenModal" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Student No Dues Add/Clear
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <table class="table table-hover" id="myTable">  
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">Roll number</th>
                                    <th scope="col">Semester</th>
                                    <th scope="col">No Due Detail </th>
                                    <th scope="col">Remarks(if any)</th>
                                    <th scope="col">No Dues at (Department/Branch)</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Cleared[Y/N]</th>
                                    <th scope="col" width="15%">Action</th>
                                </tr>

                            </thead>


                            <tbody>
                                @foreach($nodues as $detail)
                                <tr>
                                    <td>{{$detail->RollNumber}}</td>
                                    <td>{{$detail->Semester}}</td>
                                    <td>{{$detail->NoDueDetail}}</td>
                                    <td>{{$detail->NoDueRemark}}</td>
                                    <td>{{$detail->NoDueByDept}}</td>
                                    <td>{{$detail->NoDueDate}}</td>
                                    <td>{{$detail->isCleared}}</td> 
                                    <td>
                                        <a class="btn btn-danger btn-xs" href="{{ url('faculty/delete-nodues/'.$detail->id.'') }}" onclick="return confirm('Are you sure you want to delete this?')" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-trash" aria-hidden="true"></i></a>

                                        <a class="btn btn-primary btn-xs" href="{{ url('faculty/clear-nodues/'.$detail->id.'/'.$usname.'') }}" onclick="return confirm('Are you sure you want to clear no dues this?')"  style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i> Clear No Dues</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->

<div class="modal fade" id="addOpenModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Add No Dues for Students</h3>

            </div>
            <div class="modal-body">    
                <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('faculty/add-student-nodues') }}"> 
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <input class="form-control RollNumToName"  id ="RollNumToName" required="" placeholder="Enter Roll Number"  name="rollnumber" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control stundentName" readonly required=""   name="naam" type="text">
                        </div>

                        <div class="form-group">
                            <select class="form-control" name="semester" required="">
                                <option value="">Select Semester</option>
                                <option value="S01">S01</option>
                                <option value="S02">S02</option>
                                <option value="S03">S03</option>
                                <option value="S04">S04</option>
                                <option value="S05">S05</option>
                                <option value="S06">S06</option>
                                <option value="S07">S07</option>
                                <option value="S08">S08</option>
                                <option value="S09">S09</option>
                                <option value="S10">S10</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Enter No Dues Detail"   name="noduedetail" type="text">
                        </div>

                        <div class="form-group">
                            <input class="form-control"  placeholder="Remarks (if any)"   name="remarks" type="text">
                        </div>


                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection