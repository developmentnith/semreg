@extends('layouts.faculty_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Registed Students</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-2">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Department By Students Lists
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <div class="m-portlet__head-tools">
                                    <ul class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--left m-tabs-line--primary">
                                        @php $current_tab = $populatebranshort[0]->DepartmentShortName;   @endphp 
                                        @foreach ($populatebranshort as $populatebr)
                                        @php $tab_class = ($populatebr->DepartmentShortName==$current_tab) ? 'active' : '' ;   @endphp                                          
                                        <li class="{{$tab_class}}"><a data-toggle="tab" href="#{{$populatebr->DepartmentShortName}}">{{ $populatebr->DepartmentShortName }}</a></li>                                     
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="tab-content">
                            @php $current_tab = $populatebranshort[0]->DepartmentShortName;   @endphp 
                            @foreach ($populatebranshort as $populatebr)
                            @php 
                            $tab = $populatebr->DepartmentShortName;
                            $content_class = ($tab==$current_tab) ? 'active' : '' ;                           
                            @endphp       
                            <div id="home" class="tab-pane fade in {{$content_class}}">
                                <table class="table table-hover" id="{{$tab}}"> 
                                    <thead style="background: #f1f2f7;">
                                        <tr>
                                            <th scope="col">Roll Number</th>
                                            <th scope="col">Student Name</th>
                                            <th scope="col">Email ID</th>
                                            <th scope="col">Branch</th>
                                            <th scope="col">Current Semester</th>
                                            <th scope="col">Active [Y/N]</th>
                                            <!--<th scope="col" width="10%">Action</th>-->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($student as $row)
                                        <tr>                                    
                                            <td>{{$row->InstituteRollNumber}}</td>
                                            <td>{{$row->StudentName}}</td>
                                            <td>{{$row->EmailIDStudent}}</td>
                                            <td>{{$row->AdmissionBranch}}</td>
                                            <td>{{ 'S0'.$row->currentSem}}</td>
                                            <td>{{$row->IsActive}}</td>
                                            <!--<td></td>-->                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->

@endsection