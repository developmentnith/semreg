@extends('layouts.faculty_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Float Department Elective</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-9">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Department Elective
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                    
                                    <span>
                                         <button type="button" class="btn btn-lg btn-info launch-modal" > <i class="la la-plus"></i>Click to Add Department Elective</button>
                                       
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <table class="table table-hover" id="myTable">  
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">Subject Code</th>
                                    <th scope="col">Degree</th>
                                    <th scope="col">Semester</th>
                                    <th scope="col">Float Year</th>
                                    <th scope="col">Floated By</th>
                                    <th scope="col">Pool</th>

                                    <th scope="col" width="10%">Action</th>
                                    <th scope="col" width=""><input type='checkbox' class='checkAllAud' type='checkbox' value='current[]' /></th>
                                </tr>

                            </thead>
                            <tbody>
                                @foreach($deptelectiveconfig as $row)
                                <tr>                                    
                                    <td> <?= $row->SubjectCode ?></td>
                                    <td><?= $row->Degree ?></td>
                                    <td><?= $row->Semester ?></td>
                                    <td><?= $row->FloatYear ?></td>
                                    <td><?= $row->FloatedBy ?></td>   
                                    <td><?= $row->Pool ?></td>                                   
                                    <td>
                                        <a class="btn btn-danger btn-xs" href="{{ url('faculty/delete-depelective/'.$row->id.'') }}" onclick="return confirm('Are you sure you want to delete this?')" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-trash" aria-hidden="true"></i></a>

                                        <a class="btn btn-primary btn-xs" onclick="showAjaxModal('{{ url('faculty/modal-pop/edit_depelective/'.$row->id.'') }}');"  style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>  
                                    <td> 
                                        <input type="checkbox" class="checkValue" id="checkValue" name="current[]"  value="{{$row->id}}">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->

<div class="modal fade" id="addOpenModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Add Department Elective</h3>

            </div>
            <div class="modal-body">    
                <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('faculty/add-department-elective') }}"> 
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <label for="email">Degree:</label>
                            <select class="form-control" name="degree" required="">
                                <option value="">Select Degree</option>
                                @foreach($depelective as $de)
                                @if($de->electiveType == 'DEPARTMENT ELECTIVE')
                                <option value="{{$de->Degree}}">{{$de->Degree}}</option>
                                @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Semester:</label>
                            <select class="form-control depSemester" name="semester" id='depSemester' required="">
                                <option value="">Select Semester</option>
                                @foreach($depelective as $se)
                                @if($se->electiveType == 'DEPARTMENT ELECTIVE')
                                <option value="{{$se->Semester}}">{{$se->Semester}}</option>
                                @endif
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="email">Year:</label>
                            <select class="form-control" name="year" required="">
                                <option value="">Select Year</option>
                                @php
                                date_default_timezone_set('Asia/Calcutta');
                                $startingYear = (int)date('Y');
                                $endingYear = $startingYear + 1; 
                                @endphp
                                @for ($i = $startingYear;$i <= $endingYear;$i++)      
                                <option value="{{$i}}">{{$i}}</option>';
                                @endfor
                            </select>
                        </div>
                        <div class="form-group">

                            <label for="email">Subject Code:</label>
                            <select class="form-control openSubject" name="subject" required="">
                                <option value="">Select Code</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="email">Pool:</label>
                            <select class="form-control" name="pool" required="">
                                <option value="">Select Pool</option>
                                <option value="A">A</option>
                                <option value="B">B</option>
                                <option value="C">C</option>
                                <option value="D">D</option>
                            </select>
                        </div>

                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
$(document).ready(function(){
	$('.launch-modal').click(function(){
		$('#addOpenModal').modal({
			backdrop: 'static'
		});
	}); 
});
</script>
@endsection