@extends('layouts.faculty_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">View Department Elective</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-6">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Registered Student Details for Department Elective
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">

                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <table class="table table-hover" id="subjectTable"> 
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">Roll Number</th>
                                    <th scope="col">Semester</th>
                                    <th scope="col">Subject Code</th>
                                    <th scope="col">Pool</th>
                                    <th scope="col">Status</th>
                                    <th scope="col" width="10%">Action</th>
                                    <th scope="col" width=""><input type='checkbox' class='checkAlldepelective' value='current[]' /></th>
                                </tr>

                            </thead>
                            <tbody>
                                @forelse($student as $stud)
                                <tr>                                    
                                    <td>{{$stud->RollNumber}}</td>  
                                    <td>{{$stud->Semester}}</td>
                                    <td>{{$stud->SubjectID}}</td>
                                    <td>{{$stud->Pool}}</td>
                                    <td>{{$stud->Status}}</td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" onclick="showAjaxModal('{{ url('faculty/modal-pop/edit_viewdepelective/'.$stud->id.'') }}');"  style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>
                                    <td> 
                                        <input type="checkbox" class="checkDepValue" id="checkDepValue" name="current[]"  value="{{$stud->id}}">
                                    </td>
                                </tr>   
                                @empty
                                <tr><td>No Record Found</td></tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->

<div class="modal fade" id="createSubjectModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Add Subject</h3>

            </div>
            <div class="modal-body">    
                <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('admin/add-subject') }}"> 
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Subject Name"  name="subjectname" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Subject Code" name="subjectcode" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Credit" required="" name="credit" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Theory Practical" r name="theorypractical" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Degree"  name="degree" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Semester"  name="semester" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Group" name="group" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Subject Type"  name="SubjectType" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturetheory" type="text">
                        </div>
                        <div class="form-group"> 
                            <input class="form-control" required="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturePractical" type="text">
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="isactive">
                                <option value="active">Active</option>
                                <option value="deactive">Deactive</option>
                            </select>
                        </div>

                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editSubjectModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Edit Subject</h3>

            </div>
            <div class="modal-body">    
                <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('admin/edit-subject') }}"> 
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <input class="form-control"   name="subjectname" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"   name="subjectcode" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Credit" required="" name="credit" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"  name="theorypractical" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"   name="degree" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"  name="semester" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"  name="group" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"   name="SubjectType" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturetheory" type="text">
                        </div>
                        <div class="form-group"> 
                            <input class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturePractical" type="text">
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="isactive">
                                <option value="active">Active</option>
                                <option value="deactive">Deactive</option>
                            </select>
                        </div>

                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection