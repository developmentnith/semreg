@extends('layouts.faculty_app')

@section('content')
 <div class="m-grid__item m-grid__item--fluid m-wrapper">

                <!-- BEGIN: Subheader -->
                <!--<div class="m-subheader ">
                    <div class="d-flex align-items-center">
                        <div class="mr-auto">
                            <h3 class="m-subheader__title ">Dashboard</h3>
                        </div>
                        <div>
                            <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
					<span class="m-subheader__daterange-label">
						<span class="m-subheader__daterange-title"></span>
                            <span class="m-subheader__daterange-date m--font-brand"></span>
                            </span>
                            <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-angle-down"></i>
                            </a>
                            </span>
                        </div>
                    </div>
                </div>-->
                <!-- END: Subheader -->
                <div class="m-content">
                    <!--Begin::Section-->
                        <div class="m-portlet ">
                            <div class="m-portlet__body  m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <!--begin::Total Profit-->
                                        <div class="m-widget24">                     
                                            <div class="m-widget24__item">
                                                <h4 class="m-widget24__title">
                                                  
                                                    
                                                </h4><br>
                                                <span class="m-widget24__desc">
                                                   
                                                </span>
                                                <span class="m-widget24__stats m--font-brand coin-qty">
                                                    
                                                </span>  
                                                <br/>
                                                <span class="m-widget24__stats m--font-brand coin-sec">
                                                   
                                                </span>    
                                                <!--<div class="m--space-10"></div>-->
                                                <div class="progress m-progress--sm">
                                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span class="m-widget24__change">
                                                    
                                                </span>
                                                <span class="m-widget24__number" style="color: #f4516c;">
                                                 
                                                </span>
                                            </div>                    
                                        </div>
                                        <!--end::Total Profit-->
                                    </div>
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <!--begin::New Feedbacks-->
                                        <div class="m-widget24">
                                             <div class="m-widget24__item">
                                                <h4 class="m-widget24__title">
                                                   
                                                   
                                                </h4><br>
                                                <span class="m-widget24__desc">
                                                 
                                                </span>
                                                <span class="m-widget24__stats m--font-info coin-qty">
                                                  
                                                </span>   
                                                <br/>
                                                <span class="m-widget24__stats m--font-info coin-sec">
                                                   
                                                </span>   
                                                <!--<div class="m--space-10"></div>-->
                                                <div class="progress m-progress--sm">
                                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                <span class="m-widget24__change">
                                                
                                                </span>
                                                <span class="m-widget24__number" style="color: #34bfa3;">
                                              
                                                </span>
                                            </div>      
                                        </div>
                                        <!--end::New Feedbacks--> 
                                    </div>
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <!--begin::New Orders-->
                                        <div class="m-widget24">
                                            <div class="m-widget24__item">
                                                <h4 class="m-widget24__title">
                                                 
                                                </h4><br>
                                                <span class="m-widget24__desc">
                                                 
                                                </span>
                                                <span class="m-widget24__stats m--font-danger coin-qty">
                                                  
                                                </span> 
                                                <br/>
                                                <span class="m-widget24__stats m--font-danger coin-sec">
                                                 
                                                </span>     
                                                <!--<div class="m--space-10"></div>-->
                                                <div class="progress m-progress--sm">
                                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                               
                                            </div>      
                                        </div>
                                        <!--end::New Orders--> 
                                    </div>
                                    <div class="col-md-12 col-lg-6 col-xl-3">
                                        <!--begin::New Users-->
                                       
                                        <!--end::New Users--> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--=========2==========-->
                                <div class="row">
                                    <div class="col-xl-5" style="padding-right: 0px; margin-bottom: -25px;">
                                        <!--begin:: Widgets/Top Products-->
                                        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                                            
                                            <div class="m-portlet__body">
                                                <!--begin::Widget5-->
                                                
                                                <!--end::Widget 5-->
                                            </div>
                                        </div>
                                        <!--end:: Widgets/Top Products-->
                                    </div>
                                    
                                    <div class="col-xl-7" style="height: 362px;">
                                        <!--begin:: Widgets/Blog-->
                                        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height  m-portlet--rounded-force">
                                            <div class="m-portlet__body">
                                                <div class="m-widget14">
                                                    <div class="m-widget14__header">
                                                        <h3 class="m-widget14__title" style="color:#170ca7;">         
                                                        </h3>
                                                        <span class="m-widget14__desc"></span>
                                                    </div>
                                                    
                                                </div>  
                                                                                 
                                            </div>
                                        </div>
                                        <div class="row chart-bottom" style="padding-right: 0px;">
                                    <div class="col-xl-6 col-lg-12" style="height: 176px; padding-right: 0px;">
                                        
                                        <!--End::Portlet-->
                                    </div>
                                    
                                </div>

                        <!----------3------------->

                        </div> 
                    </div>
                    <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
                            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title" style="padding-top: 0px;">
                        <h3 class="m-portlet__head-text">
                      
                        </h3>
                    </div>
                </div>
            </div>
                            
                        </div>
                </div>
            </div>

        </div>
@endsection
