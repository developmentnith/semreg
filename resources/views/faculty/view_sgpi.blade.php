<div class="row">
    <div class="col-md-12">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Student Roll No.</th>
                    <th>Semester</th>
                    <th>SGPI</th>
                    <th>CGPI</th>
                </tr>
            </thead>
            <tbody>
                @foreach($semdata as $se)
                @php
                $semester =   explode(',', $se->Semester);
                $SGPI =   explode(',', $se->SGPI);
                $CGPI =  explode(',', $se->CGPI);
                @endphp
                @for($i = 0; $i < count($semester); $i++)
                <tr>
                    <td>{{ $se->RollNumber }}</td>
                    @if($semester[$i]==10)
                    <td>{{ 'S'. $semester[$i] }}</td>
                    @else
                    <td>{{ 'S0'. $semester[$i] }}</td>
                    @endif
                    <td>{{ $SGPI[$i] }}</td>
                    <td>{{ $CGPI[$i] }}</td>                   
                </tr>
                @endfor
                @endforeach
            </tbody>
        </table>
        <p style="text-align: center">Current Semester is : 
            @if($semdata[0]->currentSem==10)
            {{ 'S'.$semdata[0]->currentSem}}
             @else
             {{ 'S0'.$semdata[0]->currentSem}}
           @endif
        </p>

    </div>
</div>
