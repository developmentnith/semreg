@extends('layouts.user_app') 

@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">All Registed Students</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">


            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="tab-content">                                                       
                            <table class="table table-hover regStudent"> 
                                <thead style="background: #f1f2f7;">
                                    <tr>                                          
                                        <th scope="col">Student Name</th>
                                        <th scope="col">Father Name</th>
                                        <th scope="col">Roll Number</th>
                                        <!--<th scope="col">Email ID</th>-->
                                        <!--<th scope="col">Student Address</th>-->
                                        <!--<th scope="col">Gender</th>-->
                                        <th scope="col">Branch</th>
                                        <th scope="col">Admission IN</th>
                                        <th scope="col">Group</th>
                                        <th scope="col">Section</th>
                                        <!--<th scope="col" width="20%">Action</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($students as $row) 
                                    @if($row->cancelstatus == 'Active')
                                    @php
                                    $group =  Illuminate\Support\Facades\DB::table('StudentRollNumber')->where('StudentID', $row->studentID)->value('Grop');
                                    $Section =  Illuminate\Support\Facades\DB::table('StudentRollNumber')->where('StudentID', $row->studentID)->value('Section');
                                    @endphp
                                    <tr>                                    
                                        <td>{{$row->StudentName}}</td>
                                        <td>{{$row->FatherName}}</td>
                                        <td>{{$row->InstituteRollNumber}}</td>
                                        <!--<td>{{$row->EmailIDStudent}}</td>-->
                                        <!--<td>{{$row->PermanentAddress}}</td>-->
                                        <!--<td>{{$row->Gender}}</td>--> 
                                        <td>{{$row->AdmissionBranch}}</td>
                                        <td>{{$row->AdmissionIN}}</td>
                                        <td>{{$group}}</td>
                                        <td>{{$Section}}</td>
<!--                                        <td>
                                           <a class="btn btn-primary btn-xs" onclick="showAdminAjaxModal('{{ url('admin/edit-modal-pop/all_edit_student/'.$row->studentID.'') }}');" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                                <a href=""  class="btn btn-primary btn-xs" ><i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                            <a href="{{ url('admin/view-student/'.$row->studentID.'') }}"  class="btn btn-success btn-xs"><i style="padding: 10px 5px;" class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="{{ url('admin/delete-student/'.$row->studentID.'') }}" onclick="return confirm('Are you sure you want to update this?')"  class="btn btn-danger btn-xs"><i style="padding: 10px 5px;" class="fa fa-trash" aria-hidden="true"></i></a>
                                            @if($row->Status == 'N')
                                             <a href="{{ url('admin/update-student/'.$row->studentID.'') }}" onclick="return confirm('Are you sure you want to update this?')" type="button" class="btn btn-warning btn-xs"><i style="padding: 10px 5px;" class="fa fa-refresh" aria-hidden="true"></i></a>
                                            <a onclick="showAdminAjaxModal('{{ url('admin/update-modal-pop/all_allot_rollnumber/'.$row->studentID.'') }}');" class="btn btn-info btn-xs"><i style="padding: 10px 5px;" class="fa fa-refresh" aria-hidden="true"></i> </a>
                                            @endif
                                            @if($row->Status == 'Y')
                                            <a href="{{ url('admin/generate-pdf/'.$row->studentID.'') }}"  class="btn btn-info btn-xs"><i style="padding: 10px 5px;" class="fa fa-print" aria-hidden="true"></i></a>
                                            @endif
                                        </td>                                             -->
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>




                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>
</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->

@endsection