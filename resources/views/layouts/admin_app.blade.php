<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- begin::Head -->
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!-- /Added by HTTrack -->

    <head>
        <meta charset="utf-8" />

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'app.name') }}</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
        <!--begin::Web font -->
        <script src="../../../ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
WebFont.load({
google: {
"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
        },
        active: function () {
        sessionStorage.fonts = true;
        }
});
        </script>
        <!--end::Web font -->

        <!--begin::Global Theme Styles -->
        <link href="{{ asset('admin/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=poppins" rel="stylesheet">
        <!--============-->
        <link href="{{ asset('admin/assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('admin/assets/css/responsive.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="{{ asset('admin/assets/bower_components/bootstrap/dist/css/bootstrap.min.css') }}"  type="text/css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('admin/assets/bower_components/font-awesome/css/font-awesome.min.css')}}" type="text/css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset('admin/assets/bower_components/Ionicons/css/ionicons.min.css')}}" type="text/css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{{ asset('admin/assets/bower_components/jvectormap/jquery-jvectormap.css')}}" type="text/css">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('admin/assets/css/AdminLTE.min.css')}}" type="text/css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{ asset('admin/assets/css/skins/_all-skins.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{ asset('admin/assets/css/custom.css')}}" type="text/css"> 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"> 
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script>
$(function () {
$("#datepicker").datepicker({
changeMonth: true,
        changeYear: true,
        dateFormat: 'dd-MM-yy'
        });
});
        </script>
        <script>
            $(function () {
            $(".datepick").datepicker({
            changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd-mm-yyyy'

            });
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
            var d = new Date();
            function twoDigitDate(d){

            return ((d.getDate()).toString().length == 1) ? "0" + (d.getDate()).toString() : (d.getDate()).toString();
            };
            function twoDigitMonth(d){
            return ((d.getMonth() + 1).toString().length == 1) ? "0" + (d.getMonth() + 1).toString() : (d.getMonth() + 1).toString();
            };
            var today_ISO_date = d.getFullYear() + "-" + twoDigitMonth(d) + "-" + twoDigitDate(d); // in yyyy-mm-dd format

            document.getElementById('datepicker').setAttribute("value", today_ISO_date);
            var dd_mm_yyyy;
            $(".datepicker").change(function(){
            changedDate = $(this).val(); //in yyyy-mm-dd format obtained from datepicker
            var date = new Date(changedDate);
            dd_mm_yyyy = twoDigitDate(date) + "/" + twoDigitMonth(date) + "/" + date.getFullYear(); // in dd-mm-yyyy format
            $('.chdate').val(dd_mm_yyyy);
            });
            });
        </script>

        <script type="text/javascript">

            function checkForm(form)
            {
            // regular expression to match required date format
            re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
            var sdate = form.startdate.value;
            var edate = form.enddate.value;
            var d = new Date(),
                    month = '' + (d.getMonth() + 1),
                    day = '' + d.getDate(),
                    year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            var curdate = ([year, month, day].join('-'));
            if (sdate < curdate){
            alert('Start Date can not be less than current date');
            form.startdate.focus();
            return false;
            }
            if (sdate > edate){
            alert('End Date Can not be Greater than Start Date');
            form.enddate.focus();
            return false;
            }


            return true;
            }

        </script>


        <script type="text/javascript">
            function GetSelectedTextValue(ddval) {
            var selectedText = ddval.options[ddval.selectedIndex].innerHTML;
            var selectedValue = ddval.value;
            // var odd=["S01","S03","S05","S07","S09"];
            //var even=["S02","S04","S06","S08","S10"];


            var ses = document.getElementById("session").value;
            if (ses == 'July')
            {
            if ($.inArray(selectedValue, ['S01', 'S03', 'S05', 'S07', 'S09']) >= 0) {
            return true;
            }
            else{
            alert("Kindly Check Session");
            return false;
            }
            }
            if (ses == 'January')
            {
            if ($.inArray(selectedValue, ['S02', 'S04', 'S06', 'S08', 'S10']) >= 0) {
            return true;
            }
            else{
            alert("Kindly Check Session");
            return false;
            }

            }

            }
        </script>

        <style type="text/css">
            div#m_aside_left {
                background-image: url(admin/assets/images/bb.jpg) !important;
                background-size: cover !important;
            }
            .m-portlet__head-title {
                display: block !important;
                padding-top: 12px;
            }
            .m-brand__logo-wrapper img {
                width: 478px !important;
            }
        </style>
    </head>


    <body style="font-size: 12px !important;" class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
        @if(Session::has('message'))
        @if( Session::get('type') == 'success')
        <script>
            $(function () {
            toastr.success('{{ Session::get('message')}}');
            });
        </script>  
        @elseif(Session::get('type') == 'error')
        <script>
            $(function () {
            toastr.error('{{ Session::get('message')}}');
            });
        </script>
        @endif
        @endif
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">

            <!-- (Ajax Modal)-->
            <div class="modal fade" id="admin_modal_ajax" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="exampleModalLabel">Student Data</h3>
                        </div>
                        <div class="modal-body">    

                        </div> 
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- BEGIN: Header -->
            <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
                <div class="m-container m-container--fluid m-container--full-height">
                    <div class="m-stack m-stack--ver m-stack--desktop">
                        <!-- BEGIN: Brand -->
                        <div class="m-stack__item m-brand  m-brand--skin-dark ">
                            <div class="m-stack m-stack--ver m-stack--general">
                                <div class="m-stack__item m-stack__item--middle m-brand__logo">   
                                    <a href="" class="m-brand__logo-wrapper">
                                        <img alt="" src="{{url('/admin/assets/demo/default/media/img/logo/logo.png')}}" />
                                    </a>
                                </div>
                                <div class="m-stack__item m-stack__item--middle m-brand__tools" style="display: none;">

                                    <!-- BEGIN: Left Aside Minimize Toggle -->
                                    <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                                        <span></span>
                                    </a>
                                    <!-- END -->

                                    <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                    <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                        <span></span>
                                    </a>
                                    <!-- END -->

                                    <!-- BEGIN: Responsive Header Menu Toggler -->
                                    <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
                                        <span></span>
                                    </a>
                                    <!-- END -->

                                    <!-- BEGIN: Topbar Toggler -->
                                    <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                        <i class="flaticon-more"></i>
                                    </a>
                                    <!-- BEGIN: Topbar Toggler -->
                                </div>
                            </div>
                        </div>
                        <!-- END: Brand -->
                        <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                            <!-- BEGIN: Horizontal Menu -->
                            <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

                            <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
                            </div>
                            <!-- END: Horizontal Menu -->
                            <!-- BEGIN: Topbar -->
                            <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">

                                <div class="m-stack__item m-topbar__nav-wrapper">
                                    <ul class="m-topbar__nav m-nav m-nav--inline">                            

                                        <li class="m-nav__item m-topbar__languages m-dropdown m-dropdown--small m-dropdown--header-bg-fill m-dropdown--arrow m-dropdown--align-right m-dropdown--mobile-full-width" m-dropdown-toggle="click" style="margin-right: -12px;">
                                            <a href="#" class="m-nav__link m-dropdown__toggle">
                                                <span class="m-nav__link-text" style="color : #fff;">
                                                    {{ Auth::user()->name }}
                                                </span>
                                            </a>

                                        </li>
                                        <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" m-dropdown-toggle="click">
                                            <a href="#" class="m-nav__link m-dropdown__toggle">
                                                <span class="m-topbar__userpic">	
                                                    <img src="{{url('/admin/assets/app/media/img/users/user.png')}}" class="m--img-rounded m--marginless" alt=""/>
                                                </span>
                                                <span class="m-topbar__username m--hide">Nick</span>
                                            </a>
                                            <div class="m-dropdown__wrapper">
                                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                <div class="m-dropdown__inner">
                                                    <div class="m-dropdown__header m--align-center" style="background: url({{url('/admin/assets/app/media/img/misc/user_profile_bg.jpg')}}); background-size: cover;">
                                                        <div class="m-card-user m-card-user--skin-dark">

                                                            <div class="m-card-user__details">
                                                                <span class="m-card-user__name m--font-weight-500">{{ Auth::user()->name }}</span>
                                                                <a href="#" class="m-card-user__email m--font-weight-300 m-link">{{ Auth::user()->email }}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="m-dropdown__body">
                                                        <div class="m-dropdown__content">
                                                            <ul class="m-nav m-nav--skin-light">
                                                                <li class="m-nav__section m--hide">
                                                                    <span class="m-nav__section-text">Section</span>
                                                                </li>
                                                                <li class="m-nav__item">
                                                                    <a href="#" class="m-nav__link">
                                                                        <i class="m-nav__link-icon flaticon-profile-1"></i>    
                                                                        <span class="m-nav__link-text">My Profile</span>
                                                                    </a>
                                                                </li>


                                                                <li class="m-nav__item">
                                                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                        document.getElementById('logout-form').submit();" class="m-nav__link">
                                                                        <i class="m-nav__link-icon la la-users"></i>    
                                                                        <span class="m-nav__link-text">Logout</span>
                                                                    </a>
                                                                </li>
                                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                    @csrf
                                                                </form>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- END: Topbar -->
                        </div>
                    </div>
                </div>
            </header>
            <!-- END: Header -->
            <!-- begin::Body -->
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

                <!-- BEGIN: Left Aside -->
                <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>

                <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
                    <!-- BEGIN: Aside Menu -->
                    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
                        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                            <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
                                <a href="#" class="m-menu__link ">
                                    <i class="m-menu__link-icon la la-university"></i>
                                    <span class="m-menu__link-title"> 
                                        <span class="m-menu__link-wrap">     
                                            <span class="m-menu__link-text">Dashboard</span>
                                              <!--<span class="m-menu__link-badge">
                                              <span class="m-badge m-badge--danger">2</span>
                                             </span>-->
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/all-registred-students') }}" class="m-menu__link m-menu__toggle">
                                    
                                    <i class="m-menu__link-icon la la-bullseye"></i> 

                                    <span class="m-menu__link-text">All Registred Students</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/registred-students') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i> 

                                    <span class="m-menu__link-text">Registred Students by Department</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/canceled-students') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i> 

                                    <span class="m-menu__link-text">Canceled Students</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/subject') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i>

                                    <span class="m-menu__link-text">Subject</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/open-elective') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i>

                                    <span class="m-menu__link-text">Open Elective Allotment</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/create-user') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i>

                                    <span class="m-menu__link-text">Create User</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/student-sgpi-cgpi') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i>

                                    <span class="m-menu__link-text">Student SGPI/CGPI</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/date-configuration') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i>

                                    <span class="m-menu__link-text">Date Configuration</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>
                            <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/admin/import-excel') }}" class="m-menu__link m-menu__toggle">
                                    <i class="m-menu__link-icon la la-bullseye"></i>

                                    <span class="m-menu__link-text">Import Excel</span>
                                    <!--<i class="m-menu__ver-arrow la la-angle-right"></i>-->
                                </a>

                            </li>


                        </ul>
                    </div>
                    <!-- END: Aside Menu -->
                </div>


                @yield('content')


                <!-- begin::Footer -->
                <!--                <footer class="m-grid__item		m-footer ">
                                    <div class="m-container m-container--fluid m-container--full-height m-page__container">
                                        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                                            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                                                <span class="m-footer__copyright">
                                                    Copyright © 2018 National Institute of Technology Hamirpur 
                                                </span>
                                            </div>
                                            <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                                                <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                                                    <li class="m-nav__item">
                                                        <a href="#" class="m-nav__link">
                                                            <span class="m-nav__link-text">Privacy Policy</span>
                                                        </a>
                                                    </li>
                                                    <li class="m-nav__item">
                                                        <a href="#" class="m-nav__link">
                                                            <span class="m-nav__link-text">Terms of Use</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </footer>-->
                <!-- end::Footer -->
            </div>
            <!-- end:: Page -->
            <!-- begin::Scroll Top -->
            <div id="m_scroll_top" class="m-scroll-top">
                <i class="la la-arrow-up"></i>
            </div>
            <!-- end::Scroll Top -->
            <!-- begin::Quick Nav -->
            <ul class="m-nav-sticky" style="margin-top: 30px;">
                <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="">
                    <a href="#"><i class="la la-phone"></i></a>
                </li>
                <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="">
                    <a href="#" target="_blank"><i class="la la-envelope"></i></a>
                </li>
                <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="">
                    <a href="#" target="_blank"><i class="la la-globe"></i></a>
                </li>
                <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="" data-placement="left" data-original-title="">
                    <a href="#" target="_blank"><i class="la la-whatsapp"></i></a>
                </li>
            </ul>
            
            <!-- begin::Quick Nav -->
            <!--begin::Global Theme Bundle -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
            <script src="{{ asset('admin/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
            <!--end::Global Theme Bundle -->
            <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
             <script src="{{ asset('filter/js/filterDropDown.js')}}"></script>  
            <script src="{{ asset('admin/assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
            <!--begin::Page Vendors -->
            <script src="{{ asset('admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript"></script>
            <!--end::Page Vendors -->
            <script src="{{ asset('admin/assets/bower_components/fastclick/lib/fastclick.js')}}"></script>
            <!-- AdminLTE App -->
            <script src="{{ asset('admin/assets/dist/js/adminlte.min.js')}}"></script>
            <!-- Sparkline -->
            <script src="{{ asset('admin/assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>


            <script src="{{ asset('admin/assets/js/custom.js') }}" type="text/javascript"></script>
            <script src="{{ asset('js/admin.js?v=').time() }}"></script>   
            <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
            <script type="text/javascript">
                                                                        toastr.options = {
                                                                        "closeButton": false,
                                                                                "debug": false,
                                                                                "newestOnTop": false,
                                                                                "progressBar": false,
                                                                                "positionClass": "toast-bottom-left",
                                                                                "preventDuplicates": false,
                                                                                "onclick": null,
                                                                                "showDuration": "300",
                                                                                "hideDuration": "1000",
                                                                                "timeOut": "5000",
                                                                                "extendedTimeOut": "1000",
                                                                                "showEasing": "swing",
                                                                                "hideEasing": "linear",
                                                                                "showMethod": "fadeIn",
                                                                                "hideMethod": "fadeOut"
                                                                        }


            </script>
            <script>
                $(document).ready(function () {
                $('#myTable2').DataTable();
                $('.regStudent').DataTable({
                "aLengthMenu": [100]
                }); 
                
                 $('.allregStudent').DataTable({
                    // Definition of filter to display					
                    filterDropDown: {
                        columns: [                           
                            {
                                idx: 2
                            },
                            {
                                idx: 3
                            },
                            {
                                idx: 4
                            },
                            {
                                idx: 5
                            },
                            {
                                idx: 6
                            },
                            {
                                idx: 7
                            }
                            
                        ],
                        bootstrap: true
                    },
                    "aLengthMenu": [100,200,300,500]
                });
                });
            </script>
            <script type="text/javascript">
                function showAdminAjaxModal(url)
                {

                // SHOWING AJAX PRELOADER IMAGE
//        jQuery('#modal_ajax .modal-body').html('<div style="text-align:center;margin-top:200px;"><img src="assets/images/preloader.gif" /></div>');

                // LOADING THE AJAX MODAL
                $(window).scrollTop(0);
                $('#admin_modal_ajax').modal('show', {backdrop: 'true'});
                // SHOW AJAX RESPONSE ON REQUEST SUCCESS
                $.ajax({
                url: url,
                        success: function (response)
                        {
                        $('#admin_modal_ajax .modal-body').html(response);
                        }
                });
                }
            </script>
    </body>

</html> 
