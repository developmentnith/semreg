@extends('layouts.student_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper"> 

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Register for  Open Elective</h3>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--tabs">
            <!--begin::Form-->
            <div class="m-portlet">
                <!--begin::Form-->
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">

                    <div class="m-portlet__body">   
                        <div class="form-group m-form__group row"> 

                            <div class="row">
                                <div class="cointainer">

                                    <form id="regForm" action="{{ url('student/update-student-sem') }}" method="post" autocomplete="off"  enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="instroll">Institute Roll Number<span class="requiredfield">*</span></label>
                                            <div class="col-sm-12">  
                                                <input type="hidden" value="{{ $user_id }}" name="user_id">
                                                <input type="text" tabindex="2" readonly="" class="form-control" required="" id="instroll"  name="instroll"  value="{{ $roll_no }}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="admissionbranch">Select Passed Semester<span class="requiredfield">*</span></label>

                                            <div class="col-sm-12">   
                                                @php  
                                                $numberValue = '10';
                                                @endphp
                                                <select class="form-control exitSemesterNumber" required tabindex="36" id="exitSemesterNumber" name = "semester_number" >
                                                    <option value="" selected>Please Select</option>                                                         
                                                    @for ($i = 0; $i <= $numberValue; $i++) 
                                                    @if($i == 5)
                                                    <option value="{{ $i }}">S0{{ $i }}</option>
                                                    @endif
                                                    @endfor
                                                </select>
                                            </div>
                                        </div>
                                        <div class="showsgpi"></div> 
                                        <div class="dualAndArch"></div>

                                        <div class="col-sm-7">
                                            <br>
                                            <div style="overflow:auto;">
                                                <div style="margin: 0 auto;"> 
                                                    <button type="submit"  class="btn btn-flickr">Next</button>
                                                </div>
                                            </div>

                                        </div>
                                    </form>  


                                </div>

                            </div>              
                        </div>              
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Form-->
        </div>

        <!--Begin::Section-->


        <!--End::Section-->
    </div>




</div>

</div>
<!-- end:: Body -->

</div>


@endsection
