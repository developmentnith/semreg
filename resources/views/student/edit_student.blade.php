<div class="row">
    <div class="col-md-12">   

        <form  action="{{ url('student/edit-student') }}" method="post"  enctype="multipart/form-data">
            @csrf            
            <fieldset> 
                <div class="form-group">
                    <label>Student Name</label>
                    <input class="form-control"   value="<?= $result[0]->StudentName ?>"  name="StudentName" type="text">
                    <input class="form-control"    value="<?= $result[0]->studentID ?>"  name="studentID" type="hidden" readonly="">
                </div>
                <div class="form-group">
                    <label>Student Father Name</label>
                    <input class="form-control"   value="<?= $result[0]->FatherName ?>" name="fathername" type="text">
                </div>
                <div class="form-group">
                    <label>Student Father Name Hindi</label>
                    <input class="form-control"   value="<?= $result[0]->FatherNameHindi ?>" name="fathernamehinid" type="text">
                </div>
                <div class="form-group">
                    <label>Student Mother Name</label>
                    <input class="form-control"   value="<?= $result[0]->MotherName ?>" name="mothername" type="text">
                </div>
                <div class="form-group">
                    <label>Student Mother Name Hindi</label>
                    <input class="form-control"   value="<?= $result[0]->MotherNameHindi ?>" name="mothernamehindi" type="text">
                </div>
                <div class="form-group">
                    <label>Permanent Address</label>
                    <textarea class="form-control" name="peraddress"><?= $result[0]->PermanentAddress ?></textarea>
                </div>
<!--                <div class="form-group">
                    <label>Correspondence Address</label>
                    <textarea class="form-control" name="coaddress"><?= $result[0]->CoAddress ?></textarea>
                </div>-->
                <div class="form-group">
                    <img src="{{url('/upload/studentphoto/'.$result[0]->StudentPhoto)}}" class="kycimg photoImage" id="photoImage" style="width:120px;"><br>
                    <label>Update Photo</label>
                    <input type="file" class="kycphoto required" tabindex="45" name="photograph" accept="image/x-png,image/jpg,image/jpeg" onchange="readURLPhoto(this);">
                </div>
                <button type="submit" class="btn btn-success">Submit</button> 
            </fieldset>

        </form>
    </div>
</div>
