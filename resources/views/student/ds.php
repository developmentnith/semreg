@extends('layouts.student_app')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::Total Profit-->
                        <div class="m-widget24">                     
                            <div class="m-widget24__item">
                                <select class="form-control downloadPDF"  name="" id="downloadPDF">
                                    <option value="" selected>Download PDF By Semester</option>     
                                    @php $i = 1; @endphp
                                    @foreach ($semesterdata as $sem)
                                    @if($sem->degree == $course )
                                    @if($active_status == 'Y')
                                    @if($approved_sem == $sem->semNum)
                                    <option data-bind="{{ $sem->fullName}}" value="{{ $sem->semNum}}">{{ $sem->fullName}}</option>
                                    @endif 
                                    @endif 
                                    @endif 
                                    @php $i++ @endphp
                                    @endforeach   
                                </select>
                            </div>                    
                        </div>
                        <!--end::Total Profit-->


                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Feedbacks-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">

                                    Reg. For Semester
                                </h4><br>

                                <br/>
                                <span class="m-widget24__stats m--font-info coin-sec">
                                    0
                                </span>   
                                <!--<div class="m--space-10"></div>-->
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 8%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">

                                </span>

                            </div>      
                        </div>
                        <!--end::New Feedbacks--> 
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Orders-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">

                                    NO Dues
                                </h4><br>

                                <br/>
                                <span class="m-widget24__stats m--font-danger coin-sec">
                                    0
                                </span>     
                                <!--<div class="m--space-10"></div>-->
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 9%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">

                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Semester</th>
                                                <th>No Due Detail</th>
                                                <th>By Department</th>
                                                <th>Remarks</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($nodues as $ndu)
                                            <tr>
                                                <th scope="row"></th>
                                                <td>{{$ndu->Semester}}</td>
                                                <td>{{$ndu->NoDueDetail}}</td>
                                                <td>{{$ndu->NoDueByDept}}</td>
                                                <td>{{$ndu->NoDueRemark}}</td>
                                            </tr>
                                            @empty
                                            <tr><td colspan="4">No Nodues listed for you</td>
                                                @endforelse
                                        </tbody>
                                    </table>




                                </span>

                            </div>      
                        </div>
                        <!--end::New Orders--> 
                    </div>
                    <div class="col-xl-3">
                        <!--begin::New Users-->

                        <!--end::New Users--> 
                    </div>
                </div>
            </div>
        </div>

        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title" style="padding-top: 0px;">
                        <h3 class="m-portlet__head-text">
                            Your Registraion
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <table class="table table-hover">
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">Roll Number</th>
                                    <th scope="col">Student Name</th>
                                    <th scope="col">Email ID</th>
                                    <th scope="col">Branch</th>
                                    <th scope="col">Verified</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$userrolln}}</td>
                                    <td>{{$username}}</td>
                                    <td>{{$useremail}}</td>  
                                    <td>{{$userbranch}}</td>
                                    <td>{{$verifiedstatus}}</td>
                                    <td><span class="m-badge  m-badge--success m-badge--wide">{{ $userstatus }}</span></td>
                                    <td></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-6">
                        <div class="m-widget24">
                            <div class="m-widget24__item">



                                <span class="m-widget24__change">
                                    <h5>SGPI/CGPI Uploaded By Admin</h5>

                                    @if($userdeg==='DUAL')

                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Semester#</th>
                                                <th>SGPI</th>
                                                <th>CGPI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($sgpicgpiadm as $row)
                                            <tr>
                                                @if($row->UGPGTag=='UG')
                                                <td>{{$row->Semester}}</td>
                                                <td>{{$row->SGPI}}</td>
                                                <td>{{$row->CGPI}}</td>
                                                @endif    
                                                @if($row->UGPGTag=='PG')
                                                <td>{{$row->Semester}}</td>
                                                <td>{{$row->SGPI}}</td>
                                                <td>{{$row->CGPI}}</td>
                                                @endif    
                                            </tr>

                                            @empty
                                            <tr><td colspan="4">No SGPI/CGPI uploaded by admin</td></tr>
                                            @endforelse
                                        </tbody>
                                    </table>






                                    @else
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Semester#</th>
                                                <th>SGPI</th>
                                                <th>CGPI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($sgpicgpiadm as $row)
                                            <tr>

                                                <td>{{$row->Semester}}</td>
                                                <td>{{$row->SGPI}}</td>
                                                <td>{{$row->CGPI}}</td>
                                            </tr>
                                            @empty
                                            <tr><td colspan="4">No SGPI/CGPI uploaded by admin</td></tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    @endif

                                </span>

                            </div>      
                        </div>

                    </div>
                    <div class="col-md-6">
                        <!--Start of Showing Student uploaded CGPI-->  

                        <h5>SGPI/CGPI Updated By Student</h5>

                        @if($userdeg==='DUAL')

                        <table class="table">
                            <thead>
                                <tr>
                                    <th >Semester#</th>
                                    <th>SGPI(UG)</th>
                                    <th>CGPI(UG)</th>
                                    <th>SGPI(PG)</th>
                                    <th>CGPI(PG)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sgpicgpistu as $strow) 

                                @php

                                $sem = explode(',',$strow->Semester);

                                $sgpi = explode(',',$strow->SGPI);
                                $cgpi = explode(',',$strow->CGPI);
                                $pg_sgpi = explode(',',$strow->PGSGPI);
                                $pg_cgpi = explode(',',$strow->PGCGPI);

                                @endphp
                                @for ($i = 0; $i < count($sem); $i++)
                                <tr>

                                    @if($sem[$i]<7)
                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                    <td></td>
                                    <td></td>
                                    @endif

                                    @if($sem[$i]==7)
                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                    <td>{{$pg_sgpi[0]}}</td>
                                    <td>{{$pg_cgpi[0]}}</td>
                                    @endif                                   
                                    @if($sem[$i]==8)
                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                    <td>{{$pg_sgpi[1]}}</td>
                                    <td>{{$pg_cgpi[1]}}</td>
                                    @endif  
                                    @if($sem[$i]==9)
                                    <td>{{$sem[$i]}}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$pg_sgpi[3]}}</td>
                                    <td>{{$pg_cgpi[3]}}</td>
                                    @endif  
                                    @if($sem[$i]==10)
                                    <td>{{$sem[$i]}}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$pg_sgpi[4]}}</td>
                                    <td>{{$pg_cgpi[4]}}</td>
                                    @endif  

                                </tr>
                                @endfor
                                @empty
                                <tr>
                                    <td colspan="4">No SGPI/CGPI uploaded by Student</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>






                        @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th >Semester#</th>
                                    <th>SGPI</th>
                                    <th>CGPI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sgpicgpistu as $strow) 

                                @php

                                $sem = explode(',',$strow->Semester);

                                $sgpi = explode(',',$strow->SGPI);
                                $cgpi = explode(',',$strow->CGPI);
                                @endphp
                                @for ($i = 0; $i < count($sem); $i++)
                                <tr>

                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                </tr>
                                @endfor
                                @empty
                                <tr>
                                    <td colspan="4">No SGPI/CGPI uploaded by Student</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>

                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

</div>



@endsection
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

