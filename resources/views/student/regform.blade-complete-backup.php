@extends('layouts.student_app')

@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Student Registration Form </h3>
            </div>

        </div>

    </div>
    <!-- END: Subheader -->

    <div class="m-content">

        <div class="m-portlet m-portlet--tabs">



            <!--begin::Form-->
            <div class="m-portlet">

                <!--begin::Form-->


                <div class="m-portlet__body">   
                    <div class="form-group m-form__group row">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif 
                        @csrf  
                        <h3 class="m-subheader__title ">Basic Details</h3>   
                        <h4><span class="error" style="color: red; display: none">* Input digits (0 - 9)</span></h4>
                        <!-- One "tab" for each step in the form: -->
                        <div class="tab">  
                            <form id="regForm" action="{{ url('student/regform-add') }}" method="post"  enctype="multipart/form-data">
                                @csrf
                                <div class="firstFomr validate">    
                                    <div class="row">   
                                        <div class="col-sm-3">  

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="admissionin">Admission IN<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" tabindex="1" class="form-control" required="" id="admissionin" placeholder="Admission Program" value="{{$course}}" readonly  name="admissionin">
                                                </div>
                                            </div>
                                            @if($studenttyp=="oldstudent")
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="instroll">Institute Roll Number<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" tabindex="2" class="form-control" required="" id="instroll" placeholder="Rollnumber"  name="instroll"  value="{{ old('instroll') }}">
                                                </div>
                                            </div>
  
                                            @else
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="admissionthrough">Admission Through<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required course" required=""  tabindex="2" id ="course" name = "admissionthrough" >
                                                        <option value="" selected>Please Select</option>
                                                        @if($course=="BTech" || $course=="DUAL" || $course=="BArch")    
                                                        <option value="JEEMAIN">JEEMAIN</option>
                                                        <option value="DASA">DASA</option>
                                                        <option value="MEA">MEA</option> 
                                                        @endif

                                                        @if($course=="MTech" || $course=="MArch")    
                                                        <option value="CCMN">CCMN</option>
                                                        <option value="SPONSORED">Sponsored</option>
                                                        @endif

                                                    </select>
                                                </div>
                                            </div>
                                            @endif                                   

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="studentname">Student Name <span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required" required="" tabindex="3" id="studentname" placeholder="Student Name (as it appear in your Matric Certificate)"  value="{{ $usrname }}" name="studentname">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="studentnamehindi">Student  Name in Hindi<span class="requiredfield">*</span>  <div class="popup hinditab" id="btnShow"> Help</div> <div id="dialog" style="display: none" align = "center">
                                                        <span class="popuptext" id="myPopup">First Type the name in English and press spacebar.<br> if the Conversion of name from English to Hindi is not Correct then please us the below mentioned Google Link. <br><a target = "_new" href="https://translate.google.co.in/#view=home&op=translate&sl=auto&tl=hi"><font color="red"> Convert you Name to hindi using Google</font></a><br>Copy it and paste in the inputbox.</span>
                                                    </div></label>

                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control required" required="" tabindex="4" id="studentnamehindi" placeholder="Student Name in Hindi"  name="studentnamehindi" value="{{ old('studentnamehindi') }}">

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="fathername">Father  Name<span class="requiredfield">*</span> </label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control required" required="" tabindex="5" id="fathername" placeholder="Father Name as in your Matricalation Certificate"  name="fathername" value="{{ old('fathername') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="fathernamehindi">Father Name in Hindi<span class="requiredfield">*</span><div class="popup hinditab" onclick="myPopupFunction1()">Help<span class="popuptext" id="myPopup1">Convert you Name to hindi using <a target = "_new" href="https://translate.google.co.in/#view=home&op=translate&sl=auto&tl=hi">Google</a><br>Copy it and paste in the inputbox.</span></div></label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control required" required="" tabindex="6" id="fathernamehindi" placeholder="Father Name in Hindi"  name="fathernamehindi" value="{{ old('fathernamehindi') }}">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="mothername">Mother  Name<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control required" required="" tabindex="7" id="mother_name" placeholder="Mother Name "  name="mothername" value="{{ old('mothername') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="mothernamehindi">Mother Name in Hindi<span class="requiredfield">*</span><div class="popup hinditab" onclick="myPopupFunction2()">Help<span class="popuptext" id="myPopup2">Convert you Name to hindi using <a target = "_new" href="https://translate.google.co.in/#view=home&op=translate&sl=auto&tl=hi">Google</a><br>Copy it and paste in the inputbox.</span></div></label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control required" required = "" tabindex="8" id="mothernamehindi" placeholder="Mother Name in Hindi"  name="mothernamehindi" value="{{ old('mothernamehindi') }}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="dob">Date of Birth<span class="requiredfield">*</span></label>  
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required" required="" tabindex="9"readonly="true" id="datepicker" placeholder="DD-MM-YYYY" name="dob" value="{{ old('dob') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="dob">Gender<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  

                                                    <select class="form-control required" required="" tabindex="10" id="gender" name="gender" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="MALE">Male</option>
                                                        <option value="FEMALE">Female</option>
                                                        <option value="OTHER">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="category">Category<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" tabindex="11" id="category" name="category" >
                                                        <option value="" selected>Please Select</option>                                                                                                               
                                                        <option value="OP">OP</option>
                                                        <option value="SC">SC</option>
                                                        <option value="ST">ST</option>
                                                        <option value="OBC">OBC</option>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-sm-3"> 
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="religion">Religion<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" id="religion" tabindex="12" name = "religion" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="Hindu">Hindu</option>
                                                        <option value="Muslim">Muslim</option>
                                                        <option value="Christian">Christian</option>
                                                        <option value="Parsi">Parsi</option>
                                                        <option value="Buddhist">Buddhist</option>
                                                        <option value="Jain">Jain</option>
                                                        <option value="Sikh">Sikh</option>
                                                        <option value="Other">Other</option>
                                                        <option value="Not-Stated">Not-Stated</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="PWD">Weather Physically Challanged<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" tabindex="13" id="pwd" name = "PWD" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="NO" selected="">NO</option>
                                                        <option value="YES">YES</option>
                                                    </select>
                                                </div>
                                            </div>     

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="contactnumberstudent">Contact Number Student<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control digit" value="{{$phone}}" required="" maxlength="10" tabindex="14" id="contactnumberstudent" placeholder="Phone Number"  name="contactnumberstudent" value="{{ old('contactnumberstudent') }}"><span class="errmsg"></span>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="contactnumberfather">Contact Number Father</label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control digit" maxlength="10" tabindex="15" id="contactnumberfather" placeholder="Phone Number"  name="contactnumberfather" value="{{ old('contactnumberfather') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="contactnumbermother">Contact Number Mother</label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control digit" maxlength="10" tabindex="16" id="contactnumbermother" placeholder="Phone Number"  name="contactnumbermother" value="{{ old('contactnumbermother') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="email">Student Email ID</label>
                                                <div class="col-sm-12">
                                                    <input  type="email" class="form-control checkEmail" tabindex="17" id="studentemail" placeholder="Enter email" name="studentemail" value="{{ $eml }}"> 
                                                    <span id="msg"></span> 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="email">Father Email:</label>
                                                <div class="col-sm-12">
                                                    <input  type="text" class="form-control" tabindex="18" id="fatheremail" placeholder="Enter Father EMAIL ID" name="fatheremail" value="{{ old('fatheremail') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="bonafiedstate">Candidate Bonafied State<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">          
                                                    <select class="form-control required" tabindex="19" id="bonafiedstate" name = "bonafiedstate" >
                                                        <option value="" selected>Please Select</option>
                                                        @foreach ($populatestate as $populatest)
                                                        <option value="{{ $populatest->state }}">{{ $populatest->state }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="ruralurban">Candidate From (Rural/Urban)<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">          
                                                    <select class="form-control required" required="" tabindex="20" id="ruralurban" name = "ruralurban" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="Rural">Rural</option>
                                                        <option value="Urban">Urban</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="guardianname">Guardian Name (enter Father Name if no Guardian)<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control required" required="" tabindex="21" id="guardianname" placeholder="Guardian Name"  name="guardianname" value="{{ old('guardianname') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="contactnumberguardian">Contact Number Guardian</label>
                                                <div class="col-sm-12">          
                                                    <input type="text" class="form-control digit" tabindex="22" id="contactnumberguardian"  maxlength="10" placeholder="Phone Number"  name="contactnumberguardian" value="{{ old('contactnumberguardian') }}">
                                                </div>
                                            </div>


                                        </div>
                                        <div class="col-sm-3"> 


                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="peraddress">Permanent Address<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <textarea class="form-control required" required="" tabindex="24" id="peraddress" placeholder="Permanent Address"  name="peraddress" value="{{ old('peraddress') }}"></textarea>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="coaddress">Correspondence Address<span class="requiredfield">*</span></label ><div><label style="font-size:10px; margin-left: 15px; float: top ;margin-top: 2px; " ><a onclick="FillSameAddress(this.form)" style="color: blue; cursor: pointer;"> Same as Permanent Address</a> <span style="display:inline-block; vertical-align: middle;"><input type="checkbox" onclick="FillSameAddress(this.form)" style="display: inline-block;" id="filladdress" name="filladdress"/></span></label></div>
                                                <div class="col-sm-12">  
                                                    <textarea class="form-control required" required="" tabindex="23" id="coaddress" placeholder="Correspondence Address"  name="coaddress" value="{{ old('coaddress') }}"></textarea>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="railwaystation">Nearest Railway Station Near Home Town<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required" required="" tabindex="28" id="railwaystation" placeholder="Nearest Railway Station"  name="railwaystation" value="{{ old('railwaystation') }}" >
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="feewaiver">Fee Waiver Claimed<span class="requiredfield">*</span></label>

                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required=""  id="feewaiver" name = "feewaiver" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>                                                        
                                                    </select>   
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="annualincome">Annual Family Income(in Rupees)<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <!--<input type="text" class="form-control digit" tabindex="26" id="annualincome" placeholder="Annual Family Income"  name="annualincome" value="{{ old('annualincome') }}">-->
                                                    <select class="form-control required" required=""  id="annualincome" name = "annualincome" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="< 1 Lac">< 1 Lac</option>
                                                        <option value="Between 1-5 Lac">Between 1-5 Lac</option>
                                                        <option value="Between 5-8 Lac">Between 5-8 Lac</option>
                                                        <option value="> 8 Lac">> 8 Lac</option>   
                                                    </select>   
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="hostelrequired">Hostel Required<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" tabindex="27" id="hostelrequired" name = "hostelrequired" >
                                                        <option value="">Please Select</option>
                                                        <option value="YES" selected>YES</option>
                                                        <option value="NO">NO</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="admissionbranch">Admission in Branch<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" tabindex="36" id="admissionbranch" name = "admissionbranch" >
                                                        <option value="" selected>Please Select</option>
                                                        @foreach ($populatebranshort as $populatebr)
                                                        <option value="{{ $populatebr->DepartmentShortName }}">{{ $populatebr->DepartmentFullName }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                            @if($studenttyp=="oldstudent")

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="admissionbranch">Select Passed Semester<span class="requiredfield">*</span></label>
                                                <input type="hidden" id='studentCurrentDegree' value="{{ $course }}">
                                                <div class="col-sm-12">   
                                                    @php  
                                                    if($course == 'DUAL' || $course == 'BArch'){
                                                    $numberValue = '10';
                                                    }else{
                                                    $numberValue = '8';
                                                    }
                                                    @endphp
                                                    <select class="form-control semesterNumber" required tabindex="36" id="semesterNumber" name = "semester_number" >
                                                        <option value="" selected>Please Select</option>                                                         
                                                        @for ($i = 0; $i <= $numberValue; $i++) 
                                                        <option value="{{ $i }}">{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="showsgpi"></div> 
                                            <div class="dualAndArch"></div> 
                                            @endif

                                            @if(($course=="BTech" || $course=="DUAL" || $course=="BArch") && ($studenttyp=="newstudent") )
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="jeemainrollnumber">JEE Main Application/(Ref No in case of DASA)<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required" required="" tabindex="28" id="jeemainrollnumber" maxlength="20" placeholder="JEE Main Application/Roll No"  name="jeemainrollnumber" value="{{ old('jeemainrollnumber') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">   
                                                <label class="control-label col-sm-12" for="jeemainrankAIR">JEE Main CRL Rank<span  class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required"  required="" tabindex="29" maxlength="20" id="jeemainrankAIR" placeholder="JEE Main CRL Rank"  name="jeemainrankAIR" value="{{ old('jeemainrankAIR') }}">
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="jeemainrankcategory">JEE Main Category Rank</label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control " tabindex="30" id="jeemainrankcategory" placeholder="JEE Main Rank Category"  name="jeemainrankcategory" value="{{ old('jeemainrankcategory') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="jeepercentile">JEE Main Percentile <span  class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required" tabindex="30" required="" id="jeepercentile" placeholder="JEE Percentile"  name="jeepercentile" value="{{ old('jeepercentile') }}">
                                                </div>
                                            </div>

                                            @endif
                                            @if($studenttyp=="newstudent")
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="exampassedcountry">Examination Passed Country<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select  class="form-control" required="" tabindex="31" id="exampassedcountry" name="exampassedcountry">
                                                        <option value="" selected>Please Select</option>
                                                        @foreach ($populatecountry as $populatecn)
                                                        <!--<option value="{{ $populatecn->name }}">{{ $populatecn->name }}</option>-->
                                                        <option  {{($populatecn->name == 'India' ) ? 'selected' : '' }} value="{{ $populatecn->name }}"  >{{ $populatecn->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>



                                        </div>
                                        <div class="col-sm-3"> 

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="plustwoboard">+2 Board<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" tabindex="32" id="plustwoboard" name = "plustwoboard" >
                                                        <option value="" selected>Please Select</option>
                                                        @foreach ($populateboard as $rowboard)
                                                        <option value="{{ $rowboard->BoardName }}">{{ $rowboard->BoardName }}</option>
                                                        @endforeach

                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="exampassedstate">Seat Allotment Quota<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" tabindex="32" id="seetallotment" name = "seetallotment" >
                                                        <option value="" selected>Please Select</option>                                                       
                                                        <option value="HP-State">HP-State</option>
                                                        <option value="Outside-State">Outside-State</option>                                                       
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="exampassedstate">State of Eligibility<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  


                                                    <select class="form-control required" required="" tabindex="32" id="exampassedstate" name = "exampassedstate" >
                                                        <option value="" selected>Please Select</option>
                                                        @foreach ($populatestate as $populatest)
                                                        <option value="{{ $populatest->state }}">{{ $populatest->state }}</option>
                                                        @endforeach

                                                    </select>





                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="marksinpercentagecgpi">Marks in Percentage/CGPI<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required=""  tabindex="33" id="marksinpercentagecgpi" name = "marksinpercentagecgpi" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="Percentage">Percentage</option>
                                                        <option value="CGPI">CGPI</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="markscgpi">Marks% or CGPI<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required" required="" tabindex="34" id="markscgpi" placeholder="Enter Marks% or CGPI"  name="markscgpi" value="{{ old('markscgpi') }}">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="exampassedyear">Qualifying Exam Passed Year<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control" required tabindex="35" id="exampassedyear" name = "exampassedyear">
                                                        @foreach ($populateyear as $populate)
                                                        <option value="{{ $populate->gateyr }}">{{ $populate->gateyr }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>





                                            @endif

                                            @if(($studenttyp=="newstudent" && ($course=="MTech" || $course=="MArch")))
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="specilization">Specilization<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" tabindex="37" id="specilization" name = "specilization" >
                                                        <option value="" selected>Please Select</option>
                                                        @foreach ($populatespec as $populatesp)
                                                        <option value="{{ $populatesp->specilization }}">{{ $populatesp->specilization }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @endif


                                            @if($studenttyp=="newstudent")

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="admissioncategory">Admission in Category<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control required" required="" tabindex="38" id="admissioncategory" name = "admissioncategory" >
                                                        <option value="" selected>Please Select</option>
                                                        <option value="OP">OP</option>
                                                        <option value="OPPwD">OPPwD</option>
                                                        <option value="EWS">EWS</option>
                                                        <option value="EWSPwD">EWSPwD</option>
                                                        <option value="SC">SC</option>
                                                        <option value="SCPwD">SCPwD</option>
                                                        <option value="ST">ST</option>
                                                        <option value="STPwD">STPwD</option>
                                                        <option value="OBC">OBC</option>
                                                        <option value="OBCPwD">OBCPwD</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="qualifyexampassed">Exam Passed</label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control" required="" tabindex="39" id="qualifyexampassed" name = "qualifyexampassed">
                                                        <option value="" selected>Please Select</option>
                                                        <option value="10+2">10+2</option>
                                                        <option value="diploma">Diploma</option>
                                                        <option value="other">Other</option>
                                                    </select>
                                                </div>
                                            </div>  

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="lastinstattend">Last Institution/School <span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control required" required="" tabindex="40" id="lastinstattend" placeholder="Last Institution/School"  name="lastinstattend" value="{{ old('lastinstattend') }}">
                                                </div>
                                            </div>
                                            @endif
                                            @if(($studenttyp=="newstudent" && ($course=="MTech" || $course=="MArch")))
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="gateregid">Gate Registration ID<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control" required="" tabindex="41" id="gateregid" placeholder="Gate Registration ID"  name="gateregid" value="{{ old('gateregid') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="gatescore">Gate Score<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control" required="" tabindex="42" id="gatescore" placeholder="Gate Score"  name="gatescore" value="{{ old('gatescore') }}">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="gateregyear">Gate Registration Year<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control" required="" tabindex="43" id="gateregyear" placeholder="Gate Registration Year"  name="gateregyear" value="{{ old('gateregyear') }}">
                                                </div>
                                            </div>   

                                            <div class="form-group">
                                                <label class="control-label col-sm-12"  for="gatepapercode">Gate Paper Code</label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control" required="" tabindex="44" id="gatepapercode" placeholder="Gate Paper Year"  name="gatepapercode" value="{{ old('gatepapercode') }}">
                                                </div>
                                            </div> 
                                            @endif
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="adharno">Adhaar No</label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control digit" maxlength="12" tabindex="46" id="adharno" placeholder="12 Digit Adhaar No"  name="adharno" value="{{ old('adharno') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="nadid">NAD ID [<a href="https://nad.gov.in" style="color:teal" target= "_new">Know About NAD</a>]  [<a style="color:red" target= "_new" href="https://cvl.nad.co.in/NAD/uidStudentReg.action?activePage=regactuidStudentReg">Get NAD ID</a>]</font></label>
                                                <div class="col-sm-12">  
                                                    <input type="text" class="form-control" maxlength="20" tabindex ="47" id="nadid" placeholder="NAD ID"  name="nadid" value="{{ old('nadid') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="gatepapercode">Upload Your Photograph<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <img class="kycimg photoImage" id="photoImage" style="width:120px;">
                                                    <input type="file" class="kycphoto required" tabindex="45" name="photograph" accept="image/x-png,image/jpg,image/jpeg" onchange="readURLPhoto(this);" value="{{ old('photograph') }}">

                                                </div>
                                            </div> 
                                        </div>
                                    </div>   

                                    <br>
                                    <div class="row">

                                        <div class="col-sm-5"></div>
                                        <div class="col-sm-7">
                                            <div style="overflow:auto;"> 
                                                <div style="margin: 0 auto;"> 
                                                    <button type="button" class="btn btn-flickr regNext" id="nextBtn">Submit & Preview</button>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>


                                <!--show data-->



                                <div class="row datashow">   
                                    <div class="col-sm-3">  

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="admissionin">Admission IN</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_admissionin"  value="" readonly >
                                            </div>
                                        </div>
                                        @if($studenttyp=="oldstudent")
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="instroll">Institute RollNumber</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_instroll"  value="" readonly >
                                            </div>
                                        </div>
                                        @else
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="admissionthrough">Admission Through</label>
                                            <div class="col-sm-12">  
                                                @if($course=="BTech" || $course=="DUAL" || $course=="BArch")    
                                                <input type="text"  class="form-control" id="show_course" readonly="" value="" >
                                                @endif

                                                @if($course=="MTech" || $course=="MArch")    
                                                <input type="text"  class="form-control" id="show_course" readonly="" value=""  >
                                                @endif
                                            </div>
                                        </div>
                                        @endif                                       

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="studentname">Student Name </label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_studentname"  value="true" readonly >
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="studentnamehindi">Student  Name in Hindi </label>

                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control" id="show_studentnamehindi" value="true" readonly >

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="fathername">Father  Name</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control"id="show_fathername" readonly="true" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="fathernamehindi">Father Name in Hindi</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control" id="show_fathernamehindi" readonly="true" >
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="mothername">Mother  Name</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control" readonly="true" id="show_mothername"  >   
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="mothernamehindi">Mother Name in Hindi</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control"  id="show_mothernamehindi" readonly="" >
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="dob">Date of Birth</label>  
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"readonly="true" id="show_datepicker" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="dob">Gender</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"readonly="true" id="show_gender" >

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="category">Category</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"readonly="true" id="show_category" >

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-sm-3"> 
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="religion">Religion</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"readonly="true" id="show_religion">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="PWD">Weather Physically Challanged</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" readonly="true" id="show_pwd">

                                            </div>
                                        </div>     

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="contactnumberstudent">Contact Number Student</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control" readonly="" value="" id="show_contactnumberstudent"   >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="contactnumberfather">Contact Number Father</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control"readonly="" id="show_contactnumberfather"  >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="contactnumbermother">Contact Number Mother</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control"  readonly="" id="show_contactnumbermother">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="email">Student Email ID</label>
                                            <div class="col-sm-12">
                                                <input  type="email" class="form-control" value=""  id="show_studentemail" readonly="">
                                                <span id="msg"></span> 
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="email">Father Email:</label>
                                            <div class="col-sm-12">
                                                <input  type="text" class="form-control"  id="show_fatheremail" readonly="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="bonafiedstate">Candidate Bonafied State</label>
                                            <div class="col-sm-12">   
                                                <input  type="text" class="form-control"  id="show_bonafiedstate" readonly="" >

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="ruralurban">Candidate From</label>
                                            <div class="col-sm-12"> 
                                                <input  type="text" class="form-control"  id="show_ruralurban" readonly="">

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="guardianname">Guardian Name (enter Father Name if no Guardian)</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control" id="show_guardianname" readonly="" >
                                            </div>
                                        </div>




                                    </div>
                                    <div class="col-sm-3"> 
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="contactnumberguardian">Contact Number Guardian</label>
                                            <div class="col-sm-12">          
                                                <input type="text" class="form-control" id="show_contactnumberguardian" readonly=""  >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="coaddress">Correspondence Address</label>
                                            <div class="col-sm-12">  
                                                <input type="textarea" class="form-control" id="show_coaddress" readonly=""  >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="peraddress">Permanent Address</label>
                                            <div class="col-sm-12">  
                                                <input type="textarea" class="form-control"  id="show_peraddress" readonly="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="railwaystation">Nearest Railway Station Near Home Town</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_railwaystation" readonly="">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="feewaiver">Fee Waiver</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_feewaiver" readonly="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="annualincome">Annual Family Income</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_annualincome" readonly="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="hostelrequired">Hostel Required</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_hostelrequired" readonly="" >

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="admissionbranch">Admitted in Branch</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_admissionbranch" readonly="">

                                            </div> 
                                        </div>
                                        @if($studenttyp=="oldstudent")

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="admissionbranch">Select Semester<span class="requiredfield">*</span></label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_semseternum" readonly="">
                                            </div> 
                                        </div>
                                        <div class="show_sgpi"></div> 
                                        <div class="dual_AndArch"></div> 
                                        @endif
                                        @if(($course=="BTech" || $course=="DUAL" || $course=="BArch") && ($studenttyp=="newstudent") )
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="jeemainrollnumber">JEE Main Application/(Ref No in case of DASA)</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_jeemainrollnumber" readonly="" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="jeemainrankAIR">JEE Main CRL Rank</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_jeemainrankAIR" readonly="" >
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="jeemainrankcategory">JEE Main Category Rank </label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"readonly="" id="show_jeemainrankcategory" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="jeepercentile">JEE Main Percentile </label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"readonly="" id="show_jeepercentile" >
                                            </div>
                                        </div>

                                        @endif
                                        @if($studenttyp=="newstudent")
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="exampassedcountry">Examination Passed Country</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"readonly="" id="show_exampassedcountry" >

                                            </div>
                                        </div>


                                    </div>
                                    <div class="col-sm-3"> 

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="plustwoboard">+2 Board</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_plustwoboard" readonly=""  >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="exampassedstate">Seat Allotment Quota</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_seetallotment" readonly=""  >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="exampassedstate">State of Eligibility</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_exampassedstate" readonly=""  >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="marksinpercentagecgpi">Marks in Percentage/CGPI</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_marksinpercentagecgpi" readonly="" >

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="markscgpi">Marks% or CGPI</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_markscgpi" readonly="" >
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="exampassedyear">Qualifying Exam Passed Year</label>
                                            <div class="col-sm-12">
                                                <input type="text" class="form-control"  id="show_exampassedyear" readonly="" >

                                            </div>
                                        </div>





                                        @endif
                                        @if(($studenttyp=="newstudent") && (($course=="MTech" || $course=="MArch")))
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="specilization">Specilization</label>
                                            <div class="col-sm-12"> 
                                                <input type="text" class="form-control"  id="show_specilization" readonly="" >

                                            </div>
                                        </div>
                                        @endif

                                        @if($studenttyp=="newstudent")

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="admissioncategory">Admitted in Category</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_admissioncategory" readonly="" >

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="qualifyexampassed">Exam Passed</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_qualifyexampassed" readonly="" >

                                            </div>
                                        </div>  

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="lastinstattend">Last Institution/School</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_lastinstattend" readonly="" >
                                            </div>
                                        </div>
                                        @endif
                                        @if(($studenttyp=="newstudent") && (($course=="MTech" || $course=="MArch" )))
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="gateregid">Gate Registration ID</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_gateregid" readonly="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="gatescore">Gate Score</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"id="show_gatescore" readonly="" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="gateregyear">Gate Registration Year</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_gateregyear" readonly="" >
                                            </div>
                                        </div>   

                                        <div class="form-group">
                                            <label class="control-label col-sm-12"  for="gatepapercode">Gate Paper Code</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control"  id="show_gatepapercode" readonly="" >
                                            </div>
                                        </div> 
                                        @endif
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="adharno">Adhaar No</label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_adharno"readonly="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="nadid">NAD ID </label>
                                            <div class="col-sm-12">  
                                                <input type="text" class="form-control" id="show_nadid"readonly="" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-12" for="gatepapercode">Your Photograph</label>
                                            <div class="col-sm-12">  
                                                <img class="kycimg photoImage" id="photoImage" style="width:120px;">


                                            </div> 
                                        </div> 
                                    </div>


                                    <br>


                                    <div class="col-sm-5">
                                        <input style="display: inline-block;float: right;margin-right:-160px !important;margin-top: 13px;" type="checkbox" required name="terms"> 
                                    </div>
                                    <div class="col-sm-7">
                                        <p id="sucessmeg"></p>
                                        <p id="errormeg"></p>
                                        <div style="overflow:auto;">
                                            <div style="margin: 0 auto;"> 
                                                <p> I agreed to  all <u>Terms and Conditions</u> laid down by NIT Hamirpur.</p>
                                                <button type="button" class="btn btn-flickr regEdit" id="nextBtn">Edit</button>
                                                <button type="submit"  class="btn btn-flickr">Final Submit</button>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                @include('student.formerrors')
                            </form> 

                        </div>





                    </div>




                </div>              
            </div>

            <!--end::Form-->
        </div>
        <!--end::Form-->
    </div>

    <!--End::Section-->
</div>




</div>

</div>
<!-- end:: Body -->

</div>



@endsection
