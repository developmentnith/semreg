<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

        <title>pdf</title>
        <style>
            * { margin: 0; padding: 0; 

            }   
            body {
                font: 13px/1.4 Helvetica, Arial, sans-serif;   

            }

            /*            #page-wrap {margin: 10px auto !important; width: 800px;border: 1px solid #ccc;padding: 20px;}*/
        </style>
    </head>

    <body>


        <div id="page-wrap" style="margin-left: 20px; margin-right: 20px;"> 
            <p style="text-align: center;font-size: 25px; font-weight: bold;">NATIONAL INSTITUTE OF TECHNOLOGY, HAMIRPUR (H.P.)</p>
            <p style="float: right;font-size: 12px;">Registration No..............................</p>
            <table width="100%">   
                <tr>
                    <td style="border: 0;  text-align: left" width="20%">

                    </td>
                    <td style="border: 0;  text-align: left;" width="60%">    
                        <p style="text-align:center;font-size: 25px; font-weight: bold;">REGISTRATION CARD</p>
                        <p style="text-align:center;font-size: 16px;">(Session.......................)</p>                   
                        <p style="text-align:center;font-size: 20px;">Part -1</p>
                        <p style="text-align:center;font-size: 25px;">Roll. No. {{$studentbasic[0]->InstituteRollNumber}}</p>
                    </td>
                    <td style="border: 0;  text-align: left" width="20%"> 
                        <p style="height: 140px;border: 1px solid;width: 132px;margin-bottom: 15px;margin-top: 12px;float: right;">
                            <img style="width: 132px;" src="upload/studentphoto/{{$studentbasic[0]->StudentPhoto}}"></img>
                        </p>
                    </td>
                </tr>

            </table>
            <table width="100%">   
                <tr>

                    <td style="border: 0;  text-align: left;" width="100%">    
                        <p>Name (In English)..................{{$studentbasic[0]->StudentName}}..................(In Hindi).................{{$studentbasic[0]->StudentNameHindi}}......................</p>
                        <p>(as per Matriculation Certificate)</p>                    
                        <p>Father's Name............{{$studentbasic[0]->FatherName}}................... Class: B.Tech/B.Arch./M.Tech. .............{{$studentbasic[0]->AdmissionIN}}.............................</p>
                        <p>Branch ...............{{$studentbasic[0]->AdmissionBranch}}.................Semester. .....................Hostler/Day Scholar..........................................</p>
                    </td>

                </tr>

            </table>


            <table border="1" style="color:initial;font-size:10px;  border-collapse: collapse; margin: 0 auto;"   width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="background-color: #ccc; color: black;">Address for Correspondence (in CAPITAL LETTERS)</th>
                        <th style="background-color: #ccc; color: black;">Permanent Home Address (In CAPITAL LETTERS)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$studentbasic[0]->CoAddress}}</td>
                        <td>{{$studentbasic[0]->PermanentAddress}}</td>
                    </tr>                                     
                    <tr>
                        <td>Pin Code.......................   <b>Mobile No.</b> {{$studentbasic[0]->ContactNumberStudent}} Email {{$studentbasic[0]->EmailIDStudent}} </td>
                        <td>Pin Code..........................    <b>Mobile No.</b> {{$studentbasic[0]->ContactNumberStudent}} Email {{$studentbasic[0]->EmailIDStudent}} </td>                      
                    </tr>


                </tbody>
            </table>                        
            <p style="text-align:center;font-size: 16px;">Results of Pervious Semester(S)</p>
            <table border="1" style="color:initial;font-size:10px;  border-collapse: collapse; margin: 0 auto;"   width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="background-color: #ccc; color: black;">Semester</th>
                        <th style="background-color: #ccc; color: black;">SGPI</th>
                        <th style="background-color: #ccc; color: black;">CGPI</th>
                        <th style="background-color: #ccc; color: black;">Repeat (if any)</th>
                    </tr>
                </thead>
                <tbody>
                    @php $j = 1 ;
                    $sgpi = explode(',', $studentsem[0]->SGPI);
                    $cgpi = explode(',', $studentsem[0]->CGPI); 
                    $totalRow = count($sgpi);             
                    @endphp                  
                    @for($i = 0; $i < $totalRow; $i++ )
                    <tr>
                        <td>S0{{$j}}</td>  
                        <td>
                            {{$sgpi[$i]}}
                        </td>
                        <td>
                            {{$cgpi[$i]}}
                        </td>
                        <td></td>
                    </tr>
                    @php  $j++  @endphp
                    @endfor


                </tbody>
            </table>
            <p style="margin-top:30px;"></p>

            <p  style="text-align:right;font-size: 16px;">Signature of the Student</p>
            <p><b>Note:</b> The Students should fill this information carefully/legibly and correctly. In case any 
                discrepancy is found later on, the candidate will be held solely responsible for same.</p>

            <table border="1" style="color:initial;font-size:10px;  border-collapse: collapse; margin: 0 auto;"   width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="background-color: #ccc; color: black;">Sr.No.</th>
                        <th style="background-color: #ccc; color: black;">Course No.</th>
                        <th style="background-color: #ccc; color: black;">Title of the Course</th>
                        <th style="background-color: #ccc; color: black;">L</th>
                        <th style="background-color: #ccc; color: black;">T</th>
                        <th style="background-color: #ccc; color: black;">P</th>
                        <th style="background-color: #ccc; color: black;">C</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 1; @endphp
                    @forelse($csubject as $subj)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$subj->SubjectCode}}</td>
                        <td>{{$subj->SubjectName}}</td>
                        <td>{{$subj->TotalLectureTheory}}</td>
                        <td>{{$subj->TotalLabPractical}}</td>
                        <td></td> 
                        <td>{{$subj->credit}}</td>                        
                    </tr>
                    @php $i++; @endphp
                    @empty
                    <tr><td>No Subject Found Kindly Contact System Admin</td></tr>
                      
                    @endforelse 
                    
                     @forelse($openelective as $open_elective)
                    <tr>
                        @php 
                        $sub = \Helpers::getSubject($open_elective->SubjectID); 
                        @endphp
                        <td>{{$i}}</td>
                        <td>{{$open_elective->SubjectID}}</td>
                        <td>{{$open_elective->SubjectName}}</td>
                        <td>{{$sub->TotalLectureTheory}}</td>
                        <td>{{$sub->TotalLabPractical}}</td>
                        
                        <td></td> 
                        <td>{{$sub->credit}}</td>                        
                    </tr>
                     @empty
                      @endforelse 
                       @forelse($depelective as $dep_elective)
                    <tr>
                        @php 
                        $subel = \Helpers::getSubject($dep_elective->SubjectID); 
                        @endphp
                        
                        <td>{{$i}}</td>
                        <td>{{$dep_elective->SubjectID}}</td>
                        <td>{{$subel->SubjectName}}</td>
                        <td>{{$subel->TotalLectureTheory}}</td>
                        <td>{{$subel->TotalLabPractical}}</td>
                        <td></td> 
                        <td>{{$subel->credit}}</td>                        
                    </tr>
                     @empty
                      @endforelse 
                      
                </tbody>
            </table>
            <p style="margin-top:30px;"></p>
            <p  style="text-align:right;font-size: 16px;">Department Course Converner<br>NIT,Hamirpur(H.P.)</p>
            <p><b>Note:</b> The Department Course Convener should verify the course code and title of the above cources 
                there will not be any change in Department Elective/Open Elective courses once opted.</p>
            <p style="text-align:center;font-size: 20px;">Part -11</p>
            <p style="text-align:center;font-size: 18px;">FOR OFFICE USE</p>
            <p style="text-align:center;font-size: 16px;">[No Dues Certificate (Application for old students only)]</p>
            <table border="1" style="color:initial;font-size:10px;  border-collapse: collapse; margin: 0 auto;"   width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th style="font-size:16px">1. Dy Registrar (Accounts) or his nominee
                            <p style="height:100px"></p>

                        </th>
                        <th style="font-size:16px">2. Hostel Warden or his nominee
                            <p style="height:100px"></p>
                        </th>

                    </tr>
                    <tr>
                        <th style="font-size:16px">3. Dy Registrar (Accounts) or his nominee
                            <p style="height:100px"></p>

                        </th>
                        <th style="font-size:16px">4. Hostel Warden or his nominee
                            <p style="height:100px"></p>
                        </th>

                    </tr>
                </thead>

            </table>

            <div style="clear:both"></div> 


        </div>

    </body>

</html>