@extends('layouts.student_app')

@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Register for  Department Elective</h3>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--tabs">
            <!--begin::Form-->
            <div class="m-portlet">
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" action="{{ url('student/depreg-add') }}" method="post" >
                    @csrf
                    <div class="m-portlet__body">   
                        <div class="form-group m-form__group row">
                            <div class="row">
                                <div class="col-md-6">
                                    @php  $_result = array();  @endphp 
                                    @foreach($result as $key => $value)
                                    @php
                                    $_result = Illuminate\Support\Facades\DB::table('DeptElectiveConfig')
                                    ->select("Pool", 'SubjectCode', 'SubjectName','Semester')
                                    ->where('Branch', '=', $branch)
                                    ->Where('FloatYear', '=', $yr)
                                    ->Where('Pool', '=', $value->Pool)
                                    ->Where('SemNum', '=', 5)
                                    ->Where('Degree', '=', $course)->get(); 
                                    @endphp
                                    <label class="control-label col-sm-12" for="admissionbranch">Pool- {{$value->Pool}}<span class="requiredfield">*</span></label>
                                    <input type ="hidden" name="pool_{{$value->Pool}}" value="{{$value->Pool}}">
                                    <select class="form-control" name = "sub_{{$value->Pool}}" required="">
                                        <option value="">Please Select</option>  
                                        @foreach ($_result as $p) {                                              
                                        <option value="{{$p->SubjectCode}}">{{$p->SubjectName}}</option>
                                        @endforeach  
                                    </select>      
                                    <input type="hidden"name="Semester" value="{{$p->Semester}}">
                                    @endforeach  
                                </div>
                                <div class="col-md-6">

                                </div>
                                <div class="col-sm-12">  
                                    <br>
                                    <button type="submit" class="btn btn-flickr">Submit</button>                                           
                                </div>
                            </div>

                        </div>              
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Form-->
        </div>

        <!--Begin::Section-->


        <!--End::Section-->
    </div>




</div>

</div>
<!-- end:: Body -->

</div>



@endsection
