@extends('layouts.student_app')

@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Samester Register</h3>
            </div>

        </div>
    </div>
    <!-- END: Subheader -->

    <div class="m-content">

        <div class="m-portlet m-portlet--tabs">

            <!--begin::Form-->
            <div class="m-portlet">

                <!--begin::Form-->
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    <div class="m-portlet__body">   
                        <div class="form-group m-form__group row">
                            <div class="row" ng-app="myApp" >
                                <div ng-controller="myAppController">
                                    <div class="col-sm-6">
                                        <form  action="{{ url('student/semreg-add')}}" method="post" >
                                            @csrf
                                            <div class="form-group">
                                                <label class="control-label col-sm-12" for="admissionbranch">Select Sem<span class="requiredfield">*</span></label>
                                                <div class="col-sm-12">  
                                                    <select class="form-control semRegister" id="semRegister"  tabindex="36"  name = "semester_name" required="">                                              
                                                        <option value="" selected>Please Select</option>
                                                        @foreach ($semesterdata as $sem)
                                                        @if($sem->degree == $course)
                                                        <option data-bind="{{ $sem->fullName}}" value="{{ $sem->semNum}}">{{ $sem->fullName}}</option>
                                                        @endif 
                                                        @endforeach                                               
                                                    </select>
                                                </div>
                                                @php $j = $now + 1;     
                                               
                                                @endphp 
                                                @for ($i = $now; $i < $last; $i++)
                                                <div class="col-sm-12">  
                                                    <label class="control-label col-sm-12" for="admissionbranch">SGPI Semester {{$j}}  {{$msgug}}<span class="requiredfield">*</span></label>
                                                    <input type ="text" name="sgpi" onchange="semSgpi($(this).val(), {{$j}});" class="form-control" required="">
                                                    <input type ="hidden" name="semester_num" value="{{$j}}">
                                                </div> 
                                                <div class="col-sm-12">  
                                                    <label class="control-label col-sm-12" for="admissionbranch">CGPI Semester {{$j}} {{$msgug}}<span class="requiredfield">*</span></label>
                                                    <input type ="text" name="cgpi" onchange="semCgpi($(this).val(), {{$j}});"  class="form-control" required="">
                                                </div> 
                                               @if($course == 'DUAL')
                                                @if($j == 7 || $j == 8)
                                                <div class="col-sm-12">  
                                                    <label class="control-label col-sm-12" for="admissionbranch">SGPI Semester {{$j}}  {{$msgpg}}<span class="requiredfield">*</span></label>
                                                    <input type ="text" name="pgsgpi" onchange="pgsemSgpi($(this).val(), {{$j}});" class="form-control" required="">                                                  
                                                </div> 
                                                <div class="col-sm-12">  
                                                    <label class="control-label col-sm-12" for="admissionbranch">CGPI Semester {{$j}} {{$msgpg}}<span class="requiredfield">*</span></label>
                                                    <input type ="text" name="pgcgpi" onchange="pgsemCgpi($(this).val(), {{$j}});"  class="form-control" required="">
                                                </div> 
                                                @endif
                                                @endif
                                                @php $j++; @endphp
                                                @endfor
                                               
                                            </div>
                                            <div class="col-sm-12">  
                                                <br>
                                                <button type="submit" class="btn btn-flickr">Submit</button>                                           
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-sm-6">
                                        <br>
                                        <table width='100%' class="table table-striped">
                                            <tr>
                                                <th>Semester Name:</th>
                                                <td class="semName">-</td>
                                            </tr>
                                            @php $j = $now + 1; 
                                             
                                            @endphp
                                            @for ($i = $now; $i < $last; $i++)
                                            <tr>
                                                <th>SGPI Semester {{$j}} {{$msgug}}</th>
                                                <td class="sgpi_{{$j}}">-</td>
                                            </tr>
                                            <tr>
                                                <th>CGPI Semester {{$j}} {{$msgug}}</th>
                                                <td class="cgpi_{{$j}}">-</td>
                                            </tr>
                                             @if($course == 'DUAL')
                                             @if($j == 7 || $j == 8)
                                              <tr>
                                                <th>SGPI Semester {{$j}} {{$msgpg}}</th>
                                                <td class="pgsgpi_{{$j}}">-</td>
                                            </tr>
                                            <tr>
                                                <th>CGPI Semester {{$j}} {{$msgpg}}</th>
                                                <td class="pgcgpi_{{$j}}">-</td>
                                            </tr>
                                             @endif
                                             @endif
                                            @php $j++; @endphp
                                            @endfor
                                        </table>



                                    </div>
                                </div>

                            </div>              
                        </div>              
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Form-->
        </div>

        <!--Begin::Section-->


        <!--End::Section-->
    </div>




</div>

</div>
<!-- end:: Body -->

</div>




@endsection
