@extends('layouts.student_app')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <!--begin::Total Profit-->
                        <div class="m-widget24">                     
                            <div class="m-widget24__item">

<!--                                <select class="form-control downloadPDF"  name="" id="downloadPDF">
                                    <option value="" selected>Download PDF By Semester</option>     
                                    @php $i = 1; @endphp
                                    @foreach ($semesterdata as $sem)
                                    @if($sem->degree == $course )
                                    @if($active_status == 'Y')
                                    @if($approved_sem == $sem->semNum)
                                    <option data-bind="{{ $sem->fullName}}" value="{{ $sem->semNum}}">{{ $sem->fullName}}</option>
                                    @endif 
                                    @endif 
                                    @endif 
                                    @php $i++ @endphp
                                    @endforeach   
                                </select>-->

                            </div>                    
                        </div>
                        <!--end::Total Profit-->


                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-6">
                        <!--begin::New Feedbacks-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Semester</th>
                                            <th>No Due Detail</th>
                                            <th>By Department</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($nodues as $ndu)
                                        <tr>
                                            <td>{{$ndu->Semester}}</td>
                                            <td>{{$ndu->NoDueDetail}}</td>
                                            <td>{{$ndu->NoDueByDept}}</td>
                                            <td>{{$ndu->NoDueRemark}}</td>
                                        </tr>
                                        @empty
                                        <tr><td colspan="4" align="center">No Nodues listed for you</td>
                                            @endforelse
                                    </tbody>
                                </table>


                            </div>      
                        </div>
                        <!--end::New Feedbacks--> 
                    </div>


                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <h3 class="m-portlet__head-text" style="padding: 5px;">
                            Your Registraion
                        </h3>
                        <table class="table table-hover">
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">Roll Number</th>
                                    <th scope="col">Student Name</th>
                                    <th scope="col">Email ID</th>
                                    <th scope="col">Branch</th>
                                    <th scope="col">Verified</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$userrolln}}</td>
                                    <td>{{$username}}</td>
                                    <td>{{$useremail}}</td>  
                                    <td>{{$userbranch}}</td>
                                    <td>{{$verifiedstatus}}</td>
                                    <td><span class="m-badge  m-badge--success m-badge--wide">Active</span></td>
                                    <td>
                                        <a class="btn btn-primary btn-xs" onclick="showStudentAjaxModal('{{ url('student/edit-modal-pop/edit_student/'.$studentbasicId.'') }}');" style="cursor: pointer;"> <i style="padding: 10px 5px; color: #fff;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
<!--                    <div class="col-md-12 col-lg-12 col-xl-6">
                        <h3 class="m-portlet__head-text" style="padding: 5px;">
                            SGPI/CGPI Uploaded By Admin
                        </h3>
                        @if($userdeg==='DUAL')

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Semester#</th>
                                    <th>SGPI</th>
                                    <th>CGPI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sgpicgpiadm as $row)
                                <tr>
                                    @if($row->UGPGTag=='UG')
                                    <td>{{$row->Semester}}</td>
                                    <td>{{$row->SGPI}}</td>
                                    <td>{{$row->CGPI}}</td>
                                    @endif    
                                    @if($row->UGPGTag=='PG')
                                    <td>{{$row->Semester}}</td>
                                    <td>{{$row->SGPI}}</td>
                                    <td>{{$row->CGPI}}</td>
                                    @endif    
                                </tr>

                                @empty
                                <tr><td colspan="4" align="center">No SGPI/CGPI uploaded by admin</td></tr>
                                @endforelse
                            </tbody>
                        </table>
                        @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Semester</th>
                                    <th>SGPI</th>
                                    <th>CGPI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sgpicgpiadm as $row)
                                <tr>

                                    <td>{{$row->Semester}}</td>
                                    <td>{{$row->SGPI}}</td>
                                    <td>{{$row->CGPI}}</td>
                                </tr>
                                @empty
                                <tr><td colspan="4" align="center">No SGPI/CGPI uploaded by admin</td></tr>
                                @endforelse
                            </tbody>
                        </table>
                        @endif
                    </div>-->

                    <div class="col-md-12 col-lg-12 col-xl-6">
                        <h3 class="m-portlet__head-text" style="padding: 5px;">
                            SGPI/CGPI Updated By Student
                        </h3>
                        @if($userdeg==='DUAL')
                        <table class="table">
                            <thead>
                                <tr>
                                    <th >Semester</th>
                                    <th>SGPI(UG)</th>
                                    <th>CGPI(UG)</th>
                                    <th>SGPI(PG)</th>
                                    <th>CGPI(PG)</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sgpicgpistu as $strow) 
                                @php
                                $sem = explode(',',$strow->Semester);
                                $sgpi = explode(',',$strow->SGPI);
                                $cgpi = explode(',',$strow->CGPI);
                                $pg_sgpi = explode(',',$strow->PGSGPI);
                                $pg_cgpi = explode(',',$strow->PGCGPI);

                                @endphp
                                @for ($i = 0; $i < count($sem); $i++)
                                <tr>

                                    @if($sem[$i]<7)
                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                    <td></td>
                                    <td></td>
                                    @endif

                                    @if($sem[$i]==7)
                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                    <td>{{$pg_sgpi[0]}}</td>
                                    <td>{{$pg_cgpi[0]}}</td>
                                    @endif                                   
                                    @if($sem[$i]==8)
                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                    <td>{{$pg_sgpi[1]}}</td>
                                    <td>{{$pg_cgpi[1]}}</td>
                                    @endif  
                                    @if($sem[$i]==9)
                                    <td>{{$sem[$i]}}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$pg_sgpi[3]}}</td>
                                    <td>{{$pg_cgpi[3]}}</td>
                                    @endif  
                                    @if($sem[$i]==10)
                                    <td>{{$sem[$i]}}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{$pg_sgpi[4]}}</td>
                                    <td>{{$pg_cgpi[4]}}</td>
                                    @endif  

                                </tr>
                                @endfor
                                @empty
                                <tr>
                                    <td colspan="4" align="center">No SGPI/CGPI uploaded by Student</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        @else
                        <table class="table">
                            <thead>
                                <tr>
                                    <th >Semester</th>
                                    <th>SGPI</th>
                                    <th>CGPI</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($sgpicgpistu as $strow) 

                                @php

                                $sem = explode(',',$strow->Semester);

                                $sgpi = explode(',',$strow->SGPI);
                                $cgpi = explode(',',$strow->CGPI);
                                @endphp
                                @for ($i = 0; $i < count($sem); $i++)
                                <tr>

                                    <td>{{$sem[$i]}}</td>
                                    <td>{{$sgpi[$i]}}</td>
                                    <td>{{$cgpi[$i]}}</td>
                                </tr>
                                @endfor
                                @empty
                                <tr>
                                    <td colspan="4" align="center">No SGPI/CGPI uploaded by Student</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                        @endif
                    </div>


                </div>
            </div>
        </div>

    </div>
</div>
</div>


@endsection
