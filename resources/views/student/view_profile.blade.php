@extends('layouts.student_app')

@section('content')


<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">My Profile</h3>
            </div>

        </div>

    </div>
    <!-- END: Subheader -->

    <div class="m-content">

        <div class="m-portlet m-portlet--tabs">



            <!--begin::Form-->
            <div class="m-portlet">

                <!--begin::Form-->

                <div class="m-portlet__body">   
                    <div class="form-group m-form__group row">
                        <div class="row">   
                            <div class="col-sm-12">  
                                <form action="{{ url('student/save-profile') }}" method="post" enctype="multipart/form-data">
                                    @csrf 
                                    <!-- text input -->
                                    <div class="form-group">
                                        <label>Name:</label>
                                        <input type="text" name="name" value="<?= $user_info[0]->name ?>" class="form-control" >
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" name="email"  value="<?= $user_info[0]->email ?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Change Password</label>
                                        <input type="password" name="password"  value="" class="form-control">
                                    </div>                                   
                                    <div class="form-group">                                
                                        <input type="submit" name="submit"  value="Submit" class="form-control">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>




                </div>              
            </div>

            <!--end::Form-->
        </div>
        <!--end::Form-->
    </div>

    <!--End::Section-->
</div>




</div>

</div>
<!-- end:: Body -->

</div>



@endsection
