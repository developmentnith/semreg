@extends('layouts.student_app')

@section('content')

<style>

    .container{overflow: hidden}
    .tab{float: left}
    .tab-btn{margin:80px;}
    button{display:block;margin-bottom: 20px;}
    tr{transition:all .25s ease-in-out}
    tr:hover{background-color: #ddd;}
    
.option-input {
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  -o-appearance: none;
  appearance: none;
  position: relative;
  top: 0px;
  right: 0;
  bottom: 0;
  left: 12px;
 height: 20px;
width: 20px;
  transition: all 0.15s ease-out 0s;
  background: #cbd1d8;
  border: none;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  margin-right: 0.5rem;
  outline: none;
  position: relative;
  /*z-index: 1000;*/
}
.option-input:hover {
  background: #9faab7;
}
.option-input:checked {
  background: #0a7909;
}
.option-input:checked::before {
  height: 20px;
  width: 20px;
  position: absolute;
  content: '✔';
  display: inline-block;
  text-align: center;
  line-height: 20px;
}
.option-input:checked::after {
  -webkit-animation: click-wave 0.65s;
  -moz-animation: click-wave 0.65s;
  animation: click-wave 0.65s;
  background: #40e0d0;
  content: '';
  display: block;
  position: relative;
  z-index: 100;
}
.option-input.radio {
  border-radius: 50%;
}
.option-input.radio::after {
  border-radius: 50%;
}

.option-input1 {
  -webkit-appearance: none;
  -moz-appearance: none;
  -ms-appearance: none;
  -o-appearance: none;
  appearance: none;
  position: relative;
  top: 0px;
  right: 0;
  bottom: 0;
  left: 12px;
 height: 20px;
width: 20px;
  transition: all 0.15s ease-out 0s;
  background: #cbd1d8;
  border: none;
  color: #fff;
  cursor: pointer;
  display: inline-block;
  margin-right: 0.5rem;
  outline: none;
  position: relative;
  /*z-index: 1000;*/
}
.option-input1:hover {
  background: #9faab7;
}
.option-input1:checked {
  background: red;
}
.option-input1:checked::before {
  height: 20px;
  width: 20px;
  position: absolute;
  content: '✔';
  display: inline-block;
  text-align: center;
  line-height: 20px;
}
.option-input1:checked::after {
  -webkit-animation: click-wave 0.65s;
  -moz-animation: click-wave 0.65s;
  animation: click-wave 0.65s;
  background: #40e0d0;
  content: '';
  display: block;
  position: relative;
  z-index: 100;
}
.option-input1.radio {
  border-radius: 50%;
}
.option-input1.radio::after {
  border-radius: 50%;
}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {	
	font-size: 12px !important;
}
.tab1 {
	background-color: #0a7909;
	color: white;
	border: none;
	padding: 1px 6px;
}
.tab2 {
	background-color: red;
	color: white;
	border: none;
	padding: 1px 6px;
}
</style>   
<div class="m-grid__item m-grid__item--fluid m-wrapper"> 
    
@php
die('Open Elective Subject Choice Filling Portal is Closed');
@endphp

<!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Register for  Open Elective</h3>
                        @if($ChoiceExist != 'True')
                                <p style="color: red;font-size: 22px;font-weight: 500;">Add one subject at a time in the order of priority</p>
                                @endif
            </div>

        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet m-portlet--tabs">
            <!--begin::Form-->
            <div class="m-portlet">
                <!--begin::Form-->
                <div class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">

                    <div class="m-portlet__body">   
                        <div class="form-group m-form__group row"> 
                     
                            <div class="row">
                                <div class="cointainer">
                                    @if($ChoiceExist == 'True')
                                    <div class="col-sm-12"> <span><h2>Choices Filled By </h2><span>
                                        <span>Roll Number:{{$Choices[0]->RollNumber}}    Semester: {{$Choices[0]->Semester}}  Year:{{$Choices[0]->year}}</span>
                                        <table class="table">
                                           <tr>
                                                <th>Priority</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Dept</th>
                                            </tr>
                                            <tbody>
                                                @foreach($Choices as $row)
                                                <tr>
                                                    <td>{{$row->ChoiceNum}}</td>
                                                    <td>{{$row->SubjectName}}</td>
                                                    <td>{{$row->SubjectID}}</td>
                                                    <td>{{$row->Department}}</td>                                                                                                                                                          
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @else


                                    <div class="col-sm-5">  
                                        <table id="table1" border="1" class="table">
                                            <tr>
                                                <th width="20%">Name</th>
                                                <th>Subject</th>
                                                <th>Dept</th>
                                                <th>Sem</th>
                                                <th>Deg</th>
                                                <th>Year</th>
                                                <th>Select</th>
                                            </tr>
                                            @php 
                                            $i = 1 
                                            
                                            @endphp                                          
                                            @foreach($subject as $sub)
                                            @php 
                                           
                                            $total_number[] = $i @endphp
                                            <input type="hidden" value="{{$totalRow}}" id="totalRowInTable">
                                            <tr>
                                                <td>{{$sub->SubjectName}} </td>  
                                                <td>{{$sub->SubjectID}}</td>
                                                <td>{{$sub->FloatedByDept}}</td>
                                                <td><input type ="hidden" value="{{$rollno}}" name="student_roll"><input type="hidden" class="totalchoices" name="totalcount" value="">
                                                    <input type="hidden" name="semester" value="{{$sub->Semester}}"> {{$sub->Semester}}</td> 
                                                <td><input type ="hidden" value="{{$sub->Degree}}" name="degree">{{$sub->Degree}}</td> 
                                                <td><input type ="hidden" value="{{$sub->FloatYear}}" name="year">{{$sub->FloatYear}}</td>
                                                <td><input type="radio" class="option-input radio" name="check-tab1"></td>
                                            </tr>
                                            @php $i++ @endphp 
                                            @endforeach  

                                        </table>
                                    </div>
                                    <div class="col-sm-2">  
                                        <div class="tab tab-btn">
                                            <button class="tab1" type="button" onclick="tab1_To_tab2();">>>></button>
                                            <button class="tab2" type="button" onclick="tab2_To_tab1();"><<<</button>
                                        </div>
                                    </div>

                                    <div class="col-sm-5"> 
                                        <form action="{{ url('student/open-elective-add')}}" onsubmit="return confirm('Do you want to submit your choices?');" method="post">
                                            @csrf
                                            <table id="table2" border="1" class="table">
                                                <tr>
                                                    <th>Priority</th>
                                                    <th width="20%">Name</th>
                                                    <th>Code</th>
                                                    <th>Dept</th>
                                                    <th>Sem</th>
                                                    <th>Deg</th>
                                                    <th>Year</th>
                                                    <th>Select</th>
                                                </tr>
                                            </table>
                                            <div id='submitChoice' style="display:none">
<!--                                            <p style="float: left;"><input type="checkbox" required name="terms"></p>
                                            <p>&nbsp;  I have carefully filled all my choices.</p>-->
                                            <button style="float:right;" type="submit" class="btn btn-flickr" id="btn-flickrSub">Submit</button>  
                                            </div>
                                        </form>
                                    </div>
                                    @endif

                                </div>

                            </div>              
                        </div>              
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <!--end::Form-->
        </div>

        <!--Begin::Section-->


        <!--End::Section-->
    </div>




</div>

</div>
<!-- end:: Body -->

</div>

<script>

    function arrangeChoices() {
        var table2 = document.getElementById("table2");
        var rowCount = table2.rows.length;
        var totalRaw = rowCount - 1;
        $(".totalchoices").attr('value', totalRaw);
        var nm = 1;
        for (var k = 1; k <= 20; k++)
        {
            var name = $("#" + k).attr("name");
            if ($("#" + k).length) {
                $("#" + k).attr('name', 'Choice_' + nm);
                $("#" + k).attr('id', nm);
                $("#sub_" + k).attr('name', 'subid_' + nm);
                $("#sub_" + k).attr('id', 'sub_' + nm);
                $("#dep_" + k).attr('name', 'depid_' + nm);
                $("#dep_" + k).attr('id', 'dep_' + nm);
                $("#sb_" + k).attr('name', 'sbid_' + nm);
                $("#sb_" + k).attr('id', 'sb_' + nm);

                nm++;
            }
        }
        for (var j = 1; j <= rowCount; j++) {
            $("#" + j).val('');
            $("#" + j).val(j);
            $("#" + j).attr('value', j);

        }
        var _TotalTableRow = (rowCount -1);
        var _TotalRow = $('#totalRowInTable').val();
        if(_TotalTableRow == _TotalRow){
            $('#btn-flickrSub').show();
            document.getElementById('submitChoice').style.display = "block";
            
        }else{
            $('#btn-flickrSub').hide();
            document.getElementById('submitChoice').style.display = "none";
        }  
    }



    function arrangeChoices1() {
        var table2 = document.getElementById("table2");
        var rowCount = table2.rows.length;
        
        $(".totalchoices").attr('value', '');
        $(".totalchoices").attr('value', rowCount - 1);
        var nm = 1;
        for (var k = 1; k <= rowCount; k++)
        {
            var name = $("#" + k).attr("name");
            $("#" + k).attr('name', 'Choice_' + nm);
            $("#sub_" + k).attr('name', 'subid_' + nm);
            $("#dep_" + k).attr('name', 'depid_' + nm);
            $("#sb_" + k).attr('name', 'sbid_' + nm);

            $("#" + k).val('');
            $("#" + k).val(nm);

            nm++;
        }
        var _TotalTableRow = (rowCount -1);
        var _TotalRow = $('#totalRowInTable').val();
        if(_TotalTableRow == _TotalRow){
            $('#btn-flickrSub').show();
            document.getElementById('submitChoice').style.display = "block";
        }else{
            $('#btn-flickrSub').hide();
            document.getElementById('submitChoice').style.display = "none";
        }  
        
    }








    function tab1_To_tab2()
    {
        var table1 = document.getElementById("table1"),
                table2 = document.getElementById("table2"),
                checkboxes = document.getElementsByName("check-tab1");

        console.log("Val1 = " + checkboxes.length);

        for (var i = 0; i < checkboxes.length; i++)
            if (checkboxes[i].checked)
            {

                var Value = table2.rows.length;

                var idv = table2.rows.length;
                var cls = 'choice_' + table2.rows.length;
                var subcls = 'subclas_' + table2.rows.length;
                var subid = 'sub_' + table2.rows.length;
                var subname = 'subid_' + table2.rows.length;
                var depid = 'dep_' + table2.rows.length;
                var depname = 'depid_' + table2.rows.length;
                var sbid = 'sb_' + table2.rows.length;
                var sbname = 'sbid_' + table2.rows.length;
                // create new row and cells
                var newRow = table2.insertRow(table2.length),
                        cell0 = newRow.insertCell(0),
                        cell1 = newRow.insertCell(1),
                        cell2 = newRow.insertCell(2),
                        cell3 = newRow.insertCell(3),
                        cell4 = newRow.insertCell(4),
                        cell5 = newRow.insertCell(5),
                        cell6 = newRow.insertCell(6),
                        cell7 = newRow.insertCell(7);

                var depValue = table1.rows[i + 1].cells[0].innerHTML.trim();
                var sValue = table1.rows[i + 1].cells[2].innerHTML.trim();
                // add values to the cells
                cell0.innerHTML = '<input type="hidden" id = ' + sbid + ' class=' + subcls + ' value="' + depValue + '" name=' + sbname + '>\n\
    <input type="hidden" id = ' + subid + ' class=' + subcls + ' value=' + table1.rows[i + 1].cells[1].innerHTML + ' name=' + subname + '>\n\
    <input type="hidden" id = ' + depid + ' class=' + subcls + ' value="' + sValue + '" name=' + depname + '>\n\
<input readonly="" type="text" id = ' + idv + ' class=' + cls + ' value=' + Value + ' name=' + cls + '>';
                cell1.innerHTML = table1.rows[i + 1].cells[0].innerHTML;
                cell2.innerHTML = table1.rows[i + 1].cells[1].innerHTML;
                cell3.innerHTML = table1.rows[i + 1].cells[2].innerHTML;
                cell4.innerHTML = table1.rows[i + 1].cells[3].innerHTML;
                cell5.innerHTML = table1.rows[i + 1].cells[4].innerHTML;
                cell6.innerHTML = table1.rows[i + 1].cells[5].innerHTML;
                cell7.innerHTML = "<input class='option-input1 radio' type='radio' name='check-tab2'>";

                // remove the transfered rows from the first table [table1]
                var index = table1.rows[i + 1].rowIndex;

                table1.deleteRow(index);
                // we have deleted some rows so the checkboxes.length have changed
                // so we have to decrement the value of i
                i--;

                console.log(checkboxes.length);
            }

        arrangeChoices1();
    }


    function tab2_To_tab1()
    {
        var table1 = document.getElementById("table1"),
                table2 = document.getElementById("table2"),
                checkboxes = document.getElementsByName("check-tab2");
        console.log("Val1 = " + checkboxes.length);
        for (var i = 0; i < checkboxes.length; i++)
            if (checkboxes[i].checked)
            {
                // create new row and cells
                var newRow = table1.insertRow(table1.length),
                        cell1 = newRow.insertCell(0),
                        cell2 = newRow.insertCell(1),
                        cell3 = newRow.insertCell(2),
                        cell4 = newRow.insertCell(3),
                        cell5 = newRow.insertCell(4),
                        cell6 = newRow.insertCell(5),
                        cell7 = newRow.insertCell(6);
                // add values to the cells
                cell1.innerHTML = table2.rows[i + 1].cells[1].innerHTML;
                cell2.innerHTML = table2.rows[i + 1].cells[2].innerHTML;
                cell3.innerHTML = table2.rows[i + 1].cells[3].innerHTML;
                cell4.innerHTML = table2.rows[i + 1].cells[4].innerHTML;
                cell5.innerHTML = table2.rows[i + 1].cells[5].innerHTML;
                cell6.innerHTML = table2.rows[i + 1].cells[6].innerHTML;
                cell7.innerHTML = "<input class='option-input radio' type='radio' name='check-tab1'>";

                // remove the transfered rows from the second table [table2]
                var index = table2.rows[i + 1].rowIndex;
                table2.deleteRow(index);
                // we have deleted some rows so the checkboxes.length have changed
                // so we have to decrement the value of i
                i--;
                console.log(checkboxes.length);
            }
        arrangeChoices();
    }
    
    $('#btn-flickrSub').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

</script>    

@endsection
