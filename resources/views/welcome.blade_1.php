<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/fontawesome-all.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/iofrm-style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/iofrm-theme22.css') }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    </head>
    <body>
        <div class="form-body without-side">
            <div class="website-logo">
                <a href="">
                    <div class="logo">
                        <img class="logo-size" src="images/logo.png" alt="">
                    </div>
                </a>
            </div>
            <div class="row">
                <div class="img-holder">
                    <!--<div class="bg"></div>-->  
                    <div class="info-holder">
                        <img src="images/logo.png" alt=""> 
                    </div>
                </div>    
                <div class="form-holder">
                    <div class="form-content">
                        <div class="form-items">
                            <p style="text-align: center;font-weight: 500; color:red;">Please use Mozilla Firefox or Google Chrome for better compatibility.</p>
                              <p style="text-align: center;font-weight: 500; color:red;">If you are already registered on the portal in previous Semester, then use those credentials to login. Do not try to register again.</p>
                            <h3>Login to account</h3>
                            <p>Welcome to Online Registration Portal</p>
                            <form method="POST" id="loginForm" >
                                <input id="email" placeholder="Email ID" type="email" class="form-control" name="email" value="" required autofocus>
                                <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                                <br>
                                <select class="form-control" name="login_type" required>
                                    <option value="">Select Login Type</option>  
                                    <option value="student" selected="">Student</option>
                                    <option value="faculty">Faculty</option> 
                                    <option value="admin">Admin</option>
                                    <option value="user">User</option>
                                </select>

                                <div class="form-button">
                                    <button  type="button" class="ibtn submitLogin">Login</button> <a href="{{ URL::to('/forget-password') }}">Forget password?</a>
                                </div>
                                <p id="sucessmsg"></p>
                                <p id="errormsg"></p>
                            </form>
<!--                            <div class="other-links">
                                <div class="text">Or login with</div>
                                <a href=""><i class="fab fa-facebook-f"></i>Facebook</a><a href=""><i class="fab fa-google"></i>Google</a><a href=""><i class="fab fa-linkedin-in"></i>Linkedin</a>
                            </div>-->
                            <div class="page-links">
                                <a href="{{ route('register') }}">Register new account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/popper.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script> 
        <script src="{{ asset('js/main.js') }}"></script>
        <script src="{{ asset('js/custom.js?v=').time() }}"></script> 
        <script src="{{ asset('js/cookies.js?v=').time() }}"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
            <script type="text/javascript">
                                                                        toastr.options = {
                                                                        "closeButton": false,
                                                                                "debug": false,
                                                                                "newestOnTop": false,
                                                                                "progressBar": false,
                                                                                "positionClass": "toast-bottom-left",
                                                                                "preventDuplicates": false,
                                                                                "onclick": null,
                                                                                "showDuration": "300",
                                                                                "hideDuration": "1000",
                                                                                "timeOut": "5000",
                                                                                "extendedTimeOut": "1000",
                                                                                "showEasing": "swing",
                                                                                "hideEasing": "linear",
                                                                                "showMethod": "fadeIn",
                                                                                "hideMethod": "fadeOut"
                                                                        }


            </script>
    </body>
</html>

