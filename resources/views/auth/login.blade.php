@extends('layouts.app')

@section('content')
<div class="form-body without-side">
 
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
         
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <h3 style="text-align: center;font-size: 17px;">Login to account</h3>
                    <p style="text-align: center;font-size: 13px;">Student Registration Portal NIT Hamirpur</p>
                    <p style="text-align: center;font-weight: 500; color:red;">Please use Mozilla Firefox or Google Chrome for better compatibility.</p>
                    <p style="text-align: center;font-weight: 500; color:red;">If you are already registered on the portal in previous Semester, then use those credentials to login. Do not try to register again.</p>
                    <form method="POST" id="loginForm" >
                        <div class="LoginFieldset">
                            <div class="row"> 
                                <div class="col-sm-6"> 
                                    <input id="email" placeholder="Email ID" type="email" class="form-control required" name="email" value="" required autofocus>

                                </div>
                                <div class="col-sm-6"> 
                                    <input id="password" type="password" placeholder="Password" class="form-control required" name="password" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12"> 
                                    <select class="form-control required" name="login_type" required>
                                        <option value="">Select Login Type</option>  
                                        <option value="student" selected="">Student</option>
                                        <option value="faculty">Faculty</option> 
                                        <option value="admin">Admin</option>
                                        <option value="user">User</option> 
                                    </select> 
                                </div>
                            </div>
                            <div class="row">                            
                                <div class="col-sm-12">
                                    <div class="form-button">
                                        <button  type="button" class="ibtn submitLogin">Login</button>
                                        <!--<a href="{{ URL::to('/forget-password') }}">Forget password?</a>-->
                                    </div>
                                    <p id="sucessmsg"></p>
                                    <p id="errormsg"></p>
                                </div>
                            </div>
                        </div>

                    </form>   
                    <br>
                    <div class="page-links">
                        <a href="{{ route('register') }}">Register new account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
