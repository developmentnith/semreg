@extends('layouts.app')

@section('content')
@php
die('Registration Portal for Open Elective Choice Filling is Closed');
@endphp
<div class="form-body without-side">
    <!--    <div class="website-logo">
            <a href="index.html">
                <div class="logo">
                    <img class="logo-size" src="images/logo.png" alt="">
                </div>
            </a>
        </div>-->
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">    
                <!--<img src="images/logo.png" alt="">-->
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <div id="meg"></div>
                    <h3 style="text-align: center;font-size: 17px;">Register new account</h3>
                    <p style="text-align: center;font-size: 13px;">Student Registration</p>   
                    <p style="text-align: center;font-weight: 500; color:red;">If you are already registered on the portal in previous Semester, then use those credentials to login. Do not try to register again.</p>
                    <form id="registerFrom" class="fieldset">
                        <div class="row">   
                            <div class="col-sm-6"> 
                                <input class="form-control required" type="text" name="name" placeholder="Full Name" required> 
                            </div>
                            <div class="col-sm-6"> 
                                <input class="form-control userCheckEmail required" type="email" id="emailValue" onblur="validateEmail(this);" name="email" placeholder="Email Address" required>
                                <span id="userError" style="color:red; margin-left: 17px;"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6"> 
                                <input class="form-control required" type="text" id="phoneValue" name="phone" maxlength="10" onkeypress="javascript:return isNumber(event)" placeholder="Mobile" required>
                            </div>
                            <div class="col-sm-6"> 
                                <select class="form-control required" name="course" required>
                                    <option value="">Select Course</option>
                                    <option value="BTech">BTech</option>
                                    <option value="BArch">BArch</option>
                                    <option value="DUAL">Dual Degree</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label class="radio-inline">
                                    <input required type="radio" name="student_type" checked="" value="oldstudent"> &nbsp;Existing NIT Hamirpur Student</label>
                                <!--<label class="radio-inline">
                                <input required type="radio" checked="" name="student_type" value="newstudent">New Student</label>--> 
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-button" style="margin-top: 0;">
                                    <span id="sucess"></span>
                                    <span style="color:red;" id="error"></span> 
                                    <br>
                                    <span>Gentrate OTP <small>(OTP will be sent on your phone number.)</small></span> <br>
                                    <button type="button" class="ibtn OtpButton" id="OtpButton" style="float: right; margin-top: -26px;">Generate OTP </button>
                                </div>  
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <span id="checkPhone" class="showmsg"></span>  
                                <input class="form-control confirmOtp required" type="text" name="otpphone" placeholder="OTP on Your Phone" required>
                        <!--<input class="form-control" type="text" name="otpemail" placeholder="OTP Email" required><span id="checkEmail" class="showmsg"></span>-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <input class="form-control required" type="password" name="password" placeholder="Password" required>

                            </div>
                            <div class="col-sm-6">
                                <input class="form-control required" type="password" name="confirmPassword" placeholder="Confirm Password" required>  
                            </div>
                        </div>
                        <div class="row">                            
                            <div class="col-sm-12">
                                <div class="form-button">
                                    <button id="submitRegister" type="button" class="ibtn submitRegister">Register</button> 
                                </div> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
