@extends('layouts.app')

@section('content')
<div class="form-body without-side">
    <div class="website-logo">
        <a href="index.html">
            <div class="logo">
                <img class="logo-size" src="images/logo.png" alt="">
            </div>
        </a>
    </div>
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">
                <img src="images/logo.png" alt="">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <p style="text-align: center;font-weight: 500; color:red;">Please use Mozilla Firefox or Google Chrome for better compatibility.</p>
                              <p style="text-align: center;font-weight: 500; color:red;">If you are already registered on the portal in previous Semester, then use those credentials to login. Do not try to register again.</p>
                    <h3>Login to account</h3>
                    <p>Student Registration Portal NIT Hamirpur</p>
                    <form method="POST" id="loginForm" >
                        <input id="email" placeholder="Email ID" type="email" class="form-control" name="email" value="" required autofocus>
                        <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                        <br>
                        <select class="form-control" name="login_type" required>
                            <option value="">Select Login Type</option>  
                            <option value="student" selected="">Student</option>
                            <option value="faculty">Faculty</option> 
                            <option value="admin">Admin</option>
                            <option value="user">User</option> 
                        </select>

                        <div class="form-button">
                            <button  type="button" class="ibtn submitLogin">Login</button> <a href="{{ URL::to('/forget-password') }}">Forget password?</a>
                        </div>
                        <p id="sucessmsg"></p>
                        <p id="errormsg"></p>
                    </form>
                    <div class="other-links">
                        <!--<div class="text">Or login with</div>-->
                        <!--<a href=""><i class="fab fa-facebook-f"></i>Facebook</a><a href=""><i class="fab fa-google"></i>Google</a><a href=""><i class="fab fa-linkedin-in"></i>Linkedin</a>-->
                    </div>
                    <div class="page-links">
                        <a href="{{ route('register') }}">Register new account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
