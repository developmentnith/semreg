@extends('layouts.app')

@section('content')
<div class="form-body without-side">
    <div class="website-logo">
        <a href="index.html">
            <div class="logo">
                <img class="logo-size" src="images/logo.png" alt="">
            </div>
        </a>
    </div>
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">    
                <img src="images/logo.png" alt="">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <p style="text-align: center;font-weight: 500; color:red;">If you are already registered on the portal in previous Semester, then use those credentials to login. Do not try to register again.</p>
                    <h3>Register new account</h3>
                    <p>Student Registration</p>   
                    <form id="registerFrom">

                        <input class="form-control" type="text" name="name" placeholder="Full Name" required> 
                        <input class="form-control userCheckEmail" type="email" id="emailValue" onblur="validateEmail(this);" name="email" placeholder="Email Address" required>
                        <span id="userError" style="color:red; margin-left: 17px;"></span>
                        <input class="form-control" type="text" id="phoneValue" name="phone" maxlength="10" onkeypress="javascript:return isNumber(event)" placeholder="Mobile" required>
                        <select class="form-control" name="course" required>
                            <option>Select Course</option>
                            <option value="BTech">BTech</option>
                            <option value="BArch">BArch</option>
                            <option value="DUAL">Dual Degree</option>
                        </select>
                        <br>
                
                        <label class="radio-inline"><input required type="radio" name="student_type" checked="" value="oldstudent"> &nbsp;Existing NIT Hamirpur Student</label>
                        <!--<label class="radio-inline"><input required type="radio" checked="" name="student_type" value="newstudent">New Student</label>--> 

                        <div class="form-button">
                            <span style="margin-left: 20px;">Gentrate OTP <small>(OTP will be sent on your phone number.)</small></span> <br>
                            <span id="sucess"></span>
                            <span id="error"></span>  
                            <button type="button" class="ibtn OtpButton" id="OtpButton" style="float: right;">Generate OTP </button>
                        </div>
                        <input class="form-control confirmOtp" type="text" name="otpphone" placeholder="OTP on Your Phone" required><span id="checkPhone" class="showmsg"></span>  
                        <input class="form-control" type="password" name="password" placeholder="Password" required>
                        <input class="form-control" type="password" name="confirmPassword" placeholder="Confirm Password" required>  
                        <!--<input class="form-control" type="text" name="otpemail" placeholder="OTP Email" required><span id="checkEmail" class="showmsg"></span>-->


                        <div class="form-button">
                            <button id="submitRegister" type="button" class="ibtn submitRegister" disabled>Register</button> 
                        </div>   
                    </form>
<!--                    <div class="other-links">
                        <p id="meg"></p>
                        <div class="text">Or register with</div>
                        <a href=""><i class="fab fa-facebook-f"></i>Facebook</a><a href=""><i class="fab fa-google"></i>Google</a><a href=""><i class="fab fa-linkedin-in"></i>Linkedin</a>
                    </div>-->
<!--                    <div class="page-links">
                        <br>
                        <a href="{{ route('login') }}">Login to account</a>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
