
@extends('layouts.app')

@section('content')
<div class="row">
    <div class ="col-md-12">
        <h1> Subject Master details</h1>
    </div>
</div>
<div class ="row">
    <div class="table table-responsive ">
        <div class="table table-bordered">
            <table>
                <tr>
                    <th width="150px;">No</th>
                    <th >Subject Name</th>
                    <th >Subject Code</th>
                    <th >Credit</th>
                    <th >Theory/Practical</th>
                    <th >Degree</th>
                    <th >Semester</th>
                    <th ></th>
                    <th width="30px" >Group</th>
                    <th >Subject Type</th>
                    <th width="30px">Total Lecture (Theory)</th>
                    <th width="30px">Total Lecture Practical</th>
                    <th width="30px">Active</th>
                    <th class="text-center" width="150px"><a href="#" class="create-modal btn btn-success btn-sm"></a></th>         </th>
                <i class="glyphicon glyphicon-plus"></i>
                </tr>
                {{ csrf_field()}}
                <?php $no = 1; ?>
                @foreach($subject as $key=> $value)
                <tr class="SubjectMaster{{$value->id}}">
                    <td>{{$no++}}</td>
                    <td>{{$value->SubjectName}}</td>
                    <td>{{$value->SubjectCode}}</td>
                    <td>{{$value->credit}}</td>
                    <td>{{$value->TheoryPractical}}</td>
                    <td>{{$value->Degree}}</td>
                    <td>{{$value->Semester}}</td>
                    <td>{{$value->Group}}</td>
                    <td>{{$value->SubjectType}}</td>
                    <td>{{$value->TotalLectureTheory}}</td>
                    <td>{{$value->TotalLecturePractical}}</td>
                    <td>{{$value->isActive}}</td>
                    <td><a href="#" class="show-modal btn btn-info btn-sm" data-id="{{$value->id}}" data-title="{{$value->SubjectCode}}" data-body="{{$value->SubjectName}}"><i class="fa fa-eye" ></i>

                            <a href="#" class="edit-modal btn btn-warning btn-sm" data-id="{{$value->id}}" data-title="{{$value-SubjectCode}}" data-body="{{$value->SubjectName}}">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>

                            <a href="#" class="delete-modal btn btn-danger btn-sm" data-id="{{$value->id}}" data-title="{{$value-SubjectCode}}" data-body="{{$value->SubjectName}}">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a> 


                    </td>
                </tr>
                @endforeach


            </table>

        </div>
    </div>

</div>

<div id="create" class="modal fade" role ="dialog">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dissmiss="modal">%time</button>
                <h4 class="modal-title"></h4>
            </div>
        </div><div class="main-body">
            <form class="form-horizontal" role ="form">

                <div class="form-group row add">
                    <label class="control-label col-sm-2" for="title">Subject Name </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="subjectname" name="subjectname" placeholder="Enter subject Name" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Subject Code </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="subjectcode" name="subjectcode" placeholder="Enter Subject Code" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Credit </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="credit" name="credit" placeholder="Enter Subject Credit" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Theory/Practical </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="theorypractical" name="theorypractical" placeholder=Theory/Practical" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Degree</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="degree" name="degree" placeholder="Degree" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Semester </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="semester" name="semester" placeholder="Semester" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Group </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="group" name="group" placeholder="Enter Group" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Subject Type </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="subjecttype" name="subjecttype" placeholder="Enter Subject Type" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Total Lecture Theory </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="totallecturetheory" name="totallecturetheory" placeholder="Enter Total Theory Leture" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Total Lecture Practical </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="totallecturepractical" name="totallecturepractical" placeholder="Enter Total Theory Practical" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div> 

                <div class="form-group">
                    <label class="control-label col-sm-2" for="title">Active[Y/N] </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="isactive" name="isactive" placeholder="Active[Y/N]" required>
                        <p class="error text-center alert alert-danger hidden"></p>
                    </div>
                </div> 
            </form>
        </div>

        <div class="modal-footer">
            <button class="btn btn-warning" type ="button" id="add">
                <span class="glyphicon glyphicon-plus"></span>Save
            </button>
            <button class="btn btn-warning" type ="button" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span>Close
            </button>

        </div>

    </div>

</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="crossorigin="anonymous"></script>



<script type="text/javascript">
$(document).on('click', '.create-modal', function () {
    $('#create').modal('show');
    $('.form-horizontal').show();
    $('.modal-title').text('Add Subject');
});

//function to add subject

$('#add').click(function () {
    $.ajax({
        type: 'POST',
        url: 'addSubject',
        data: {
            '_token': $('input[name=_token]').val(),
            'subjectname': $('input[name=subjectname]').val(),
            'subjectcode': $('input[name=subjectcode]').val(),
            'credit': $('input[name=credit]').val(),
            'theorypractical': $('input[name=theorypractical]').val(),
            'degree': $('input[name=degree]').val(),
            'semester': $('input[name=semester]').val(),
            'group': $('input[name=group]').val(),
            'subjecttype': $('input[name=subjecttype]').val(),
            'totallecturetheory': $('input[name=totallecturetheory]').val(),
            'totallecturepractical': $('input[name=totallecturepractical]').val(),
            'isactive': $('input[name=isactive]').val()

        },
        success: function (data) {
            if (data.error){
            $('.error').removeClass('hidden');
                    $('.error').text(data.errors.subjectname);
                    $('.error').text(data.errors.subjectcode);
                    $('.error').text(data.errors.credit);
                    $('.error').text(data.errors.theorypractical);
                    $('.error').text(data.errors.degree);
                    $('.error').text(data.errors.semester);
                    $('.error').text(data.errors.group);
                    $('.error').text(data.errors.subjecttype);
                    $('.error').text(data.errors.totallecturetheory);
                    $('.error').text(data.errors.totallecturepractical);
                    $('.error').text(data.errors.isactive);
            }
            else{
            $('.error').remove();
                    $('#table').append("<tr class='subject" + data.id + "'>") +
                    "<td>" + data.id + "</td>" +
                    "<td>" + data.subjectname + "</td>" +
                    "<td>" + data.subjectcode + "</td>" +
                    "<td>" + data.credit + "</td>" +
                    "<td>" + data.theorypractical + "</td>" +
                    "<td>" + data.degree + "</td>" +
                    "<td>" + data.semester + "</td>" +
                    "<td>" + data.group + "</td>" +
                    "<td>" + data.subjecttype + "</td>" +
                    "<td>" + data.totallecturetheory + "</td>" +
                    "<td>" + data.totallecturepractical + "</td>" +
                    "<td>" + data.isactive + "</td>" + "</tr>" + );
        }
    },
    }
    );
    $('#subjectname').val('');
    $('#subjectcode').val();
});

</script>
@endsection