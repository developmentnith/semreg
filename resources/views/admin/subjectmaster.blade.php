@extends('layouts.admin_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Subject List</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-10">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    All Subject Lists
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="javascript:void()" data-toggle="modal" data-target="#createSubjectModal" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Add Subject
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <table class="table table-hover regStudent" id="subjectTable"> 
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">SI.NO #</th>
                                    <th scope="col">Subject Name</th>
                                    <th scope="col">Subject Code</th>
                                    <th scope="col">Credit</th>
                                    <th scope="col">Theory/Practical</th>             
                                    <th scope="col">Degree</th>
                                    <th scope="col">Semester</th>
                                    <th scope="col">Group</th>
                                    <th scope="col">Subject Type</th>
                                    <th scope="col">Total Lecture (Theory)</th>
                                    <th scope="col">Total Lecture Practical</th>
                                    <th scope="col">Status</th>
                                    <th scope="col" width="10%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sr = 1;
                                if (!empty($subject)) {
                                    foreach ($subject as $key => $row) {
                                        ?>
                                        <tr>
                                            <td><?= $sr; ?></td>
                                            <td><?= $row->SubjectName; ?></td>
                                            <td><?= $row->SubjectCode; ?></td>
                                            <td><?= $row->credit; ?></td>
                                            <td><?= $row->TheoryPractical; ?></td>
                                            <td><?= $row->Degree; ?></td>
                                            <td><?= $row->Semester; ?></td>
                                            <td><?= $row->Group; ?></td>
                                            <td><?= $row->SubjectType; ?></td>
                                            <td><?= $row->TotalLectureTheory; ?></td>
                                            <td><?= $row->TotalLabPractical; ?></td>
                                            <td><?= $row->isActive; ?></td>
                                            <td>
                                                <a class="btn btn-primary btn-xs" onclick="showAdminAjaxModal('{{ url('admin/edit-modal-pop/edit_subject/'.$row->id.'') }}');" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a class="btn btn-danger btn-xs" href="{{ url('admin/subjectdelete/'.$row->id.'') }}" onclick="return confirm('Are you sure you want to delete this?')" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        $sr++;
                                    }
                                } else {
                                    echo 'NO Data Found';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->

<div class="modal fade" id="createSubjectModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Add Subject</h3>

            </div>
            <div class="modal-body">    
                <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('admin/add-subject') }}"> 
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Subject Name"  name="subjectname" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" placeholder="Subject Code" name="subjectcode" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Credit" required="" name="credit" type="text">
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="theorypractical">
                                <option value="Theory">Theory</option>
                                <option value="Practical">Practical</option>
                            </select>
                           
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="degree">
                                <option value="BTech">BTech</option>
                                <option value="BArch">BArch</option>
                                <option value="MTech">MTech</option>
                                <option value="MArch">MArch</option>
                                <option value="Dual">Dual</option>
                                <option value="DualUG">DualUG</option>
                                <option value="DualPG">DualPG</option>
                            </select>
                        
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="semester">
                                <option value="S01">S01</option>
                                <option value="S02">S02</option>
                                <option value="S02">S02</option>
                                <option value="S03">S03</option>
                                <option value="S04">S04</option>
                                <option value="S05">S05</option>
                                <option value="S06">S06</option>
                                <option value="S07">S07</option>
                                <option value="S08">S08</option>
                                <option value="S09">S09</option>
                                <option value="S10">S10</option>

                            </select>
                        </div>
                        <div class="form-group">
                           <select class="form-control" name="group">
                                  <option value="A">A</option>
                                  <option value="B">B</option>
                                  <option value="C">C</option>
                                  <option value="D">D</option>
                                </select> 
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="SubjectType">
                                
                                  <option value="Core">Core</option>
                                  <option value="Elective">Elective</option>
                                  <option value="CorePractical">Core Practical</option>
                                  <option value="ElectivePractical">Elective Practical</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input class="form-control" required="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturetheory" type="text">
                        </div>
                        <div class="form-group"> 
                            <input class="form-control" required="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturePractical" type="text">
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="isactive">
                                <option value="active">Active</option>
                                <option value="deactive">Deactive</option>
                            </select>
                        </div>

                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editSubjectModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Edit Subject</h3>

            </div>
            <div class="modal-body">    
                <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('admin/edit-subject') }}"> 
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <input class="form-control"   name="subjectname" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control"   name="subjectcode" type="text">
                        </div>
                        <div class="form-group">
                            <input class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Credit" required="" name="credit" type="text">
                        </div>
                        <div class="form-group">

                            <select class="form-control" name="theorypractical">
                                <option value="Theory">Theory</option>
                                <option value="Practical">Practical</option>
                            </select>

                        </div>
                        <div class="form-group">

                            <select class="form-control" name="degree">
                                <option value="BTech">BTech</option>
                                <option value="BArch">BArch</option>
                                <option value="MTech">MTech</option>
                                <option value="MArch">MArch</option>
                                <option value="Dual">Dual</option>
                                <option value="DualUG">DualUG</option>
                                <option value="DualPG">DualPG</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="semester">
                                <option value="S01">S01</option>
                                <option value="S02">S02</option>
                                <option value="S02">S02</option>
                                <option value="S03">S03</option>
                                <option value="S04">S04</option>
                                <option value="S05">S05</option>
                                <option value="S06">S06</option>
                                <option value="S07">S07</option>
                                <option value="S08">S08</option>
                                <option value="S09">S09</option>
                                <option value="S10">S10</option>

                            </select>
                          
                        </div>
                        <div class="form-group">
                            
                              <select class="form-control" name="group">
                                  <option value="A">A</option>
                                  <option value="B">B</option>
                                  <option value="C">C</option>
                                  <option value="D">D</option>
                                </select>   
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="SubjectType">
                                
                                  <option value="Core">Core</option>
                                  <option value="Elective">Elective</option>
                                  <option value="CorePractical">Core Practical</option>
                                  <option value="ElectivePractical">Elective Practical</option>
                            </select>
                          
                        </div>
                        <div class="form-group">
                            <input class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturetheory" type="text">
                        </div>
                        <div class="form-group"> 
                            <input class="form-control"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Practical" required="" name="totallecturePractical" type="text">
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="isactive">
                                <option value="active">Active</option>
                                <option value="deactive">Deactive</option>
                            </select>
                        </div>

                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection