<div class="row">
    <div class="col-md-12">
        <?php
        $result = App\User::find($param2);
        ?>
        <form id="openelectiveedit"  action="{{ url('admin/edit-user') }}" method="post" >
            @csrf            
            <fieldset> 
                <div class="form-group">
                    <label for="name">User Name<span class="requiredfield">*</span></label>
                    <div class="col-sm-12">  
                        <input class="form-control"  placeholder="Name" required="" value="{{$result->name }}" name="name" type="text">
                        <input class="form-control"   required="" value="{{$result->id }}" name="user_id" type="hidden">
                    </div>
                </div>

                <div class="form-group">
                    <label for="email">Email<span class="requiredfield">*</span></label>
                    <div class="col-sm-12">  
                        <input class="form-control"  placeholder="Email" value="{{$result->email }}" required="" name="email" type="text"> 
                    </div>
                </div>
                <div class="form-group">
                    <label for="Password">Password<span class="requiredfield">*</span></label>
                    <div class="col-sm-12">  
                        <input class="form-control"  placeholder="Password" value="{{$result->password }}" required="" name="password" type="password"> 
                    </div>
                </div>
                <div class="form-group">
                    <label for="semester">Department<span class="requiredfield">*</span></label>
                    <div class="col-sm-12">  
                        <select class="form-control" required="" name="department">
                            <option value="" selected>Please Select Department</option>                                   
                            @foreach ($populatebranshort as $populatebr) 
                            <option <?php if ($result->department == $populatebr->DepartmentShortName) echo 'selected'; ?> value="{{ $populatebr->DepartmentShortName }}">{{ $populatebr->DepartmentFullName }}</option>
                            @endforeach                               
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="semester">Type<span class="requiredfield">*</span></label>
                    <div class="col-sm-12">  
                        <select class="form-control" required="" name="type">
                            <option value="" selected>Please Select Type</option>                                   
                            <option <?php if ($result->type == 'faculty') echo 'selected'; ?> value="faculty">Faculty</option>                                   
                            <option <?php if ($result->type == 'student') echo 'selected'; ?> value="student">Student</option>                                   
                            <option <?php if ($result->type == 'user') echo 'selected'; ?> value="user">User</option>                                   
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="semester">Role<span class="requiredfield">*</span></label>
                    <div class="col-sm-12">  
                        <?php
                        $rolldata = array("ADMIN", "FACULTY", "NODUES", "SUGC", "SPGC", "USER");
                        $role = explode(',', $result->role);
                        $rollValue = array();
                        foreach ($rolldata as $value) {
                            for ($i = 0; $i < count($role); $i++) {
                                $roleshow = $role[$i];
                                $rollValue[] = $role[$i];
                                if ($value == $roleshow) { 
                                    ?>
                                    <div class="checkbox"> 
                                        <label><input type="checkbox" name="role[]" <?= ($roleshow == $value) ? 'checked' : '' ?>  value="<?= $value ?>"><?= $value ?></label>
                                    </div>
                                    <?php
                                }
                               
                            }
                        }
                        ?>

                    </div>
                </div>
                <br>
                <br>

                <input class="btn btn-success pull-right" type="submit" value="Submit">
            </fieldset>
        </form>
    </div>
</div>
