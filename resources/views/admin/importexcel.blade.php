@extends('layouts.admin_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Import Excel</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-10">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Import Excel Sheet
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="javascript:void()" data-toggle="modal" data-target="#createExcelModal" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Upload Data
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl"> 
                    <div class="col-md-12 col-lg-12 col-xl-12">
                        <table class="table table-hover" id="myTable2"> 
                            <thead style="background: #f1f2f7;">
                                <tr>
                                    <th scope="col">SI.No</th>
                                    <th scope="col">File Name</th>
                                    <th scope="col">File</th>                                  
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $sr = 1;  @endphp
                                @foreach($importfile as $row)
                                <tr>
                                    <td><?= $sr ?></td> 
                                    <td><?= $row->fileName ?></td>
                                    <td><a href="<?= 'upload/studentphoto/' . $row->fileExtension ?>"><span class="fa fa-download"></a></td>
                                    <td>
                                        <a class="btn btn-danger btn-xs" href="{{ url('admin/import-file/'.$row->id.'') }}" onclick="return confirm('Are you sure you want to delete this?')" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-trash" aria-hidden="true"></i></a>
                                    </td>
                                </tr>
                                @php $sr++ @endphp
                                @endforeach      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->

<div class="modal fade" id="createExcelModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Uplaod Data</h3>

            </div>
            <div class="modal-body">     
                <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('admin/add-excel') }}" enctype="multipart/form-data"> 
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <label for="degree">Select Table<span class="requiredfield">*</span></label>
                            <select class="form-control" required="" tabindex="10" id="degree" name="table_name">
                                <option value="" selected>Please Select Table</option>
                                <option value="1" selected>Student Data</option>
                                <option value="2" selected>Student SGPI/CGPI</option> 
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">File Name</label>
                            <input class="form-control" required="" placeholder="File Name"  name="filenames" type="text">
                        </div>                       
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Select File</label>
                            <input type="file" name="exceldata" class="form-control-file" id="exampleFormControlFile1">
                        </div>                       
                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>
            </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection