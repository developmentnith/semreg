<div class="row">
    <div class="col-md-12">
        <?php
        ?>
        <form id="openelectiveedit"  action="{{ url('admin/edit-student') }}" method="post" >
            @csrf            
            <fieldset> 
                <div class="form-group">
                    <label>Student Name</label>
                    <input class="form-control"   value="<?= $result[0]->StudentName ?>"  name="StudentName" type="text">
                    <input class="form-control"    value="<?= $result[0]->studentID ?>"  name="studentID" type="hidden" readonly="">
                    <input class="form-control"    value="dep"  name="type" type="hidden" readonly="">
                </div>
                <div class="form-group">
                    <label>Student Father Name</label>
                    <input class="form-control"   value="<?= $result[0]->FatherName ?>" name="fathername" type="text">
                </div>
                <div class="form-group">
                    <label>Student Father Name Hindi.</label>
                    <input class="form-control"   value="<?= $result[0]->FatherNameHindi ?>" name="fathernamehinid" type="text">
                </div>
                <div class="form-group">
                    <label>Student Mother Name</label>
                    <input class="form-control"   value="<?= $result[0]->MotherName ?>" name="mothername" type="text">
                </div>
                <div class="form-group">
                    <label>Student Mother Name Hindi.</label>
                    <input class="form-control"   value="<?= $result[0]->MotherNameHindi ?>" name="mothernamehindi" type="text">
                </div>
                <div class="form-group">
                    <label>Student Roll No.</label>
                    <input class="form-control"   value="<?= $result[0]->InstituteRollNumber ?>" name="InstituteRollNumber" type="text">
                </div>
                <div class="form-group">
                    <label>Student Email ID</label>
                    <input class="form-control"  value="<?= $result[0]->EmailIDStudent ?>" name="EmailIDStudent" type="text">
                </div>
                <div class="form-group">
                    <label>Gender</label>
                    <input class="form-control"  value="<?= $result[0]->Gender ?>" name="Gender" type="text">
                </div>
                <div class="form-group">
                    <label>Gender</label>
                    <input class="form-control"  value="<?= $result[0]->Gender ?>" name="Gender" type="text">
                </div>
                <div class="form-group">
                    <label>Select Group </label>
                    <select class="form-control"  name="group">
                        <option value="">Select Group</option>
                        <option value="1"<?php if ($resultdata[0]->Grop == '1') echo 'selected'; ?>>1</option>
                        <option value="2"<?php if ($resultdata[0]->Grop == '2') echo 'selected'; ?>>2</option>
                        <option value="3"<?php if ($resultdata[0]->Grop == '3') echo 'selected'; ?>>3</option>                                               
                    </select>
                </div>  
                <div class="form-group">
                    <label>Section </label>
                    <select class="form-control"  name="section">
                        <option value="">Select Group</option>
                        <option value="A"<?php if ($resultdata[0]->Section == 'A') echo 'selected'; ?>>A</option>
                        <option value="B"<?php if ($resultdata[0]->Section == 'B') echo 'selected'; ?>>B</option>
                        <option value="C"<?php if ($resultdata[0]->Section == 'C') echo 'selected'; ?>>C</option>
                        <option value="D"<?php if ($resultdata[0]->Section == 'D') echo 'selected'; ?>>D</option>
                        <option value="E"<?php if ($resultdata[0]->Section == 'E') echo 'selected'; ?>>E</option>                                                 
                        <option value="F"<?php if ($resultdata[0]->Section == 'F') echo 'selected'; ?>>F</option>                                                 
                        <option value="G"<?php if ($resultdata[0]->Section == 'G') echo 'selected'; ?>>G</option>                                                 
                        <option value="H"<?php if ($resultdata[0]->Section == 'H') echo 'selected'; ?>>H</option>                                                 
                        <option value="I"<?php if ($resultdata[0]->Section == 'I') echo 'selected'; ?>>I</option>                                                 
                        <option value="J"<?php if ($resultdata[0]->Section == 'J') echo 'selected'; ?>>J</option>                                                 
                        <option value="Z"<?php if ($resultdata[0]->Section == 'Z') echo 'selected'; ?>>Z</option>                                                  
                    </select>
                </div>
                <div class="form-group">
                    <label>Branch</label>
                    <select class="form-control required" required="" tabindex="36" id="admissionbranch" name = "AdmissionBranch" >
                        <option value="" selected>Please Select</option>
                        @foreach ($populatebranshort as $populatebr)
                        <option value="{{ $populatebr->DepartmentShortName }}"<?php if ($result[0]->AdmissionBranch == $populatebr->DepartmentShortName) echo 'selected'; ?>>{{ $populatebr->DepartmentShortName }}</option>
                        @endforeach 
                    </select>
                </div>                      
                <input class="btn btn-success pull-right" type="submit" value="Submit">
            </fieldset>

        </form>
    </div>
</div>
