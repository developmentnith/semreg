@extends('layouts.admin_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Open Elective</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-4">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Open Elective Allotment Details
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="javascript:void()" data-toggle="modal" data-target="#CreateOpenElectiveModal" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Open Elective Allotment
                                        </span>
                                    </span>
                                </a>
                            </div>
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="{{url('admin/cancel-allotment')}}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                           Cancel Recent Open Elective Allotment 
                                        </span>
                                    </span>
                                </a>
                            </div>
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="{{url('admin/final-open-allotment')}}"  class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                          Finalize Open Allotment 
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">

                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <table class="table table-hover regStudent" > 
                                    <thead style="background: #f1f2f7;">
                                        <tr>
                                            <th scope="col">Roll Number</th>
                                            <th scope="col">Subject Code</th>
                                            <th scope="col">Subject Name</th>
                                            <th scope="col">Department</th>
                                            <th scope="col">Choice Num</th>             
                                            <th scope="col">Semester</th>
                                            <th scope="col">Year</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($AllotmentDetail as $row)
                                        <tr>
                                            <td><?= $row->RollNumber ?></td>
                                            <td><?= $row->SubjectID ?></td>
                                            <td><?= $row->SubjectName ?></td>
                                            <td><?= $row->Department ?></td>
                                            <td><?= $row->ChoiceNum ?></td>
                                            <td><?= $row->Semester ?></td>
                                            <td><?= $row->Year ?></td>
                                            <td><?= $row->Status ?></td>
                                            <td>
                                             @if ($row->Token === 'N')
                                                <a class="btn btn-primary btn-xs" onclick="showAdminAjaxModal('{{ url('admin/modal-pop/edit_openelective/'.$row->id.'') }}');"  style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a class="btn btn-danger btn-xs" href="{{ url('admin/delete-openelective-allotted/'.$row->id.'') }}" onclick="return confirm('Are you sure you want to delete this?')" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-trash" aria-hidden="true"></i></a>
                                                @else
                                                Reocord Finallized
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach      
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->



<div class="modal fade" id="CreateOpenElectiveModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Give Open Elective Details</h3>

            </div>
            <div class="modal-body">    
                <form id="openelectiveallot" onsubmit="return confirm('Do you really want to submit the form?');" action="{{ url('admin/open-elective-allotment') }}" method="post" >
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <label for="degree">Degree<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <select class="form-control" required="" tabindex="10" id="degree" name="degree">
                                    <option value="" selected>Please Select Degree</option>
                                    <option value="ALL">ALL</option>
                                    @foreach($degree as $deg)
                                    <option value="{{$deg->Degree}}">{{$deg->Degree}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="semester">Semester<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <select class="form-control"  required="" id="semester" name="semester">

                                    <option value="" selected>Please Select Semester</option>
                                    @foreach($openelective as $de)
                                    <option value="{{$de->Semester}}">{{$de->Semester}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="year">Year<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <select class="form-control" required=""  id="year" name="year">
                                    <option value="" selected>Please Select</option>
                                    @foreach($yer as $ye)
                                    <option value="{{$ye->FloatYear}}">{{$ye->FloatYear}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nos"># of student per class <span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <select class="form-control" required=""  id="nos" name="nos">
                                    <option value="" selected>Please Select</option>
                                   @for($i=40;$i <= 150; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                   @endfor
                                </select>
                            </div>
                        </div>
                        <br>
                        <br>

                        <input class="btn btn-success pull-right" type="submit" value="Make Allotment">
                    </fieldset>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div> 

        </div>
    </div>
</div>






@endsection