@extends('layouts.admin_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Start and End Date for Semester Registration and Elective Subjects</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-7">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Add Start and End Date for Semester Registration and Elective Subjects
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__head">

                    <div class="col-md-5">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 15px;">
                                <a href="javascript:void()" data-toggle="modal" data-target="#RegistrationDatesConfig" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Specify Start and End Date for Semester Registration and Open Elective
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">

                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-md-12 col-lg-12 col-xl-12">

                                <table class="table table-hover regStudent" id="subjectTable" > 
                                    <thead style="background: #f1f2f7;">
                                        <tr>
                                            <th scope="col">Sr.NO #</th>
                                            <th scope="col">Type</th>
                                            <th scope="col">Department</th>             
                                            <th scope="col">Degree</th>
                                            <th scope="col">Semester</th>
                                            <th scope="col">Session</th>
                                            <th scope="col">Start Date</th>
                                            <th scope="col">End Date</th>
                                            <th scope="col">Is Active</th>

                                            <th scope="col" width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($detail as $row)

                                        <tr>
                                            <td><?= $loop->index ?></td>
                                            <td><?= $row->Type ?></td>
                                            <td><?= $row->Department ?></td>
                                            <td><?= $row->Degree ?></td>
                                            <td><?= $row->Semester ?></td>
                                            <td><?= $row->Session ?></td>
                                            <td><?= $row->StartDate ?></td>
                                            <td><?= $row->EndDate ?></td>
                                            <td><?= $row->Status ?></td>
                                            <td><a href="{{ url('admin/delete-registration-date/'.$row->RID.'') }}" onclick="return confirm('Are you sure you want to delete this?')" type="button" class="btn btn-danger btn-xs">Delete<i class="fa fa-times" aria-hidden="true"></i></a>
                                                <a href="{{ url('admin/deactivate-registration-date/'.$row->RID.'') }}" onclick="return confirm('Are you sure you want to Deactivate this?')" type="button" class="btn btn-info btn-xs">Deactivate<i class="fa fa-check-square" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @endforeach  

                                    </tbody>
                                </table>

                            </div>
                        </div>


                    </div>
                </div>
            </div>

            <!--deactivate portion--------------------->


            <div class="row m-row--no-padding m-row--col-separator-xl">
                <div class="col-md-12 col-lg-12 col-xl-12">

                    <span>

                        <span>
                            <h2>    Deactivated Record Details</h2>
                        </span>
                    </span>
                    <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 50px 50px;">
                        <!---end-->
                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-md-12 col-lg-12 col-xl-12">

                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-md-12 col-lg-12 col-xl-12">

                                        <table class="table table-hover regStudent" id="subjectTable" > 
                                            <thead style="background: #f1f2f7;">
                                                <tr>
                                                    <th scope="col">Sr.NO #</th>
                                                    <th scope="col">Type</th>
                                                    <th scope="col">Department</th>             
                                                    <th scope="col">Degree</th>
                                                    <th scope="col">Semester</th>
                                                    <th scope="col">Session</th>
                                                    <th scope="col">Start Date</th>
                                                    <th scope="col">End Date</th>
                                                    <th scope="col">Is Active</th>

                                                    <th scope="col" width="10%">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($detail1 as $row)

                                                <tr>
                                                    <td><?= $loop->index ?></td>
                                                    <td><?= $row->Type ?></td>
                                                    <td><?= $row->Department ?></td>
                                                    <td><?= $row->Degree ?></td>
                                                    <td><?= $row->Semester ?></td>
                                                    <td><?= $row->Session ?></td>
                                                    <td><?= $row->StartDate ?></td>
                                                    <td><?= $row->EndDate ?></td>
                                                    <td><?= $row->Status ?></td>
                                                    <td><
                                                        <a href="{{ url('admin/activate-registration-date/'.$row->RID.'') }}" onclick="return confirm('Are you sure you want to Activate this?')" type="button" class="btn btn-danger btn-xs">Activate<i class="fa fa-times" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                                @endforeach  

                                            </tbody>
                                        </table>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->



<div class="modal fade" id="RegistrationDatesConfig" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Start and End Date for Semester Registration and Elective Subjects</h3>

            </div>
            <div class="modal-body">    
                <form id="regdateconfig" action="{{'/admin/add-semreg-start-end-date'}}" onsubmit="return checkForm(this);" method="post" >
                    @csrf
                    <fieldset> 
                        <div class="col-md-12 col-lg-12 col-xl-12">

                            <div class="form-group">
                                <label class="control-label col-sm-12" for="Type">Type<span class="requiredfield">*</span></label>
                                <div class="col-sm-12">  

                                    <select class="form-control" required=""  tabindex="1" id ="Type" name = "type" >
                                        <option value="" selected>Please Select</option>
                                        <option value="SEMESTER">SEMESTER</option>
                                        <option value="OPEN ELECTIVE">OPEN ELECTIVE</option>
                                        <option value="DEPARTMENT ELECTIVE">DEPARTMENT ELECTIVE</option>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-12" for="degree">Degree<span class="requiredfield">*</span></label>
                                <div class="col-sm-12">  

                                    <select class="form-control" required=""  tabindex="1" id ="Type" name = "degree" >
                                        <option value="" selected>Please Select</option>
                                        <option value="BTech">B.Tech.</option>
                                        <option value="DUAL">DUAL (B.Tech. & M.Tech)</option>
                                        <option value="MTech">M.Tech.</option>
                                        <option value="MArch">M.Arch.</option>
                                        <option value="MBA">MBA</option>
                                    </select>

                                </div>

                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-12" for="department">Department<span class="requiredfield">*</span></label>
                                <div class="col-sm-12">  

                                    <select class="form-control" required="" tabindex="36" id="department" name = "department" >
                                        <option value="" selected>Please Select</option>
                                        @foreach ($populatebranshort as $populatebr)
                                        <option value="{{ $populatebr->DepartmentShortName }}">{{ $populatebr->DepartmentFullName }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-12" for="session">Session<span class="requiredfield">*</span></label>
                                <div class="col-sm-12">  

                                    <select class="form-control" required=""  tabindex="2" id ="session" name = "session" >
                                        <option value="" selected>Please Select</option>
                                        <option value="January">January</option>
                                        <option value="July">July</option>
                                    </select>
                                </div>
                            </div>         


                            <div class="form-group">
                                <label class="control-label col-sm-12" for="semester">Semester<span class="requiredfield">*</span></label>
                                <div class="col-sm-12">  

                                    <select class="form-control" required=""  tabindex="2" onchange="GetSelectedTextValue(this)" id ="Semester" name = "semester" >
                                        <option value="" selected>Please Select</option>
                                        <option value="S01">S01</option>
                                        <option value="S02">S02</option>
                                        <option value="S03">S03</option>
                                        <option value="S04">S04</option>
                                        <option value="S05">S05</option>
                                        <option value="S06">S06</option>
                                        <option value="S07">S07</option>
                                        <option value="S08">S08</option>
                                        <option value="S09">S09</option>
                                        <option value="S01">S10</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-12" for="Start Date">Start Date<span class="requiredfield">*</span></label>
                                <div class="col-sm-12">  

                                    <div class="form-group">
                                        <input class="form-control" required=""   placeholder="Start Date"  name="startdate" type="date">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-12"   for="enddate">End Date<span class="requiredfield">*</span></label>
                                <div class="col-sm-12">  


                                    <div class="form-group">
                                        <input class="form-control"   required="" placeholder="End Date" name="enddate" type="date">
                                    </div>

                                </div>
                            </div>



                        </div>
                        <input class="btn btn-success pull-right" type="submit" value="Save">
                    </fieldset>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
            </div> 

        </div>
    </div>
</div>






@endsection