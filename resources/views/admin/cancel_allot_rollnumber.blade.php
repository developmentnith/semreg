<div class="row">
    <div class="col-md-12">
        <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('admin/cancel-update-student') }}"> 
            @csrf
            <fieldset> 
                <div class="form-group">
                    <label class="control-label col-sm-6" for="title">Student File Number: </label>
                    <input class="form-control" required="" placeholder="Enter Student File Number" value="{{ $result[0]->FileNumber }}"  name="filenumber" type="text">
                    <input type="hidden" readonly="" name="studentId" value="{{$result[0]->studentID}}">
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-6" for="title">Registration Number: </label>
                    <input class="form-control" required="" placeholder="Student Registration Number" value="{{ $result[0]->RegistrationNumber }}"  name="regNumber" type="text">
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-6" for="title">Manually Allot RollNo: </label>
                    <select class="form-control auto_RollNubmer" name="autorollnubmer" id="auto_RollNubmer">
                        <option value="manually">Manually Roll Number</option>                        
                    </select>

                </div>

                <!--<div style="display:none;" id="autodiv">--> 
                <div  id="autodiv"> 
                    <div class="form-group">
                        <label class="control-label col-sm-6" for="title">Enter Roll Number: </label>
                        <input class="form-control"  placeholder="Enter Roll Number" value="{{ $result[0]->InstituteRollNumber }}" name="rollnumber" type="text">
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-6" for="title">Select Group: </label>
                        <select class="form-control"  name="group">
                            <option value="">Select Group</option>
                            <option value="1"<?php if ($resultdata[0]->Grop == '1') echo 'selected'; ?>>1</option>
                            <option value="2"<?php if ($resultdata[0]->Grop == '2') echo 'selected'; ?>>2</option>
                            <option value="3"<?php if ($resultdata[0]->Grop == '3') echo 'selected'; ?>>3</option>                                               
                        </select>
                    </div>  
                    <div class="form-group">
                        <label class="control-label col-sm-6" for="title">Select Section: </label>
                        <select class="form-control"  name="section">
                            <option value="">Select Group</option>
                            <option value="A"<?php if ($resultdata[0]->Section == 'A') echo 'selected'; ?>>A</option>
                            <option value="B"<?php if ($resultdata[0]->Section == 'B') echo 'selected'; ?>>B</option>
                            <option value="C"<?php if ($resultdata[0]->Section == 'C') echo 'selected'; ?>>C</option>
                            <option value="D"<?php if ($resultdata[0]->Section == 'D') echo 'selected'; ?>>D</option>
                            <option value="E"<?php if ($resultdata[0]->Section == 'E') echo 'selected'; ?>>E</option>                                                 
                            <option value="F"<?php if ($resultdata[0]->Section == 'F') echo 'selected'; ?>>F</option>                                                 
                            <option value="G"<?php if ($resultdata[0]->Section == 'G') echo 'selected'; ?>>G</option>                                                 
                            <option value="H"<?php if ($resultdata[0]->Section == 'H') echo 'selected'; ?>>H</option>                                                 
                            <option value="I"<?php if ($resultdata[0]->Section == 'I') echo 'selected'; ?>>I</option>                                                 
                            <option value="J"<?php if ($resultdata[0]->Section == 'J') echo 'selected'; ?>>J</option>                                                 
                            <option value="Z"<?php if ($resultdata[0]->Section == 'Z') echo 'selected'; ?>>Z</option>                                                  
                        </select>

                    </div>


                </div>


                <input class="btn btn-success pull-right" type="submit" value="Submit">
            </fieldset>
        </form>
    </div>
</div>
