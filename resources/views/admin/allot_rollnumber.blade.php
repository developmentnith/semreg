<div class="row">
    <div class="col-md-12">
        <form accept-charset="UTF-8" role="form" method="POST" action="{{ url('admin/update-student') }}"> 
            @csrf
            <fieldset> 
                <div class="form-group">
                    <label class="control-label col-sm-6" for="title">Student File Number: </label>
                    <input class="form-control" required="" placeholder="Enter Student File Number"  name="filenumber" type="text">
                    <input type="hidden" readonly="" name="studentId" value="{{$studentid}}">
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-6" for="title">Registration Number: </label>
                    <input class="form-control" required="" placeholder="Student Registration Number" value="2K19-" name="regNumber" type="text">
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-6" for="title">Auto/Manually Allot RollNo: </label>
                    <select class="form-control auto_RollNubmer" name="autorollnubmer" id="auto_RollNubmer">
                        <option value="manually">Manually Roll Number</option>                        
                        <!--<option value="auto">Auto Allot RollNo</option>-->
                    </select>

                </div>

                <!--<div style="display:none;" id="autodiv">--> 
                <div  id="autodiv"> 
                    <div class="form-group">
                        <label class="control-label col-sm-6" for="title">Enter Roll Number: </label>
                        <input class="form-control"  placeholder="Enter Roll Number" name="rollnumber" type="text">
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-6" for="title">Select Group: </label>
                        <select class="form-control"  name="group">
                            <option value="">Select Group</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>                                               
                        </select>
                    </div>  
                    <div class="form-group">
                        <label class="control-label col-sm-6" for="title">Select Section: </label>
                        <select class="form-control"  name="section">
                            <option value="">Select Group</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                            <option value="E">E</option>                                                 
                            <option value="F">F</option>                                                 
                            <option value="G">G</option>                                                 
                            <option value="H">H</option>                                                 
                            <option value="I">I</option>                                                 
                            <option value="J">J</option>                                                 
                            <option value="Z">Z</option>                                                  
                        </select>

                    </div>


                </div>


                <input class="btn btn-success pull-right" type="submit" value="Submit">
            </fieldset>
        </form>
    </div>
</div>
