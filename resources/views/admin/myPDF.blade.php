<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

        <title>pdf</title>
        <style>

            * { margin: 0; padding: 0; 

            }   
            body {
                font: 12px/1.4 Helvetica, Arial, sans-serif;    

            }

            /*#page-wrap {margin: 10px auto; width: 1000px;border: 1px solid #ccc;padding: 20px;}*/
            #terms p {text-align: left;}
            #terms ul li {text-align: left;padding: 0 0;list-style: none;}
            textarea { border: 0; font: 14px Helvetica, Arial, sans-serif; overflow: hidden; resize: none; }
            table { border-collapse: collapse; }
            table td, table th { border: 1px solid black; padding: 5px; }

            #header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

            #address { width: 250px; height: 150px; float: left; }
            #customer { overflow: hidden; }

            #logo { text-align: right; float: right; position: relative; margin-top: 25px; border: 1px solid #fff; max-width: 540px; overflow: hidden; }
            #customer-title { font-size: 20px; font-weight: bold; float: left; }

            #meta { margin-top: 1px; width: 100%; float: right; }
            #meta td { text-align: right;  }
            #meta td.meta-head { text-align: left; background: #eee; }
            #meta td textarea { width: 100%; height: 20px; text-align: right; }

            #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
            #items th { background: #eee; }
            #items textarea { width: 80px; height: 50px; }
            #items tr.item-row td {  vertical-align: top; }
            #items td.description { width: 300px; }
            #items td.item-name { width: 175px; }
            #items td.description textarea, #items td.item-name textarea { width: 100%; }
            #items td.total-line { border-right: 0; text-align: right; }
            #items td.total-value { border-left: 0; padding: 10px; }
            #items td.total-value textarea { height: 20px; background: none; }
            #items td.balance { background: #eee; }
            #items td.blank { border: 0; }

            #terms { text-align: center; margin: 20px 0 0 0; }
            #terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
            #terms textarea { width: 100%; text-align: center;}
            .sig {
                height: 47px;
            }

            .sig p {
                text-align: center;
            }

            .originalfor {
                width: 100%;
            }
            .originalfor th {
                border: none;
            }
            .originalfor td {
                border: none;
            }
            .chek input {
                margin-top: 9px;
            }
            .bord {
                border: none;
            }
            .itdis {
                font-size:13px;
            }
            .divbor {
                border: 1px solid #333;
                padding: 1px;
                margin-top: 1px;
            }
            .taxinvoice th {
                width: 117px;
                height: 20px;
            }
            .taxin_voice td {
                width: 117px;
                height: 21px;
            }
            .ra {
                width: 50%;
            }
            /*.taxin_voice td {
              padding: 6px 45px;
            }*/
            .taxcgst th{
                font-weight: normal !important;  

            }
            .taxin_voice {
                margin-left: -6px;
                margin-top: -6px;
                margin-right: -6px;
                margin-bottom: -6px;
            }
            .taxinvoice {
                margin: 11px -6px -6px;
            }
            .texsummary {
                font-weight: bold;
            }
            .taxin_voice .raigst {
                width: 178px;
            }
            .taxinvoice .raigst {
                width: 178px;
            }
            /*.taxinvoice th {
              padding: 6px 43px;
            }*/
            .taxcgst th {
                font-weight: bold !important;
            }
            .taxin_voice p {
                width: 71px;
            }



            .delete-wpr { position: relative; }
            .delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }

            /* Extra CSS for Print Button*/
            .button {
                display: -webkit-box;
                display: -webkit-flex;
                display: -ms-flexbox;
                display: flex;
                overflow: hidden;
                margin-top: 20px;
                padding: 12px 12px;
                cursor: pointer;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                -webkit-transition: all 60ms ease-in-out;
                transition: all 60ms ease-in-out;
                text-align: center;
                white-space: nowrap;
                text-decoration: none !important;

                color: #fff;
                border: 0 none;
                border-radius: 4px;
                font-size: 14px;
                font-weight: 500;
                line-height: 1.3;
                -webkit-appearance: none;
                -moz-appearance: none;

                -webkit-box-pack: center;
                -webkit-justify-content: center;
                -ms-flex-pack: center;
                justify-content: center;
                -webkit-box-align: center;
                -webkit-align-items: center;
                -ms-flex-align: center;
                align-items: center;
                -webkit-box-flex: 0;
                -webkit-flex: 0 0 160px;
                -ms-flex: 0 0 160px;
                flex: 0 0 160px;
            }
            .button:hover {
                -webkit-transition: all 60ms ease;
                transition: all 60ms ease;
                opacity: .85;
            }
            .button:active {
                -webkit-transition: all 60ms ease;
                transition: all 60ms ease;
                opacity: .75;
            }
            .button:focus {
                outline: 1px dotted #959595;
                outline-offset: -4px;
            }

            .button.-regular {
                color: #202129;
                background-color: #edeeee;
            }
            .button.-regular:hover {
                color: #202129;
                background-color: #e1e2e2;
                opacity: 1;
            }
            .button.-regular:active {
                background-color: #d5d6d6;
                opacity: 1;
            }

            .button.-dark {
                color: #FFFFFF;
                background: #333030;
                float: right;
                margin-bottom: 26px;
                margin-right: 277px;
                margin-top: -5px;
            }
            .button.-dark:focus {
                outline: 1px dotted white;
                outline-offset: -4px;
            }
            .button.-darks {
                color: #FFFFFF;
                background: #333030;
                float: right;
                margin-bottom: 26px;

                margin-top: -5px;
            }
            .button.-darks:focus {
                outline: 1px dotted white;
                outline-offset: -4px;
            }
            .formSetting {
                width: 50%;
                display: inline;

            }
            .finalDoc {
                margin-left: 13px;
                margin-top: 11px;
                font-size: 10px; 
            }

            @media print
            {
                .no-print, .no-print *
                {
                    display: none !important;
                }

            }
            @page {
                margin: 10px auto; size: auto;
            }  

        </style>
    </head>

    <body>


        <div id="page-wrap" style="margin-left: 20px; margin-right: 20px;margin-top: 20px; border: 1px solid #000;"> 
            <p style="text-align: center;font-size: 18px; font-weight: bold;">NATIONAL INSTITUTE OF TECHNOLOGY, HAMIRPUR (H.P.)</p>
            <p style="float: right;font-size: 12px;">Registration No.{{$studentbasic[0]->RegistrationNumber}}</p>
            <table width="100%">   
                <tr>
                    <td style="border: 0;  text-align: left" width="20%">

                    </td>
                    <td style="border: 0;  text-align: left;" width="60%">    
                        <p style="text-align:center;font-size: 18px; font-weight: bold;">REGISTRATION SLIP</p>

                    </td>
                    <td style="border: 0;  text-align: left" width="20%">

                    </td>
                </tr>

            </table>
            <table width="100%">   
                <tr>
                    <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                        <p class="formSetting"><b>File No.</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->FileNumber}}</p>
                    </td>
                    <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                        <p class="formSetting"><b>Mr./Miss</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->StudentName}}</p>
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ccc; text-align: left" width="50%">
                        <p class="formSetting"><b>Roll No.</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->InstituteRollNumber}}</p>
                    </td>
                    <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                        <p class="formSetting"><b>S/o/D/o</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->FatherName}}</p>
                    </td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ccc; text-align: left" width="50%">
                        <p class="formSetting"><b>Section</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$StudentNumber[0]->Section}}</p>
                    </td>
                    <td style="border: 1px solid #ccc; text-align: left" width="50%">
                        <p class="formSetting"><b>Group</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$StudentNumber[0]->Grop}}</p>
                    </td>

                </tr>
                <tr>
                    <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                        <p class="formSetting"><b>Department</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$branchName[0]->DepartmentFullName}}</p>
                    </td>
                    <td style="border: 1px solid #ccc; text-align: left" width="50%">
                        <p class="formSetting"><b>Date of Registration</b></p>
                        <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->created_at}}</p>
                    </td>                   
                </tr>


            </table>
            <table width="100%" style="margin-top:1px;"> 
                <tr>
                    <td style="border: 0;  text-align: left">                              

                        <p style="margin-left:0px; margin-top: 5px; font-size: 12px;">
                            Document to be submitted (if any):  
                        </p>
                        <p>Admission / registration is provisional subject to production of the above document/s on or before</p>
                        <br></br>
                        <p><b>Place:</b></p>
                        <p style="float:right;">NIT, Hamirpur (HP) Dy. Registrar (Academic) or his nominee</p>
                        <p><b>Dated: </b></p>
                        <!--<p style="float:right;">NIT, Hamirpur(HP)-177005</p>-->
                    </td>
                </tr>
            </table>
            <div style="clear:both"></div> 


        </div>
        <br>
        <br>
        <br>
        <br>
            <div id="page-wrap" style="margin-left: 20px; margin-right: 20px; border: 1px solid #000;"> 
                <p style="text-align: center;font-size: 18px; font-weight: bold;">NATIONAL INSTITUTE OF TECHNOLOGY, HAMIRPUR (H.P.)</p>
                <p style="float: right;font-size: 12px;">Registration No.{{$studentbasic[0]->RegistrationNumber}}</p>
                <table width="100%">   
                    <tr>
                        <td style="border: 0;  text-align: left" width="20%">

                        </td>
                        <td style="border: 0;  text-align: left;" width="60%">    
                            <p style="text-align:center;font-size: 18px; font-weight: bold;">REGISTRATION SLIP</p>

                        </td>
                        <td style="border: 0;  text-align: left" width="20%">

                        </td>
                    </tr>

                </table>
                <table width="100%">   
                    <tr>
                        <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                            <p class="formSetting"><b>File No.</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->FileNumber}}</p>
                        </td>
                        <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                            <p class="formSetting"><b>Mr./Miss</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->StudentName}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; text-align: left" width="50%">
                            <p class="formSetting"><b>Roll No.</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->InstituteRollNumber}}</p>
                        </td>
                        <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                            <p class="formSetting"><b>S/o/D/o</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->FatherName}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc; text-align: left" width="50%">
                            <p class="formSetting"><b>Section</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$StudentNumber[0]->Section}}</p>
                        </td>
                        <td style="border: 1px solid #ccc; text-align: left" width="50%">
                            <p class="formSetting"><b>Group</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$StudentNumber[0]->Grop}}</p>
                        </td>

                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc;  text-align: left" width="50%">
                            <p class="formSetting"><b>Department</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$branchName[0]->DepartmentFullName}}</p>
                        </td>
                        <td style="border: 1px solid #ccc; text-align: left" width="50%">
                            <p class="formSetting"><b>Date of Registration</b></p>
                            <p class="formSetting" style="float:right;text-align: right;">{{$studentbasic[0]->created_at}}</p>
                        </td>                   
                    </tr>


                </table>
                <table width="100%" style="margin-top:1px;"> 
                    <tr>
                        <td style="border: 0;  text-align: left">                              

                            <p style="margin-left:0px; margin-top: 5px; font-size: 12px;">
                                Document to be submitted (if any):  
                            </p>
                            <p>Admission / registration is provisional subject to production of the above document/s on or before</p>
                            <br></br>
                            <p><b>Place:</b></p>
                            <p style="float:right;">NIT, Hamirpur (HP) Dy. Registrar (Academic) or his nominee</p>
                            <p><b>Dated: </b></p>
                            <!--<p style="float:right;">NIT, Hamirpur(HP)-177005</p>-->
                        </td>
                    </tr>
                </table>
                <div style="clear:both"></div> 
            </div>

    </body>

</html>