@extends('layouts.admin_app')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::Total Profit-->


                        <div class="m-widget24">                     
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    <img src="assets/app/media/img/client-logos/BTC.png" alt="" class="wallet-img">
                                    Reg. Form
                                </h4><br>

                                <br/>
                                <span class="m-widget24__stats m--font-brand coin-sec">
                                    0
                                </span>    
                                <!--<div class="m--space-10"></div>-->
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 5%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">

                                </span>

                            </div>                    
                        </div>
                        <!--end::Total Profit-->


                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Feedbacks-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    <img src="assets/app/media/img/client-logos/XLM.png" alt="" class="wallet-img">
                                    Reg. For Semester
                                </h4><br>

                                <br/>
                                <span class="m-widget24__stats m--font-info coin-sec">
                                    0
                                </span>   
                                <!--<div class="m--space-10"></div>-->
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 8%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">

                                </span>

                            </div>      
                        </div>
                        <!--end::New Feedbacks--> 
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Orders-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    <img src="assets/app/media/img/client-logos/LTC.png" alt="" class="wallet-img">
                                    Check NO Dues
                                </h4><br>

                                <br/>
                                <span class="m-widget24__stats m--font-danger coin-sec">
                                    0
                                </span>     
                                <!--<div class="m--space-10"></div>-->
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 9%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">

                                </span>

                            </div>      
                        </div>
                        <!--end::New Orders--> 
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <!--begin::New Users-->
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    <img src="assets/app/media/img/client-logos/XRP.png" alt="" class="wallet-img">
                                    Update SGPI/CGPI For Semester
                                </h4><br>

                                <br/>
                                <span class="m-widget24__stats m--font-success coin-sec">
                                    0
                                </span>    
                                <!--<div class="m--space-10"></div>-->
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 10%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">

                                </span>

                            </div>      
                        </div>
                        <!--end::New Users--> 
                    </div>
                </div>
            </div>
        </div>

<!--        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title" style="padding-top: 0px;">
                        <h3 class="m-portlet__head-text">
                            Your Registraion
                        </h3>
                    </div>
                </div>
            </div>
            
        </div>-->
    </div>
</div>
</div>
@endsection
