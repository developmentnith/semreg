@extends('layouts.admin_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">All Students Semester Wise SGPI/CGPI Details</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-6">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    SGPI/CGPI Details
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__head">

                    <div class="col-md-4">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="javascript:void()" data-toggle="modal" data-target="#CreateDuplicateSGPICGPI" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            View/Process Duplicate Sgpi/CGPI
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="{{ url('/admin/download-SGPI-CGPI-Excel/xlsx') }}"class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">Export to .xlsx</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="{{ url('/admin/download-SGPI-CGPI-Excel/xls') }}" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">Export to .xls</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">

                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <table class="table table-hover" id="myTable2"> 
                                    <thead style="background: #f1f2f7;">
                                        <tr>
                                            <th scope="col">Roll Number</th>
                                            <th scope="col">Semester</th>
                                            <th scope="col">SGPI</th>                                           
                                            <th scope="col">CGPI</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($sgpicgpi as $row)
                                        <tr>
                                            <td><?= $row->RollNumber ?></td>
                                            <td><?= $row->Semester ?></td>
                                            <td><?= $row->SGPI ?></td>
                                            <td><?= $row->CGPI ?></td>

                                        </tr> 
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->



<div class="modal fade" id="CreateDuplicateSGPICGPI" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Duplicate SGPI/CGPI Details</h3>

            </div>
            <div class="modal-body">    
                <form id="duplicatsgpicgpi" action="{{'/admin/upd-dupl-sgpi'}}" method="post" >
                    @csrf
                    <fieldset> 
                        <div class="col-md-12 col-lg-12 col-xl-12">
                            <table class="table table-hover regStudent" "> 
                                <thead style="background: #f1f2f7;">
                                    <tr>
                                        <th scope="col">Roll Number</th>
                                        <th scope="col">Semester</th>
                                        <th scope="col">SGPI</th>                                           
                                        <th scope="col">CGPI</th>

                                    </tr>
                                </thead>

                                <tbody>
                                    @forelse ($duplicate as $row1)
                                    <tr>
                                        <td><?= $row1->RollNumber ?></td>
                                        <td><?= $row1->Semester ?></td>
                                        <td><?= $row1->SGPI ?></td>
                                        <td><?= $row1->CGPI ?></td>
                                        @empty
                                <p>No Reocord Found</p>
                                @endforelse
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        </div>


                        <input class="btn btn-success pull-right" type="submit" value="Process Duplicate SGPI/CGPI">
                    </fieldset>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div> 

        </div>
    </div>
</div>






@endsection