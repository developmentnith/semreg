@extends('layouts.admin_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Student View</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    Student Details
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">
                      
                    </div>
                </div>
            </div>
        </div>
         <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
<div class="col-sm-12" style="background-color:lavender;">
<div class="col-sm-2" style="background-color:lavender;">
</div>
    <div class="col-sm-8" style="background-color:lavender;">
        <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Particulars</th>
      <th scope="col">Data</th>
    </tr>
  </thead>
 @foreach( $studentdata as $student )

       <tr>
      <th scope="row">1</th>
      <td>Student Unique ID</td>
      <td>{{$student->studentID}}</td>
      <td></td>
    </tr>
    
       <tr>
      <th scope="row">2</th>
      <td>Admission In</td>
      <td>{{$student->AdmissionIN}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">3</th>
      <td>Admission Through</td>
      <td>{{$student->AdmissionThrough}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">4</th>
      <td>Admission Category</td>
      <td>{{$student->AdmissionCategory}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">5</th>
      <td>Student Name</td>
      <td>{{$student->StudentName}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">6</th>
      <td>Student Name(Hindi)</td>
      <td>{{$student->StudentNameHindi}}</td>
      <td></td>
    </tr>
    
       <tr>
      <th scope="row">7</th>
      <td>Father Name</td>
      <td>{{$student->FatherName}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">8</th>
      <td>Father Name (Hindi)</td>
      <td>{{$student->FatherNameHindi}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">9</th>
      <td>Mother Name</td>
      <td>{{$student->MotherName}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">10</th>
      <td>Mother Name (in Hindi)</td>
      <td>{{$student->MotherNameHindi}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">11</th>
      <td>Guardisn Name</td>
      <td>{{$student->GuardianName}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">12</th>
      <td>Date of Birth</td>
      <td>{{$student->DOB}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">13</th>
      <td>Gender</td>
      <td>{{$student->Gender}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">14</th>
      <td>Category</td>
      <td>{{$student->Category}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">15</th>
      <td>Religion</td>
      <td>{{$student->Religion}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">16</th>
      <td>Permanent Address</td>
      <td>{{$student->PermanentAddress}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">17</th>
      <td>Correspondence Address</td>
      <td>{{$student->CoAddress}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">18</th>
      <td>Contact Number Student</td>
      <td>{{$student->ContactNumberStudent}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">19</th>
      <td>Contact Number Father</td>
      <td>{{$student->ContactNumberFather}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">20</th>
      <td>Contact Number Mother</td>
      <td>{{$student->ContactNumberMother}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">21</th>
      <td>Contach Number (Gurardian)</td>
      <td>{{$student->ContactNumberGuardian}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">22</th>
      <td>Email-ID Student</td>
      <td>{{$student->EmailIDStudent}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">23</th>
      <td>Email ID Father</td>
      <td>{{$student->EmailIDFather}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">24</th>
      <td>Student's Bonafied State</td>
      <td>{{$student->BonafiedState}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">25</th>
      <td>Adhaar Number</td>
      <td>{{$student->AdharNo}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">26</th>
      <td>Weather PwD</td>
      <td>{{$student->WeatherPWD}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">27</th>
      <td>Belong to Rural/Urban</td>
      <td>{{$student->Rural_Urban}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">28</th>
      <td>Annual Family Income</td>
      <td>{{$student->AnnualFamilyIncome}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">29</th>
      <td>Hostel Required</td>
      <td>{{$student->HostelRequired}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">30</th>
      <td>Nearest Railway Station</td>
      <td>{{$student->RailwayStation}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">31</th>
      <td>JEE Main Roll Number</td>
      <td>{{$student->jeeMainRollNumber}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">32</th>
      <td>Jee Main Rank</td>
      <td>{{$student->JeeMainRankAl}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">33</th>
      <td>Jee Main State</td>
      <td>{{$student->JeeMainRankState}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">34</th>
      <td>JEE Main Category Rank</td>
      <td>{{$student->JeeMainRankCategoryAI}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">35</th>
      <td>JEE Main Rank State Category </td>
      <td>{{$student->JeeMainRankCategoryState}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">36</th>
      <td>Gate Registration ID</td>
      <td>{{$student->GateRegID}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">37</th>
      <td>Gate Score</td>
      <td>{{$student->GateScore}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">38</th>
      <td>Gate Reg Year</td>
      <td>{{$student->GateRegYear}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">39</th>
      <td>Gate Paper Code</td>
      <td>{{$student->GatePaperCode}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">40</th>
      <td>Last Institution Attended</td>
      <td>{{$student->LastInstitutionAttended}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">41</th>
      <td>Qualifying Exam Passed</td>
      <td>{{$student->QualifyingExamPassed}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">42</th>
      <td>Qualifying Exam Passed From</td>
      <td>{{$student->QualifyingExamPassedState}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">43</th>
      <td>Marks in(%) or in CGPI</td>
      <td>{{$student->MarksInPercentage_CGPI}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">44</th>
      <td>Marks/CGPI</td>
      <td>{{$student->MarksPercentageCGPI}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">45</th>
      <td>Admitted in Category</td>
      <td>{{$student->AdmittedCategory}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">46</th>
      <td>Last Exam Passed in Year</td>
      <td>{{$student->LastExamPassingYear}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">47</th>
      <td>Admitted in Branch</td>
      <td>{{$student->AdmissionBranch}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">48</th>
      <td>Specilization</td>
      <td>{{$student->Specilization}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">49</th>
      <td>Exam Passed from Country</td>
      <td>{{$student->ExamPassedCountry}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">50</th>
      <td>Registration Number</td>
      <td>{{$student->RegistrationNumber}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">51</th>
      <td>File Number</td>
      <td>{{$student->FileNumber}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">52</th>
      <td>Admission Date</td>
      <td>{{$student->AdmissionDate}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">53</th>
      <td>Data Checked By</td>
      <td>{{$student->DataChcekedBy}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">54</th>
      <td>Data Verified By</td>
      <td>{{$student->DataVerifiedBy}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">55</th>
      <td>Admission Status</td>
      <td>{{$student->StudentAdmissionStatus}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">56</th>
      <td>Institute RollNumber</td>
      <td>{{$student->InstituteRollNumber}}</td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">57</th>
      <td>StudentPhoto</td>
      <td><img src="{{$student->StudentPhoto}}"></td>
      <td></td>
    </tr>
      
       <tr>
      <th scope="row">58</th>
      <td>User ID</td>
      <td>{{$student->RegUserID}}></td>
      <td></td>
    </tr>
       <tr>
      <th scope="row">59</th>
      <td>Weather Active </td>
      
      <td>{{$student->Status}}</td>
      <td></td>
    </tr>

  @endforeach
  
  
  </tbody>
</table>
        <!----------------------->
    </div>
    <div class="col-sm-2" style="background-color:lavender;"></div>
    </div>
</div>
</div>

</div>
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->


@endsection