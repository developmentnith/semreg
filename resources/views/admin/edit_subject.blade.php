<div class="row">
    <div class="col-md-12">   
        <?php
        $result = App\SubjectMaster::find($param2);
        ?>
        <form id="openelectiveedit"  action="{{ url('admin/edit-subject') }}" method="post" >
            @csrf            
            <fieldset> 
                <div class="form-group">
                    <input class="form-control" required="" placeholder="Subject Name" value="<?= $result->SubjectName ?>"  name="subjectname" type="text">
                    <input class="form-control" readonly=""  value="<?= $result->id ?>"  name="subjectid" type="hidden">
                </div>
                <div class="form-group">
                    <input class="form-control" required="" placeholder="Subject Code" value="<?= $result->SubjectCode ?>" name="subjectcode" type="text">
                </div>
                <div class="form-group">
                    <input class="form-control" value="<?= $result->credit ?>"  onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Credit" required="" name="credit" type="text">
                </div>
                <div class="form-group">
                    <select class="form-control" name="theorypractical">
                        <option value="Theory"<?php if ($result->TheoryPractical == 'Theory') echo 'selected'; ?> >Theory</option>
                        <option value="Practical"<?php if ($result->TheoryPractical == 'Practical') echo 'selected'; ?>>Practical</option>
                    </select>

                </div>
                <div class="form-group">
                    <select class="form-control" name="degree">
                        <option value="BTech"<?php if ($result->Degree == 'BTech') echo 'selected'; ?>>BTech</option>
                        <option value="BArch"<?php if ($result->Degree == 'BArch') echo 'selected'; ?>>BArch</option>
                        <option value="MTech"<?php if ($result->Degree == 'MTech') echo 'selected'; ?>>MTech</option>
                        <option value="MArch"<?php if ($result->Degree == 'MArch') echo 'selected'; ?>>MArch</option>
                        <option value="Dual"<?php if ($result->Degree == 'Dual') echo 'selected'; ?>>Dual</option>
                        <option value="DualUG"<?php if ($result->Degree == 'DualUG') echo 'selected'; ?>>DualUG</option>
                        <option value="DualPG"<?php if ($result->Degree == 'DualPG') echo 'selected'; ?>>DualPG</option>
                    </select>

                </div>
                <div class="form-group">
                    <select class="form-control" name="semester">
                        <option value="S01"<?php if ($result->Degree == 'S01') echo 'selected'; ?>>S01</option>
                        <option value="S02"<?php if ($result->Degree == 'S02') echo 'selected'; ?>>S02</option>
                        <option value="S03"<?php if ($result->Degree == 'S03') echo 'selected'; ?>>S03</option>
                        <option value="S04"<?php if ($result->Degree == 'S04') echo 'selected'; ?>>S04</option>
                        <option value="S05"<?php if ($result->Degree == 'S05') echo 'selected'; ?>>S05</option>
                        <option value="S06"<?php if ($result->Degree == 'S06') echo 'selected'; ?>>S06</option>
                        <option value="S07"<?php if ($result->Degree == 'S07') echo 'selected'; ?>>S07</option>
                        <option value="S08"<?php if ($result->Degree == 'S08') echo 'selected'; ?>>S08</option>
                        <option value="S09"<?php if ($result->Degree == 'S09') echo 'selected'; ?>>S09</option>
                        <option value="S10"<?php if ($result->Degree == 'S10') echo 'selected'; ?>>S10</option>

                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="group">
                        <option value="A"<?php if ($result->Group == 'A') echo 'selected'; ?>>A</option>
                        <option value="B"<?php if ($result->Group == 'B') echo 'selected'; ?>>B</option>
                        <option value="C"<?php if ($result->Group == 'C') echo 'selected'; ?>>C</option>
                        <option value="D"<?php if ($result->Group == 'D') echo 'selected'; ?>>D</option>
                    </select> 
                </div>
                <div class="form-group">
                    <select class="form-control" name="SubjectType">

                        <option value="Core"<?php if ($result->SubjectType == 'Core') echo 'selected'; ?>>Core</option>
                        <option value="Elective"<?php if ($result->SubjectType == 'Elective') echo 'selected'; ?>>Elective</option>
                        <option value="CorePractical"<?php if ($result->SubjectType == 'CorePractical') echo 'selected'; ?>>Core Practical</option>
                        <option value="ElectivePractical"<?php if ($result->SubjectType == 'ElectivePractical') echo 'selected'; ?>>Elective Practical</option>
                    </select>
                </div>
                <div class="form-group">
                    <input class="form-control" value="<?= $result->TotalLectureTheory ?>" required="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturetheory" type="text">
                </div>
                <div class="form-group"> 
                    <input class="form-control" required="" value="<?= $result->TotalLabPractical ?>" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Total Lecture Theory" required="" name="totallecturePractical" type="text">
                </div>
                <div class="form-group">
                    <select class="form-control" name="isactive">
                        <option value="active"<?php if ($result->isActive == 'active') echo 'selected'; ?>>Active</option>
                        <option value="deactive"<?php if ($result->isActive == 'deactive') echo 'selected'; ?>>Deactive</option>
                    </select>
                </div>

                <input class="btn btn-success pull-right" type="submit" value="Submit">
            </fieldset>

        </form>
    </div>
</div>
