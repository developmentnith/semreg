@extends('layouts.admin_app')

@section('content')

<div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Create User</h3>

            </div>
        </div>
    </div>
    <!-- END: Subheader -->

    <!-- hide section of all wallets -->

    <!-------------End-------------->

    <div class="m-content">
        <div class="m-portlet wallet-portlet" style="margin-bottom: 15px;">
            <div class="m-portlet__body m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                </div>
            </div>
        </div>

        <!----------------------->
        <div class="m-portlet" style=" margin-top: 15px; margin-bottom: 5px;">
            <div class="row">
                <div class="col-md-10">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <h3 class="m-portlet__head-text">
                                    All Users Details
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title" style="padding-top: 0px;">
                                <a href="javascript:void()" data-toggle="modal" data-target="#CreateOpenElectiveModal" class="btn btn-accent m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Add User
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body  m-portlet__body--no-padding" style="padding: 0 30px;">
                <!---end-->
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-12 col-xl-12">

                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <table class="table table-hover regStudent" > 
                                    <thead style="background: #f1f2f7;">
                                        <tr>
                                            <th scope="col">SI.NO</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>                                         
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $sr = 1; @endphp
                                        @foreach($users as $row)
                                        <tr>
                                            <td>{{ $sr }}</td>
                                            <td>{{ $row->name }}</td>
                                            <td>{{ $row->email }}</td>                                          
                                            <td>
                                                <a class="btn btn-primary btn-xs" onclick="showAdminAjaxModal('{{ url('admin/edit-modal-pop/edit_user/'.$row->id.'') }}');"  style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                <a class="btn btn-danger btn-xs" href="{{ url('admin/delete-user/'.$row->id.'') }}" onclick="return confirm('Are you sure you want to delete this?')" style="cursor: pointer;"> <i style="padding: 10px 5px;" class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                        @php $sr++; @endphp
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!----------------------->
    </div>
</div>

</div>
<!--end:: Body -->

<!-- end::Footer -->
</div>
<!--end:: Page -->



<div class="modal fade" id="CreateOpenElectiveModal" tabindex="-1" role="dialog" aria-labelledby="createClassModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Give Open Elective Details</h3>

            </div>
            <div class="modal-body">    
                <form id="openelectiveallot" action="{{ url('admin/add-user') }}" method="post" >
                    @csrf
                    <fieldset> 
                        <div class="form-group">
                            <label for="name">User Name<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <input class="form-control"  placeholder="Name" required="" name="name" type="text">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email">Email<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <input class="form-control"  placeholder="Email" required="" name="email" type="text"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Password">Password<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <input class="form-control"  placeholder="Password" required="" name="password" type="password"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="semester">Department<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <select class="form-control" required="" name="department">
                                    <option value="" selected>Please Select Department</option>                                   
                                    @foreach ($populatebranshort as $populatebr) 
                                    <option value="{{ $populatebr->DepartmentShortName }}">{{ $populatebr->DepartmentFullName }}</option>
                                    @endforeach                               
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="semester">Type<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <select class="form-control" required="" name="type">
                                    <option value="" selected>Please Select Type</option>                                   
                                    <option value="admin">Admin</option>                                   
                                    <option value="faculty">Faculty</option>                                   
                                    <option value="student">Student</option>                                   
                                    <option value="user">User</option>                                   
                                </select> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="semester">Role<span class="requiredfield">*</span></label>
                            <div class="col-sm-12">  
                                <?php
                                $rolldata = array("ADMIN", "FACULTY", "NODUES", "SUGC", "SPGC", "USER");
                                foreach ($rolldata as $value) {
                                    ?>
                                    <div class="checkbox"> 
                                        <label><input type="checkbox" name="role[]" value="<?= $value ?>"><?= $value ?></label>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <br>
                        <br>

                        <input class="btn btn-success pull-right" type="submit" value="Submit">
                    </fieldset>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div> 

        </div>
    </div>
</div>






@endsection