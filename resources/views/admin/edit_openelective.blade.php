<div class="row">
    <div class="col-md-12">
   <?php
        $result = App\OpenElectiveAllotment::find($param2); 

        ?>
        <form id="openelectiveedit"  action="{{ url('admin/edit-open-elective-allotment') }}" method="post" >
            @csrf            
            <fieldset> 
                <div class="form-group">
                    <label for="Roll Numeber">Roll Number<span class="requiredfield">*</span></label>
                    <input type="text" value="<?= $result->RollNumber ?>" readonly id = "rollnumber" name="rollnumber">
                </div>

                <div class="form-group">
                    <label for="choicenum">Choice Number:</label>
                    <select class="form-control fetchChoiceNum" name="choice_number" id = "choice_number" required="">
                        <option value="">Select Choice Number</option>
                      @foreach($choices as $choice)
                      <option value="{{$choice->ChoiceNum}}">{{$choice->ChoiceNum}}--(<?= $choice->SubjectID ?>)</option>                      
                      @endforeach

                    </select>
                </div>

                <div class="form-group">
                    <label for="subjectcode">Subject Code<span class="requiredfield">*</span></label>
                    <input type="text" readonly value="<?= $result->SubjectID ?>" id = "subjectid" name="subjectid">
                </div>

                <div class="form-group">
                    <label for="subjectname">Subject Name<span class="requiredfield">*</span></label>
                    <input type="text" readonly value="<?= $result->SubjectName ?>" id = "subjectname" name="subjectname">
                </div>
                <div class="form-group">
                    <label for="Department">Department<span class="requiredfield">*</span></label>
                    <input type="text" value="<?= $result->Department ?>" readonly  id="department" name="department">
                </div>

                <div class="form-group">
                    <label for="semester">Semester<span class="requiredfield">*</span></label>
                    <input type="text" value="<?= $result->Semester ?>" readonly  id = "semester" name="semester">
                </div>
                <div class="form-group">
                    <label for="year">Year<span class="requiredfield">*</span></label>
                    <input type="text" value="<?= $result->Year ?>" readonly id = "year" name="year">
                </div>
                <br>
                <br>

                <input  type="hidden" name ="rid" value="{{$result->id}}">
                <input class="btn btn-success pull-right" type="submit" value="Save Changes">
            </fieldset>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </form>
    </div>
</div>
