@extends('layouts.app')

@section('content')
<div class="form-body without-side">
    <div class="website-logo">
        <a href="index.html">
            <div class="logo">
                <img class="logo-size" src="images/logo.png" alt="">
            </div>
        </a>
    </div>
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">
                <img src="images/logo.png" alt="">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <h3>Login to account</h3>
                    <p>Student Registration Portal NIT Hamirpur</p>
                    <form method="POST" id="loginForm" >
                        <input id="email" placeholder="Email ID" type="email" class="form-control" name="email" value="" required autofocus>
                        <input id="password" type="password" placeholder="Password" class="form-control" name="password" required>
                        <br>
                        <select class="form-control" name="login_type" required>
                            <option value="">Select Login Type</option>  
                            <option value="student">Student</option>
                            <option value="faculty">Faculty</option> 
                            <option value="admin">Admin</option>
                        </select>

                        <div class="form-button">
                            <button  type="button" class="ibtn submitLogin">Login</button> <a href="{{ URL::to('/forget-password') }}">Forget password?</a>
                        </div>
                        <p id="sucessmsg"></p>
                        <p id="errormsg"></p>
                    </form>

                    <div class="page-links">
                        <a href="{{ route('register') }}">Register new account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
