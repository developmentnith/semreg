@extends('layouts.app')

@section('content')
<div class="form-body without-side">
    <div class="website-logo">
        <a href="index.html">
            <div class="logo">
                <img class="logo-size" src="images/logo.png" alt="">
            </div>
        </a>
    </div>
    <div class="row">
        <div class="img-holder">
            <div class="bg"></div>
            <div class="info-holder">
                <img src="images/logo.png" alt="">
            </div>
        </div>
        <div class="form-holder">
            <div class="form-content">
                <div class="form-items">
                    <h3>Forget password</h3>
                    <p>Student Registration Portal NIT Hamirpur</p> 
                    <form method="POST" id="forgetForm" >
                        <input id="email" placeholder="Email ID"  type="email" class="form-control" name="email_id" value="" required autofocus>
                        <input id="phone" placeholder="Phone Number" maxlength="10" type="text" class="form-control" name="phone_no" value="" required autofocus>
                        
                        <div class="form-button">
                            <button id="forgetPassword"  type="button" class="ibtn forgetPassword">Forget password</button> <a href="{{ route('login') }}">Login</a>
                        </div>
                        <p id="sucessmsg"></p>
                        <p id="errormsg"></p>
                    </form>

                    <div class="page-links">
                        <a href="{{ route('register') }}">Register new account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
